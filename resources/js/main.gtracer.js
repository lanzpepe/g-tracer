$(function () {
    var dropdown = { allowAdditions: true, forceSelection: false, hideAdditions: false };
    var modal = { closable: false, autofocus: false };
    var options = { clearable: true };
    var origin = window.location.origin;
    var href = window.location.href;
    var idleTime = 0;

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#container').css('padding-top', `${$('#navbar').height()}px`);
    $('#manager').css('height', `${$(window).height()}px`);

    $('.masthead').visibility({
        once: false,
        onBottomPassed: () => {
            $('.fixed.menu').transition('fade in');
        },
        onBottomPassedReverse: () => {
            $('.fixed.menu').transition('fade out');
        }
    });

    $('.search .ui.dropdown, .retrieve .ui.dropdown').dropdown();
    $('.ui.embed').embed();
    $('.icon.link').popup();
    $('.tabular.menu .item').tab();
    $('.ui.sidebar').sidebar('setting', 'transition', 'overlay').sidebar('attach events', '.toc.item');

    $('.notify.toast').toast({
        transition: {
            showMethod: 'fade left',
            hideMethod: 'fade left'
        }
    });

    $('.special.cards .image').dimmer({
        on: 'hover'
    });

    $('.menu .browse').popup({
        inline: true,
        hoverable: true,
        position: 'bottom left'
    });

    if ($('.item').hasClass('active')) {
        $.ajax({
            url: `${origin}/page`,
            method: 'POST',
            data: {
                title: $('.header.title').text().trim(),
                description: href,
                url: href
            }
        });
    }

    $.ajax({
        url: `${origin}/pages`,
        method: 'GET',
        dataType: 'json',
        success: (data) => {
            var contents = [];
            $.each(data.pages, (k, v) => {
                contents.push({
                    title: v.title,
                    description: v.description,
                    url: v.url
                });
            });
            $('.ui.search').search({
                source: contents
            });
        }
    });

    // notifications
    $('.notification').on('click', function () {
        $.ajax({
            url: `${origin}/notifications`,
            method: 'GET',
            dataType: 'html',
            success: (data) => {
                $('.notifications').empty().html(data);
            },
            error: (response) => {
                console.error(response);
            }
        });
    });

    $('.notification .ui.dropdown').dropdown({
        onChange: (value) => {
            $.ajax({
                url: `${origin}/notifications/${value}`,
                method: 'GET',
                dataType: 'html',
                success: (result) => {
                    console.log(result);
                },
                error: (response) => {
                    console.error(response);
                }
            });
        },
        direction: 'downward'
    });
    // end notifications

    // start DataTables
    $('#graduates').DataTable({
        'info': false,
        'lengthMenu': [10],
        'ordering': false,
        'stateSave': true
    }).on('page.dt', function () {
        $('html, body').animate({ scrollTop: 0 }, 0);
    }).on('click', 'tr', function () {
        window.location = $(this).attr('data-target');
    });

    $('#requests').DataTable({
        'info': false,
        'lengthMenu': [10],
        'ordering': false,
        'stateSave': true
    }).on('page.dt', function () {
        $('html, body').animate({ scrollTop: 0 }, 0);
    });

    $('#fields').DataTable({
        'info': false,
        'lengthChange': false,
        'searching': false,
        'ordering': false
    });

    $('#linkedin').DataTable({
        'info': false,
        'lengthMenu': [10],
        'ordering': false,
        'stateSave': true
    }).on('page.dt', function () {
        $('html, body').animate({ scrollTop: 0 }, 0);
    }).on('click', '.view-info', function () {
        $.ajax({
            url: `linkedin/${$(this).data('value')}`,
            method: 'GET',
            dataType: 'json',
            success: (data) => {
                $('#linkedinModal').modal(
                    $.extend(modal, {
                        onShow: () => {
                            $('#profileImage').attr('src', data.linkedin.avatar);
                            $('#profileID').html(data.linkedin.id);
                            $('#profileName').html(`${data.linkedin.first_name} ${data.linkedin.last_name}`);
                            $('#profileUrl').html(data.linkedin.profile_url);
                            $('#profileCompany').html(data.linkedin.company);
                            $('#profilePosition').html(data.linkedin.position);
                        }
                    })
                ).modal('show');
            },
            error: (result) => {
                console.error(result);
            }
        })
    });

    $('#responses').DataTable({
        'info': false,
        'lengthChange': false,
        'ordering': false
    });
    // end DataTables

    // account modal functions
    $('.add-account').on('click', function () {
        $('#accountModal').modal(
            $.extend(modal, {
                onShow: () => {
                    $('.ui.dropdown').dropdown(dropdown);
                    $('.school .ui.dropdown, .department .ui.dropdown').dropdown(options);
                    calendar('#birthdate'); departments('.school .ui.dropdown');
                }
            })
        ).modal('show');
    });
    $('.edit-account').on('click', function () {
        $.ajax({
            url: `accounts/${$(this).data('value')}`,
            method: 'GET',
            dataType: 'json',
            success: (data) => {
                $('#accountModal').modal(
                    $.extend(modal, {
                        onShow: () => {
                            $('#accountModal .title').html('Edit Account');
                            $('.username, .password').hide();
                            $('#_admin').val(data.admin.admin_id);
                            $('#_person').val(data.admin.user.person_id);
                            $('#username').val(data.admin.username);
                            $('#password').val(data.admin.password);
                            $('#password-confirm').val(data.admin.password);
                            $('#lastname').val(data.admin.user.person.last_name);
                            $('#firstname').val(data.admin.user.person.first_name);
                            $('#midname').val(data.admin.user.person.middle_name);
                            $('#gender').val(data.admin.user.person.gender);
                            $('#dob').val(data.admin.user.person.birth_date);
                            $('#school').val(data.admin.schools[0].name);
                            $('#dept').append($('<option>', {
                                value: data.admin.departments[0].name,
                                text: data.admin.departments[0].name,
                                selected: true
                            }));
                            $('#role').val(data.admin.roles[0].name);
                            $('#btnAccount').val('updated');
                            $('#btnAccount .label').html('Apply');
                            $('.ui.dropdown').dropdown(dropdown);
                            $('.school .ui.dropdown, .department .ui.dropdown').dropdown(options);
                            calendar('#birthdate'); departments('.school .ui.dropdown');
                        },
                        onHide: () => {
                            window.location.reload();
                        }
                    })
                ).modal('show');
            },
            error: (result) => {
                console.error(result);
            }
        });
    });
    $('.mark-account').on('click', function () {
        $.ajax({
            url: `accounts/${$(this).data('value')}`,
            method: 'GET',
            dataType: 'json',
            success: (data) => {
                $('#markAccountModal').modal(
                    $.extend(modal, {
                        onShow: () => {
                            var user = data.admin.user.person;
                            $('.username.holder').val(data.admin.admin_id).html(`Username: ${data.admin.username}`);
                            $('.name.holder').html(`Account Name: ${user.first_name} ${user.middle_name} ${user.last_name}`);
                            $('.department.holder').html(`Department: ${data.admin.departments[0].name}`);
                            $('.school.holder').html(`School: ${data.admin.schools[0].name}`);
                            $('#deleteForm').attr('action', `${href}/${$('.username.holder').val()}`);
                        }
                    })
                ).modal('show');
            },
            error: (result) => {
                console.error(result);
            }
        });
    });
    $('.delete-account').on('click', function () {
        window.location.assign(`${href}/${$('.username.holder').val()}`);
    });
    // end account modal

    // school modal functions
    $('.add-school').on('click', function () {
        $('#schoolModal').modal('show');
    });
    $('.edit-school').on('click', function () {
        $.ajax({
            url: `schools/${$(this).data('value')}`,
            method: 'GET',
            dataType: 'json',
            success: (data) => {
                $('#schoolModal').modal(
                    $.extend(modal, {
                        onShow: () => {
                            $('#schoolModal .title').text('Edit School');
                            $('#school').val(data.school.id);
                            $('#name').val(data.school.name);
                            $('#btnSchool').val('updated');
                            $('#btnSchool .label').html('Apply');
                        },
                        onHide: () => {
                            window.location.reload();
                        }
                    })
                ).modal('show');
            },
            error: (result) => {
                console.error(result);
            }
        });
    });
    $('.mark-school').on('click', function () {
        $.ajax({
            url: `schools/${$(this).data('value')}`,
            method: 'GET',
            dataType: 'json',
            success: (data) => {
                $('#markSchoolModal').modal(
                    $.extend(modal, {
                        onShow: () => {
                            $('.school.name').val(data.school.id).html(`School Name: ${data.school.name}`);
                            $('#deleteForm').attr('action', `${href}/${data.school.id}`);
                        }
                    })
                ).modal('show');
            },
            error: function (result) {
                console.error(result);
            }
        });
    });
    $('.delete-school').on('click', function () {
        window.location.assign(`${href}/${$('.school.name').val()}`);
    });
    // end school modal

    // department modal functions
    $('.add-dept').on('click', function () {
        $('#deptModal').modal(
            $.extend(modal, {
                onShow: () => {
                    $('.ui.dropdown').dropdown(options);
                    $('.dept .ui.dropdown').dropdown(dropdown);
                    departments('.school .ui.dropdown');
                }
            })
        ).modal('show');
    });
    $('.edit-dept').on('click', function () {
        var ids = $(this).data('value').split('+');
        $.ajax({
            url: `departments/${ids[0]}`,
            method: 'GET',
            dataType: 'json',
            success: (data) => {
                $('#deptModal').modal(
                    $.extend(modal, {
                        onShow: () => {
                            $('#deptModal .title').text('Edit Department');
                            var school = $.grep(data.dept.schools, (a) => {
                                return a.id == ids[1];
                            });
                            $('#department').val(data.dept.id);
                            $('#school').val(school[0].name);
                            $('#dept').append($('<option>', {
                                value: data.dept.name,
                                text: data.dept.name,
                                selected: true
                            }));
                            $('.ui.dropdown').dropdown(options);
                            $('.dept .ui.dropdown').dropdown(dropdown);
                            departments('.school .ui.dropdown');
                            $('#btnDept').val('updated');
                            $('#btnDept .label').html('Apply');
                        },
                        onHide: () => {
                            window.location.reload();
                        }
                    })
                ).modal('show');
            },
            error: (result) => {
                console.error(result);
            }
        });
    });
    $('.mark-dept').on('click', function () {
        var ids = $(this).data('value').split('+');
        $.ajax({
            url: `departments/${ids[0]}`,
            method: 'GET',
            dataType: 'json',
            success: (data) => {
                $('#markDeptModal').modal(
                    $.extend(modal, {
                        onShow: () => {
                            var school = $.grep(data.dept.schools, (a) => {
                                return a.id == ids[1];
                            });
                            $('.department.name').val(data.dept.id).html(`Department Name: ${data.dept.name}`);
                            $('.school.name').val(school[0].id).html(`School: ${school[0].name}`);
                            $('#deleteForm').attr('action', `departments/${data.dept.id}+${school[0].id}`);
                        }
                    })
                ).modal('show');
            },
            error: (result) => {
                console.error(result);
            }
        });
    });
    $('.delete-dept').on('click', function () {
        window.location.assign(`departments/${$('.department.name').val()}+${$('.school.name').val()}`);
    });
    // end department modal

    // course modal functions
    $('.add-course').on('click', function () {
        $('#courseModal').modal(
            $.extend(modal, {
                onShow: () => {
                    $('.ui.dropdown').dropdown(dropdown);
                }
            })
        ).modal('show');
    });
    $('.edit-course').on('click', function () {
        $.ajax({
            url: `courses/${$(this).data('value')}/edit`,
            method: 'GET',
            dataType: 'json',
            success: (data) => {
                var dept = data.course.departments[0];
                var school = data.course.schools[0];
                $('#courseModal').modal(
                    $.extend(modal, {
                        onShow: () => {
                            $('#courseModal .title').html('Edit Degree Program');
                            $('#school').val(school.name);
                            $('#dept').html(`<option value="${dept.name}" selected>${dept.name}</option>`);
                            $('#code').val(data.course.code);
                            $('#title').val(data.course.title);
                            $('#major').val(data.course.major);
                            $('.school .ui.dropdown').dropdown(options);
                            $('.ui.dropdown').dropdown(dropdown);
                            $('#btnCourse').val('updated');
                            $('#btnCourse .label').html('Apply');
                        },
                        onHide: () => {
                            window.location.reload();
                        }
                    })
                ).modal('show');
            },
            error: (result) => {
                console.error(result);
            }
        });
    });
    $('.mark-course').on('click', function () {
        $.ajax({
            url: `courses/${$(this).data('value')}`,
            method: 'GET',
            dataType: 'json',
            success: (data) => {
                $('#markCourseModal').modal(
                    $.extend(modal, {
                        onShow: () => {
                            $('.course.code').val(data.course.id).html(`Degree Code: ${data.course.code}`);
                            $('.course.title').html(`Degree Title: ${data.course.title}`);
                            $('.course.major').html(`Degree Major: ${data.course.major}`);
                            $('#deleteForm').attr('action', `courses/${data.course.id}`);
                        }
                    })
                ).modal('show');
            },
            error: (result) => {
                console.error(result);
            }
        });
    });
    $('.delete-course').on('click', function () {
        window.location.assign(`courses/${$('.course.code').val()}`);
    });
    // end course modal

    // related jobs modal functions
    $('.add-job').on('click', function () {
        $('#jobModal').modal(
            $.extend(modal, {
                onShow: () => {
                    $('.ui.dropdown').dropdown(options);
                    $('.job .ui.dropdown').dropdown(dropdown);
                },
                onApprove: () => {
                    $('#course').dropdown('get value');
                }
            })
        ).modal('show');
    });
    $('.mark-job').on('click', function () {
        $.ajax({
            url: `jobs/${$(this).data('value')}`,
            method: 'GET',
            dataType: 'json',
            success: (data) => {
                $('#markJobModal').modal(
                    $.extend(modal, {
                        onShow: () => {
                            $('.job.name').val(data.job.id).html(`Job Name: ${data.job.name}`);
                            $('#deleteForm').attr('action', `jobs/${data.job.id}`);
                        }
                    })
                ).modal('show');
            }
        });
    });
    $('.delete-job').on('click', function () {
        window.location.assign(`jobs/${$('.job.name').val()}`);
    });
    // end related jobs

    // graduate modal functions
    $('.add-graduate').on('click', function () {
        $('#graduateModal').modal(
            $.extend(modal, {
                onShow: () => {
                    $('.ui.dropdown').dropdown(); image('#image');
                    autocomplete(document.getElementById('address'));
                }
            })
        ).modal('show');
    });
    $('.edit-graduate').on('click', function () {
        $.ajax({
            url: `${href}/edit`,
            method: 'GET',
            dataType: 'json',
            success: (data) => {
                $('#graduateModal').modal(
                    $.extend(modal, {
                        onShow: () => {
                            $('#graduateModal .title').html('Update Graduate Data');
                            $('#person').val(data.graduate.person_id);
                            $('#year').val(data.graduate.academic.year);
                            $('#month').val(moment().month(data.graduate.academic.month).format('M'));
                            $('#day').val(data.graduate.academic.day);
                            $('#lastname').val(data.graduate.person.last_name);
                            $('#firstname').val(data.graduate.person.first_name);
                            $('#midname').val(data.graduate.person.middle_name);
                            $('#contact').val(data.graduate.contacts[0].contact_number);
                            $('#address').val(data.graduate.contacts[0].address);
                            $('#graduateModal .gender').val(data.graduate.person.gender);
                            $('#graduateModal .course').val(data.graduate.academic.course.code);
                            $('#graduateModal .major').val(data.graduate.academic.course.major);
                            image('#image'); autocomplete(document.getElementById('address'));
                            $('.ui.dropdown').dropdown(dropdown);
                            $('#btnGraduate').val('updated');
                            $('#btnGraduate .label').html('Apply');
                        },
                        onHide: () => {
                            window.location.reload();
                        }
                    })
                ).modal('show');
            },
            error: (result) => {
                console.error(result);
            }
        });
    });
    $('.mark-graduate').on('click', function () {
        $.ajax({
            url: `${href}/edit`,
            method: 'GET',
            dataType: 'json',
            success: (data) => {
                var graduate = data.graduate;
                var academic = graduate.academic;
                $('#markGraduateModal').modal(
                    $.extend(modal, {
                        onShow: () => {
                            $('.graduate.name').html(`Name: ${graduate.person.first_name} ${graduate.person.middle_name} ${graduate.person.last_name}`);
                            $('.graduate.course').html(`Degree: ${academic.course.title}`);
                            $('.graduate.school').html(`School: ${academic.school}`);
                            $('.graduate.sy').html(`Graduated: ${academic.month} ${academic.day}, ${academic.year}`);
                            $('#deleteForm').attr('action', href);
                        }
                    })
                ).modal('show');
            },
            error: (result) => {
                console.error(result);
            }
        })
    });
    $('.delete-graduate').on('click', function () {
        window.location.assign(href);
    });
    $('.confirm-graduate').on('click', function () {
        $('#confirmModal').modal(
            $.extend(modal, {
                onShow: () => {
                    $('.ui.dropdown').dropdown(dropdown);
                    $('.status .ui.dropdown').dropdown({
                        onChange: (value) => {
                            if (value === 'Unemployed') {
                                $('#coaddress, #cocontact, #dateHired, #remarks').removeAttr('required');
                                $('.company.checkbox, .address.checkbox, .position.checkbox').addClass('read-only');
                                $('.employed').hide();
                            } else {
                                $('#coaddress, #cocontact, #dateHired, #remarks').attr('required', '');
                                $('.company.checkbox, .address.checkbox, .position.checkbox').removeClass('read-only');
                                $('.employed').show();
                            }
                        }
                    });
                    $('.company.checkbox').checkbox({
                        onChecked: () => {
                            var value = $('#companycb').val();
                            $('.coname .ui.dropdown').dropdown('set selected', value);
                        },
                        onUnchecked: () => {
                            $('.coname .ui.dropdown').dropdown('clear', true);
                        }
                    });
                    $('.address.checkbox').checkbox({
                        onChecked: () => {
                            var value = $('#addresscb').val();
                            $('.coaddress #coaddress').val(value);
                        },
                        onUnchecked: () => {
                            $('.coaddress #coaddress').val('');
                        }
                    });
                    $('.position.checkbox').checkbox({
                        onChecked: () => {
                            var value = $('#positioncb').val();
                            $('.position .ui.dropdown').dropdown('set selected', value);
                        },
                        onUnchecked: () => {
                            $('.position .ui.dropdown').dropdown('clear', true);
                        }
                    });
                    calendar('#datehired');
                    autocomplete(document.getElementById('coaddress'));
                }
            })
        ).modal('show');
    });
    $('.edit-employment').on('click', function () {
        $.ajax({
            url: `${href}/edit`,
            method: 'GET',
            dataType: 'json',
            success: (data) => {
                var confirmed = data.graduate.confirmed[0];
                $('#confirmModal').modal(
                    $.extend(modal, {
                        onShow: () => {
                            $('#confirm').val(confirmed.id);
                            $('#status').val(confirmed.status);
                            if (confirmed.status === 'Employed') {
                                $('#coname').val(confirmed.employment.company.name);
                                $('#coaddress').val(confirmed.employment.company.address);
                                $('#cocontact').val(confirmed.employment.company.contact_number);
                                $('#position').val(confirmed.employment.job_position);
                                $('#dateHired').val(confirmed.employment.date_employed);
                            }
                            else {
                                $('.employed').hide();
                                $('#coaddress, #cocontact, #dateHired, #remarks').removeAttr('required');
                            }
                            $('#remarks').val(confirmed.remarks);
                            $('#btnConfirm').val('updated');
                            $('#btnConfirm .label').html('Apply');
                            $('.ui.dropdown').dropdown(dropdown);
                            $('.status .ui.dropdown').dropdown({
                                onChange: (value) => {
                                    if (value === 'Unemployed') {
                                        $('.employed').hide();
                                        $('#coaddress, #cocontact, #dateHired, #remarks').removeAttr('required');
                                    } else {
                                        $('.employed').show();
                                        $('#coaddress, #cocontact, #dateHired, #remarks').attr('required', '');
                                    }
                                }
                            });
                            calendar('#datehired');
                            autocomplete(document.getElementById('coaddress'));
                        },
                        onHide: () => {
                            window.location.reload();
                        },
                    })
                ).modal('show');
            },
            error: (result) => {
                console.error(result);
            }
        });
    });
    $('.verify-info').on('click', function () {
        $.ajax({
            url: `${href}/${$(this).data('value')}`,
            method: 'GET',
            dataType: 'json',
            success: (data) => {
                $('#verificationModal').modal(
                    $.extend(modal, {
                        onShow: () => {
                            $('#verification').val(data.verification.id);
                            $('#year').val(data.verification.year);
                            $('#month').val(moment().month(data.verification.month).format('M'));
                            $('#lastname').val(data.verification.person.last_name);
                            $('#firstname').val(data.verification.person.first_name);
                            $('#midname').val(data.verification.person.middle_name);
                            $('#contact').val(data.verification.contact_number);
                            $('#address').val(data.verification.address);
                            $('#verificationModal .gender').val(data.verification.person.gender);
                            $('#verificationModal .course').val(data.verification.course.code);
                            $('#verificationModal .major').val(data.verification.course.major);
                            image('#image'); autocomplete(document.getElementById('address'));
                            $('.ui.dropdown').dropdown(dropdown);
                            if (data.verification.status == true) {
                                $('#btnVerify').remove();
                            }
                        }
                    })
                ).modal('show');
            },
            error: (result) => {
                console.error(result);
            }
        });
    });
    // end graduate modal

    // import modal functions
    $('.import-graduates').on('click', function () {
        $('#uploadModal').modal(
            $.extend(modal, {
                onShow: () => {
                    $('.ui.dropdown').dropdown();
                }
            })
        ).modal('show');
    });
    $('.import-linkedin').on('click', function () {
        $('#linkedInUploadModal').modal('show');
    });
    // end import modal

    // reward modal functions
    $('.add-item').on('click', function () {
        $('#rewardModal').modal('show');
    });
    $('.edit-item').on('click', function () {
        $.ajax({
            url: `${href}/edit`,
            method: 'GET',
            dataType: 'json',
            success: (data) => {
                $('#rewardModal').modal(
                    $.extend(modal, {
                        onShow: () => {
                            $('#rewardModal .title').html('Edit Reward Item');
                            $('#reward').val(data.reward.id);
                            $('#name').val(data.reward.name);
                            $('#description').val(data.reward.description);
                            $('#points').val(data.reward.points);
                            $('#quantity').val(data.reward.admins[0].pivot.quantity);
                            $('#btnReward').val('updated');
                            $('#btnReward .label').html('Apply');
                        },
                        onHide: () => {
                            window.location.reload();
                        }
                    })
                ).modal('show');
            },
            error: (result) => {
                console.error(result);
            }
        });
    });
    $('.mark-item').on('click', function () {
        $.ajax({
            url: `${href}/edit`,
            method: 'GET',
            dataType: 'json',
            success: (data) => {
                $('#markRewardModal').modal(
                    $.extend(modal, {
                        onShow: () => {
                            $('.name.holder').val(data.reward.id).html(`Item name: ${data.reward.name}`);
                            $('#deleteForm').attr('action', href);
                        }
                    })
                ).modal('show');
            },
            error: (result) => {
                console.error(result);
            }
        });
    });
    $('.delete-item').on('click', function () {
        window.location.assign(href);
    })
    $('.confirm-item').on('click', function () {
        var reference = $(this).data('value');
        $.ajax({
            url: `${href}/edit`,
            method: 'GET',
            dataType: 'json',
            success: (data) => {
                $('#confirmModal').modal(
                    $.extend(modal, {
                        onShow: () => {
                            $('#reference').val(reference);
                            $('#updateForm').attr('action', href);
                        }
                    })
                ).modal('show');
            },
            error: (result) => {
                console.error(result);
            }
        });
    });
    $('.accept-item').on('click', function () {
        window.location.assign(href);
    });
    // end reward modal

    // point settings modal
    $('.set-point').on('click', function () {
        $.ajax({
            url: `points`,
            method: 'GET',
            dataType: 'json',
            success: (data) => {
                $('#pointModal').modal(
                    $.extend(modal, {
                        onShow: () => {
                            $('#submitted').val(data.point.submitted);
                            $('#confirmed').val(data.point.confirmed);
                        }
                    })
                ).modal('show');
            },
            error: (result) => {
                console.log(result);
            }
        });
    });
    // end point settings modal

    // generate data per department (admin)
    if ($('#srch_dept').length > 0) {
        var dept = $('#srch_dept').dropdown('get value');
        $('#srch_dept').closest('.ui.dropdown').dropdown({
            onChange: (value) => {
                department(value); report(value, 'one'); report(value, 'two');
            }
        });
        department(dept); report(dept, 'one'); report(dept, 'two');
    }
    if ($('#gen_dept').length > 0) {
        var dept = $('#gen_dept').dropdown('get value');
        $('#gen_dept').closest('.ui.dropdown').dropdown({
            onChange: (value) => {
                courses(value);
            }
        });
        courses(dept);
    }
    // end generate data

    // download reports
    $('#admin-download').on('click', function () {
        $('form').first().trigger('submit');
    });
    $('#dept-download').on('click', function () {
        $('form').first().trigger('submit');
    });
    // end download reports

    replace('.show-course');

    // auto logout
    setInterval(function () {
        idleTime++;
        if (idleTime >= 1920) { window.location.reload(); }
    }, 1000);

    $(this).on('mousemove', function () {
        idleTime = 0;
    });

    $(this).on('keypress', function () {
        idleTime = 0;
    });
    // end auto logout

    function courses(a){$.post('reports/courses',{dept:a},(o)=>{$('#course').html(o)},'html')};
    function department(a){$.post('reports/department',{dept:a},(o)=>{var length = Object.keys(o.years).length; $('#first,#second').html(`College of ${o.department.name}`); $('#third').html(`College of ${o.department.name} &middot; ${o.years[0]} to ${o.years[length - 1]}`);},'json');}
    function report(a,b){$.post(`reports/${b}`,{dept:a},(o)=>{$(`#report_${b}`).html(o);},'html');}
    function replace(a){$(a).on('click', function(e){e.preventDefault();window.location=$(this).data('target')})}
    function calendar(a){$(a).calendar({type:'date'})}
    function autocomplete(a){var b=new google.maps.places.Autocomplete(a,{componentRestrictions:{'country':'PH'},fields:['name']});b.addListener('place_changed',()=>{a.val=b.getPlace().name})}
    function departments(a){$(a).dropdown({onChange:(value)=>{$.ajax({url:`${origin}/a/departments/fetch`,method:'POST',data:{value:value},success:(result)=>{$('#dept').html(result)},error:(result)=>{console.log(result)}})}})}
    function image(a){$(a).on('change', function(e){loadImage(e.target.files[0],(canvas)=>{var a=canvas.toDataURL('image/jpeg');a.replace(/^data\:image\/\w+\;base64\,/,'');$('#preview').attr('src',a)},{canvas:true,orientation:true})})};
});

document.write("<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyAIoMfuY9nz00ggPoV5e7Pq58cB5GPqaXw&libraries=places' async defer></script>");
