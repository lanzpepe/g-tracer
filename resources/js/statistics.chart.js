import { Chartisan, ChartisanHooks } from '@chartisan/chartjs'

const query = document.getElementById('srch_dept') ? `?dept=${document.querySelector('#srch_dept').value}` : '';

const config1 = {
    el: '#statistics',
    hooks: new ChartisanHooks()
        .colors()
        .beginAtZero(true, 'y')
        .legend({ position: 'bottom' })
        .datasets([{ type: 'bar', stack: true }])
        .tooltip({ intersect: false, mode: 'index' })
};

const config2 = {
    el: '#employment',
    hooks: new ChartisanHooks()
        .colors()
        .beginAtZero(true, 'y')
        .legend({ position: 'bottom' })
        .datasets('bar')
        .tooltip({ intersect: false, mode: 'index' })
};

let statistics = new Chartisan(
    Object.assign(config1, { url: `${window.location.origin}/api/chart/graduates/statistics${query}` })
);

let employment = new Chartisan(
    Object.assign(config2, { url: `${window.location.origin}/api/chart/graduates/employment${query}` })
);

function updateChart() {
    fetch(`${window.location.origin}/api/chart/graduates/statistics?dept=${this.value}`, { method: 'GET' })
    .then((response) => response.json())
    .then((data) => {
        statistics.destroy();
        statistics = new Chartisan(
            Object.assign(config1, { data: data })
        )
    })
    .catch((error) => {
        console.error(error);
    });

    fetch(`${window.location.origin}/api/chart/graduates/employment?dept=${this.value}`, { method: 'GET' })
    .then((response) => response.json())
    .then((data) => {
        employment.destroy();
        employment = new Chartisan(
            Object.assign(config2, { data: data })
        )
    })
    .catch((error) => {
        console.error(error);
    });
}

document.querySelector('#srch_dept')?.addEventListener('change', updateChart);
