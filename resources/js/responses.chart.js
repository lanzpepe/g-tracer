import { Chartisan, ChartisanHooks } from '@chartisan/chartjs'

var path = window.location.pathname;
var query = path.substring(path.lastIndexOf('/') + 1);

const barHooks = new ChartisanHooks()
    .legend(false)
    .colors(['#00B5AD'])
    .beginAtZero(true, 'x')
    .datasets('horizontalBar')

const names = new Chartisan({
    el: '#companies',
    url: `${window.location.origin}/api/chart/responses/company?id=${query}`,
    hooks: barHooks
});

names.update({ background: true });

const positions = new Chartisan({
    el: '#positions',
    url: `${window.location.origin}/api/chart/responses/position?id=${query}`,
    hooks: barHooks
});

positions.update({ background: true });

const remarks = new Chartisan({
    el: '#remarks',
    url: `${window.location.origin}/api/chart/responses/remarks?id=${query}`,
    hooks: new ChartisanHooks()
        .datasets('pie')
        .pieColors()
});

remarks.update({ background: true });
