<div class="column">
    <table class="ui compact selectable striped teal table" id="graduates">
        <thead>
            <tr class="center aligned">
                <th></th>
                <th>{{ __('Name of Graduate') }}</th>
                <th>{{ __('Degree') }}</th>
                <th>{{ __('Major') }}</th>
                <th>{{ __('Graduated') }}</th>
                <th class="two wide">{{ __('Status') }}</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($graduates as $graduate)
            <tr class="center aligned" data-target="{{ route('graduates.show', $graduate) }}">
                <td><img class="ui middle aligned tiny circular image" src="{{ $graduate->image }}"></td>
                <td><strong>{{ $graduate->name }}</strong></td>
                <td>{{ $graduate->course }}</td>
                <td>{{ $graduate->major }}</td>
                <td>{{ $graduate->date_graduated }}</td>
                <td>
                    <div class="ui {{ $graduate->status == 'Open' ? 'green' : 'red' }} label">
                        {{ $graduate->status }}
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
