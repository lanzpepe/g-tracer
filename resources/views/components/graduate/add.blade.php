<div class="ui overlay fullscreen modal" id="graduateModal" tabindex="-1" role="dialog" aria-labelledby="graduateModalLabel" aria-hidden="true">
    <div class="header" id="graduateModalLabel">
        <i class="question circle outline teal icon"></i><span class="title">{{ __('Add Graduate') }}</span>
    </div>
    <div class="scrolling content" role="document">
        <form action="{{ route('graduates.store') }}" class="ui form" id="graduateForm" method="POST" role="form" enctype="multipart/form-data">
            @csrf
            <input type="hidden" id="person" name="person" value="">
            <div class="ui equal width centered grid">
                <div class="five wide column">
                    <x-image />
                </div>
                <div class="column">
                    <div class="required field">
                        <label for=""><i class="graduation cap teal icon"></i>{{ __('Date Graduated') }}</label>
                        <div class="equal width fields">
                            <div class="field">
                                <select name="year" id="year" class="ui fluid dropdown" required>
                                    @foreach ($years as $year)
                                    <option value="{{ $year }}">{{ $year }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="field month">
                                <select name="month" id="month" class="ui fluid dropdown" required>
                                    @foreach ($months as $month)
                                    <option value="{{ $month }}">{{ date('F', mktime(0, 0, 0, $month, 10)) }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="six wide field day">
                                <select name="day" id="day" class="ui fluid dropdown" required>
                                    @foreach ($days as $day)
                                    <option value="{{ $day }}">{{ $day }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="equal width fields">
                        <div class="required field">
                            <label for="lastname"><i class="user outline teal icon"></i>{{ __('Last Name') }}</label>
                            <input type="text" name="lastname" id="lastname" value="{{ old('lastname') }}" required>
                        </div>
                        <div class="required field">
                            <label for="firstname"><i class="user outline teal icon"></i>{{ __('First Name') }}</label>
                            <input type="text" name="firstname" id="firstname" value="{{ old('firstname') }}" required>
                        </div>
                        <div class="six wide field">
                            <label for="midname"><i class="user outline teal icon"></i>{{ __('Middle Name') }}</label>
                            <input type="text" name="midname" id="midname" value="{{ old('midname') }}">
                        </div>
                    </div>
                    <div class="equal width fields">
                        <div class="required field">
                            <label for="gender"><i class="venus mars teal icon"></i>{{ __('Gender') }}</label>
                            <select name="gender" class="ui fluid dropdown gender" required>
                                @foreach ($genders as $gender)
                                <option value="{{ $gender->name }}">{{ $gender->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="field">
                            <label for="contact"><i class="mobile alternate teal icon"></i>{{ __('Contact No.') }}</label>
                            <input type="number" name="contact" id="contact" value="{{ old('contact') }}" min="0">
                        </div>
                    </div>
                    <div class="required field">
                        <label for="address"><i class="home teal icon"></i>{{ __('Address (House #/Street/Subdivision, Barangay, City/Municipality, Province)') }}</label>
                        <input type="text" name="address" id="address" value="{{ old('address') }}" required>
                    </div>
                    <div class="equal width fields">
                        <div class="required field">
                            <label for="course"><i class="book reader teal icon"></i>{{ __('Course') }}</label>
                            <select name="course" class="ui fluid dropdown course" required>
                                <option value="" selected>{{ __('- Select Course -') }}</option>
                                @foreach ($courses->unique('title') as $course)
                                <option value="{{ $course->code }}">{{ $course->title }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="equal width fields">
                        <div class="required field">
                            <label for="major"><i class="award teal icon"></i>{{ __('Major') }}</label>
                            <select name="major" class="ui fluid dropdown major" required>
                                <option value="" selected>{{ __('- Select Major -') }}</option>
                                @foreach ($courses->unique('major') as $course)
                                <option value="{{ $course->major }}">{{ $course->major }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="actions">
        <button type="button" class="ui red cancel inverted button">
            <i class="close icon"></i>{{ __('Cancel') }}
        </button>
        <button type="submit" class="ui green submit inverted button" form="graduateForm" id="btnGraduate" name="btnGraduate" value="added">
            <i class="check icon"></i><span class="label">{{ __('Submit') }}</span>
        </button>
    </div>
</div>
