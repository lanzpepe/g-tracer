<div class="ui modal" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel" aria-hidden="true">
    <div class="header" id="confirmModalLabel">
        <i class="question circle outline teal icon"></i><span class="title">{{ __('Confirm Graduate') }}</span>
    </div>
    <div class="scrolling content" role="document">
        <form action="{{ route('g.confirm', ['id' => $graduate]) }}" class="ui form" id="confirmForm" method="POST" role="form">
            @csrf
            <input type="hidden" name="confirm" id="confirm" value="">
            <div class="ui equal width grid">
                <div class="ten wide column">
                    <div class="required field status">
                        <label for="status">{{ __('Employment Status') }}</label>
                        <select name="status" id="status" class="ui fluid dropdown" role="combobox">
                            @foreach ($statuses as $status)
                            <option value="{{ $status->description }}">{{ $status->description }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="employed">
                        <div class="fields">
                            <div class="sixteen wide required field coname">
                                <label for="coname"><i class="building outline teal icon"></i>{{ __('Company Name') }}</label>
                                <select name="coname" id="coname" class="ui fluid search dropdown" role="combobox">
                                    <option value="{{ old('coname') }}">{{ __('Name') }}</option>
                                    @foreach ($companies->unique('name') as $company)
                                    <option value="{{ $company->name }}">{{ $company->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="fields">
                            <div class="sixteen wide required field coaddress">
                                <label for="coaddress"><i class="map marker alternate teal icon"></i>{{ __('Company Address') }}</label>
                                <input type="text" name="coaddress" id="coaddress" value="{{ old('coaddress') }}" required>
                            </div>
                        </div>
                        <div class="fields">
                            <div class="sixteen wide required field">
                                <label for="cocontact"><i class="phone alternate teal icon"></i>{{ __('Company Contact') }}</label>
                                <input type="number" name="cocontact" id="cocontact" value="{{ old('cocontact') }}" min="0" required>
                            </div>
                        </div>
                        <div class="fields">
                            <div class="sixteen wide required field position">
                                <label for="position"><i class="briefcase teal icon"></i>{{ __('Position/Designation') }}</label>
                                <select id="position" name="position" class="ui fluid search dropdown" role="combobox">
                                    <option value="{{ old('position') }}">{{ __('Designation') }}</option>
                                    @foreach ($jobs as $job)
                                    <option value="{{ $job->name }}">{{ $job->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="fields">
                            <div class="sixteen wide required field">
                                <label for="datehired"><i class="calendar alternate outline teal icon"></i>{{ __('Date Hired') }}</label>
                                <div class="ui calendar" id="datehired">
                                    <input type="text" id="datehired" name="datehired" value="{{ Carbon\Carbon::now()->format('F d, Y') }}" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fields">
                        <div class="sixteen wide required remark field">
                            <label for="remarks"><i class="comment alternate outline teal icon"></i>{{ __('Remarks') }}</label>
                            <textarea name="remarks" id="remarks" rows="2" value="{{ old('remarks') }}" required></textarea>
                        </div>
                    </div>
                </div>
                <div class="six wide column">
                    <div class="ui teal segment">
                        @if ($responses->isNotEmpty())
                        <h4 class="ui centered header">{{ __('Topmost Answers Based on Category') }}</h4>
                        <div class="ui relaxed list">
                            <strong>{{ __('Company Name:') }}</strong>
                            <div class="item">
                                <div class="ui company checkbox">
                                    <input type="checkbox" name="companycb" id="companycb" value="{{ Str::of($results->keys()->get(0))->explode(',+')[0] }}">
                                    <label for="companycb">{{ Str::of($results->keys()->get(0))->explode(',+')[0] }}</label>
                                </div>
                            </div>
                            <strong>{{ __('Company Address:') }}</strong>
                            <div class="item">
                                <div class="ui address checkbox">
                                    <input type="checkbox" name="addresscb" id="addresscb" value="{{ Str::of($results->keys()->get(0))->explode(',+')[1] }}">
                                    <label for="addresscb">{{ Str::of($results->keys()->get(0))->explode(',+')[1] }}</label>
                                </div>
                            </div>
                            <strong>{{ __('Job Position:') }}</strong>
                            <div class="item">
                                <div class="ui position checkbox">
                                    <input type="checkbox" name="positioncb" id="positioncb" value="{{ $results->keys()->get(1) }}">
                                    <label for="positioncb">{{ $results->keys()->get(1) }}</label>
                                </div>
                            </div>
                        </div>
                        @else
                        <h4 class="ui centered header">{{ __('No responses yet.') }}</h4>
                        @endif
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="actions">
        <button type="button" class="ui red cancel inverted button">
            <i class="close icon"></i>{{ __('Cancel') }}
        </button>
        <button type="submit" class="ui green submit inverted button" form="confirmForm" name="btnConfirm" id="btnConfirm" value="confirmed" role="button">
            <i class="check icon"></i><span class="label">{{ __('Confirm') }}</span>
        </button>
    </div>
</div>
