<div class="ui modal" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="uploadModalLabel" aria-hidden="true">
    <div class="header" id="uploadModalLabel">
        <i class="question circle outline teal icon"></i> {{ __('Import Graduate List') }}
    </div>
    <div class="content" role="document">
        <form action="{{ route('g.preview') }}" class="ui form" id="uploadForm" method="POST" role="form" enctype="multipart/form-data">
            @csrf
            <div class="equal width fields">
                <div class="required field">
                    <label for="_school"><i class="school teal icon"></i>{{ __('School') }}</label>
                    <select name="_school" id="_school" class="ui disabled fluid dropdown" required>
                        <option value="{{ Auth::user()->school->name }}">{{ Auth::user()->school->name }}</option>
                    </select>
                </div>
                <div class="required field">
                    <label for="_dept"><i class="building teal icon"></i>{{ __('Department') }}</label>
                    <select name="_dept" id="_dept" class="ui disabled fluid dropdown" required>
                        <option value="{{ Auth::user()->department->name }}">{{ Auth::user()->department->name }}</option>
                    </select>
                </div>
            </div>
            <div class="equal width fields">
                <div class="required field">
                    <label for="_course"><i class="book reader teal icon"></i>{{ __('Course') }}</label>
                    <select name="_course" id="_course" class="ui fluid dropdown">
                        @foreach ($courses->unique('title') as $course)
                        <option value="{{ $course->code }}">{{ $course->title }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="required field">
                    <label for="_major"><i class="award teal icon"></i>{{ __('Major') }}</label>
                    <select name="_major" id="_major" class="ui fluid dropdown">
                        @foreach ($courses->unique('major') as $course)
                        <option value="{{ $course->major }}">{{ $course->major }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="equal width fields">
                <div class="required field">
                    <label for=""><i class="graduation cap teal icon"></i>{{ __('Graduation Date') }}</label>
                    <div class="equal width fields">
                        <div class="field">
                            <select name="_year" id="_year" class="ui fluid dropdown" required>
                                @foreach ($years as $year)
                                <option value="{{ $year }}">{{ $year }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="field">
                            <select name="_month" id="_month" class="ui fluid dropdown" required>
                                @foreach ($months as $month)
                                <option value="{{ $month }}">{{ date('F', mktime(0, 0, 0, $month, 10)) }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="field">
                            <select name="_day" id="_day" class="ui fluid dropdown" required>
                                @foreach ($days as $day)
                                <option value="{{ $day }}">{{ $day }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="required field">
                    <label for="_file"><i class="file outline teal icon"></i>{{ __('Upload File (.csv)') }}</label>
                    <input type="file" accept=".csv" name="_file" id="_file" required>
                </div>
            </div>
        </form>
        <div class="ui small yellow message">
            {{ __('Note: CSV template can be downloaded from the Templates folder under Shared Files in the File Manager menu.') }}
        </div>
    </div>
    <div class="actions">
        <button type="button" class="ui red cancel inverted button">
            <i class="close icon"></i>{{ __('Cancel') }}
        </button>
        <button type="submit" class="ui green submit inverted button" form="uploadForm">
            <i class="check icon"></i> {{ __('Submit') }}
        </button>
    </div>
</div>
