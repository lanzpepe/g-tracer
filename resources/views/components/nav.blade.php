<!-- Navigation bar -->
<div class="ui fluid container">
    <div class="ui fixed borderless inverted teal menu" id="navbar" role="navigation">
        <div class="toc item">
            <a class="ui teal icon button">
                <i class="large sidebar icon"></i>
            </a>
        </div>
        <a href="{{ $admin ? route('admin.home') : route('dept.home') }}" class="left header logo item">
            <img src="{{ asset('img/app/logo.png') }}" alt="App Logo" class="logo" role="img">
            <span class="ui text"> {{ __('G-Tracer') }}</span>
        </a>
        <div class="ui centered search item">
            <div class="ui icon input">
                <input type="text" class="prompt" placeholder="Search" role="search">
                <i class="search link teal icon"></i>
            </div>
            <div class="results"></div>
        </div>
        <div class="right item">
            @if (Auth::user()->role === config('constants.roles.dept'))
            <div class="notification">
                <a class="ui pointing scrolling dropdown circular icon teal button">
                    <i class="large globe asia icon"></i>
                    @if ($count > 0)
                    <div class="ui floating circular red mini label notification-count">{{ $count }}</div>
                    @endif
                    <div class="menu notifications" style="right: 0;left: auto;">
                        <div class="header">{{ __('Notifications') }}</div>
                        <div class="ui divider"></div>
                        <div class="item"></div>
                    </div>
                </a>
            </div>
            @endif
        </div>
    </div>
</div>
<!-- End navigation bar -->
