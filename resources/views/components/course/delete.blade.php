<div class="ui tiny basic modal" id="markCourseModal" tabindex="-1" role="dialog" aria-labelledby="markCourseModalLabel" aria-hidden="true">
    <div class="ui icon header" id="markCourseModalLabel">
        <i class="exclamation triangle red icon"></i>{{ __('Remove Degree Program') }}
    </div>
    <div class="content" role="document">
        <form action="#" class="ui form" id="deleteForm" method="POST" role="form">
            @csrf
            @method('DELETE')
            <h3>{{ __('The following entries will be removed:') }}</h3>
            <p class="course code"></p>
            <p class="course title"></p>
            <p class="course major"></p>
            <h3>{{ __('Proceed anyway?') }}</h3>
        </form>
    </div>
    <div class="actions">
        <button type="button" class="ui green cancel basic inverted button">
            <i class="close icon"></i>{{ __('Cancel') }}
        </button>
        <button type="submit" class="ui red submit inverted button delete-course" form="deleteForm">
            <i class="trash icon"></i> {{ __('Delete') }}
        </button>
    </div>
</div>
