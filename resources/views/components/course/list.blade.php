<div class="column">
    <div class="ui three stackable horizontal cards">
        @foreach ($courses as $course)
        <div class="card">
            <div class="image">
                <img src="{{ asset('img/app/logo.png') }}" alt="Logo">
            </div>
            <div class="content">
                <div class="header">{{ $course->code }}</div>
                <div class="meta">
                    <span>{{ __("updated {$course->updated_at->diffForHumans()}") }}</span>
                </div>
                <div class="description">
                    @if ($course->major === "None")
                    <p>{{ $course->title }}</p>
                    @else
                    <p>{{ "{$course->title} - {$course->major}" }}</p>
                    @endif
                </div>
            </div>
            <div class="extra content">
                <div class="ui attached two buttons">
                    <button class="ui green tertiary button edit-course" data-value="{{ $course->id }}">
                        <i class="pen icon"></i> {{ __('Edit') }}
                    </button>
                    <button class="ui red tertiary button mark-course" data-value="{{ $course->id }}">
                        <i class="trash icon"></i> {{ __('Remove') }}
                    </button>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
{{ $courses->links() }}
