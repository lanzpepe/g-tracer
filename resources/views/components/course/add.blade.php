<div class="ui small modal" id="courseModal" role="dialog" aria-labelledby="courseModalLabel" aria-hidden="true">
    <div class="header" id="courseModalLabel">
        <i class="question circle outline teal icon"></i><span class="title">{{ __('Add New Degree Program') }}</span>
    </div>
    <div class="content" role="document">
        <form action="{{ route('courses.store') }}" class="ui form" id="courseForm" method="POST" role="form">
            @csrf
            <div class="equal width fields">
                <div class="required field school">
                    <label for="school"><i class="school teal icon"></i>{{ __('School') }}</label>
                    <select id="school" name="school" class="ui disabled fluid dropdown">
                        <option value="{{ Auth::user()->school->name }}">{{ Auth::user()->school->name }}</option>
                    </select>
                </div>
                <div class="required field dept">
                    <label for="dept"><i class="building teal icon"></i>{{ __('Department') }}</label>
                    <select id="dept" name="dept" class="ui disabled fluid dropdown">
                        <option value="{{ Auth::user()->department->name }}">{{ Auth::user()->department->name }}</option>
                    </select>
                </div>
            </div>
            <div class="required field">
                <label for="title"><i class="book reader teal icon"></i>{{ __('Degree Title') }}</label>
                <select name="title" id="title" class="ui fluid search dropdown">
                    <option value="" selected>{{ __('') }}</option>
                    @foreach ($courses->unique('title') as $course)
                    <option value="{{ $course->title }}">{{ $course->title }}</option>
                    @endforeach
                </select>
            </div>
            <div class="equal width fields">
                <div class="field">
                    <label for="major"><i class="medal teal icon"></i>{{ __('Degree Major (if applicable)') }}</label>
                    <select name="major" id="major" class="ui fluid search dropdown">
                        <option value="" selected>{{ __('') }}</option>
                        @foreach ($courses->unique('major') as $course)
                        <option value="{{ $course->major }}">{{ $course->major }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="required field">
                    <label for="code"><i class="book reader teal icon"></i>{{ __('Degree Code') }}</label>
                    <select name="code" id="code" class="ui fluid search dropdown">
                        <option value="" selected>{{ __('') }}</option>
                        @foreach ($courses->unique('code') as $course)
                        <option value="{{ $course->code }}">{{ $course->code }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </form>
    </div>
    <div class="actions">
        <button type="button" class="ui red cancel inverted button" role="button">
            <i class="close icon"></i>{{ __('Cancel') }}
        </button>
        <button type="submit" class="ui green submit inverted button" form="courseForm" id="btnCourse" name="btnCourse" value="added" role="button">
            <i class="check icon"></i><span class="label">{{ __('Submit') }}</span>
        </button>
    </div>
</div>
