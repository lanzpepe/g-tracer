<div class="column">
    <div class="ui three horizontal stackable cards">
        @foreach ($schools as $school)
        <div class="card">
            <div class="ui centered image">
                <img src="{{ $school->image }}">
            </div>
            <div class="center aligned content">
                <div class="center aligned header">{{ $school->name }}</div>
                <div class="center aligned meta">{{ __("updated {$school->updated_at->diffForHumans()}") }}</div>
                <div class="center aligned description">{{ __('University/College') }}</div><br>
            </div>
            <div class="extra content">
                <div class="ui attached two buttons">
                    <button class="ui green tertiary button edit-school" data-value="{{ $school->id }}">
                        <i class="pen icon"></i> {{ __('Edit') }}
                    </button>
                    <button class="ui red tertiary button mark-school" data-value="{{ $school->id }}">
                        <i class="trash icon"></i> {{ __('Remove') }}
                    </button>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
{{ $schools->links() }}
