<div class="ui tiny modal" id="schoolModal" tabindex="-1" role="dialog" aria-labelledby="schoolModalLabel" aria-hidden="true">
    <div class="header" id="schoolModalLabel">
        <i class="question circle outline teal icon"></i><span class="title">{{ __('Add New School') }}</span>
    </div>
    <div class="content" role="document">
        <form action="{{ route('schools.store') }}" class="ui form" id="schoolForm" method="POST" role="form" enctype="multipart/form-data">
            @csrf
            <input type="hidden" id="school" name="school" value="">
            <div class="required field">
                <label for="name"><i class="school teal icon"></i>{{ __('School Name') }}</label>
                <input type="text" name="name" id="name" value="{{ old('name') }}" required>
            </div>
            <div class="field">
                <label for="logo"><i class="file image teal icon"></i>{{ __('School Logo (if applicable)') }}</label>
                <input type="file" name="logo" id="logo" accept="image/*">
            </div>
        </form>
    </div>
    <div class="actions">
        <button type="button" class="ui red cancel inverted button">
            <i class="close icon"></i>{{ __('Cancel') }}
        </button>
        <button type="submit" class="ui green submit inverted button" form="schoolForm" id="btnSchool" name="btnSchool" value="added">
            <i class="check icon"></i><span class="label">{{ __('Submit') }}</span>
        </button>
    </div>
</div>
