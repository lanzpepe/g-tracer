<div class="ui tiny modal" id="deptModal" tabindex="-1" role="dialog" aria-labelledby="deptModalLabel" aria-hidden="true">
    <div class="header" id="deptModalLabel">
        <i class="question circle outline teal icon"></i><span class="title">{{ __('Add New Department') }}</span>
    </div>
    <div class="content" role="document">
        <form action="{{ route('departments.store') }}" class="ui form" id="deptForm" method="POST" role="form" enctype="multipart/form-data">
            @csrf
            <input type="hidden" id="department" name="department" value="">
            <div class="required field school">
                <label for="school"><i class="school teal icon"></i>{{ __('School') }}</label>
                <select name="school" id="school" class="ui fluid dropdown" required>
                    <option value="" selected>{{ __('-- Select School --') }}</option>
                    @foreach ($schools as $school)
                    <option value="{{ $school->name }}">{{ $school->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="required field dept">
                <label for="department"><i class="building teal icon"></i>{{ __('Department Name') }}</label>
                <select name="dept" id="dept" class="ui fluid search dropdown" required>
                    <option value="" selected>{{ __('-- Select Department --') }}</option>
                </select>
            </div>
            <div class="field">
                <label for="logo"><i class="file image teal icon"></i>{{ __('Department Logo (if applicable)') }}</label>
                <input type="file" accept="image/*" name="logo" id="logo">
            </div>
        </form>
    </div>
    <div class="actions">
        <button type="button" class="ui red cancel inverted button">
            <i class="close icon"></i>{{ __('Cancel') }}
        </button>
        <button type="submit" class="ui green submit inverted button" form="deptForm" id="btnDept" name="btnDept" value="added">
            <i class="check icon"></i><span class="label">{{ __('Submit') }}</span>
        </button>
    </div>
</div>
