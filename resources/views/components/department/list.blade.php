<div class="column">
    <div class="ui three horizontal stackable cards">
        @foreach ($depts as $dept)
        @foreach ($dept->schools as $school)
        <div class="card">
            <div class="ui centered image">
                <img src="{{ $dept->image }}">
            </div>
            <div class="center aligned content">
                <div class="header">{{ $dept->name }}</div>
                <div class="meta">{{ __("updated {$dept->updated_at->diffForHumans()}") }}</div>
                <div class="description">{{ $school->name }}</div>
            </div>
            <div class="extra content">
                <div class="ui attached two buttons">
                    <button class="ui green tertiary button edit-dept" data-value="{{ __("{$dept->id}+{$school->id}") }}">
                        <i class="pen icon"></i> {{ __('Edit') }}
                    </button>
                    <button class="ui red tertiary button mark-dept" data-value="{{ __("{$dept->id}+{$school->id}") }}">
                        <i class="trash icon"></i> {{ __('Remove') }}
                    </button>
                </div>
            </div>
        </div>
        @endforeach
        @endforeach
    </div>
</div>
{{ $depts->links() }}
