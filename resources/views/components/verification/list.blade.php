<div class="column">
    <table class="ui celled compact selectable striped teal table" id="requests">
        <thead>
            <tr class="center aligned">
                <th>{{ __('Request ID') }}</th>
                <th>{{ __('Requester\'s Name') }}</th>
                <th>{{ __('Degree Program') }}</th>
                <th>{{ __('Major') }}</th>
                <th>{{ __('Date Graduated') }}</th>
                <th>{{ __('Status') }}</th>
                <th>{{ __('Actions') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($verifications as $data)
            <tr class="center aligned">
                <td>{{ $data->id }}</td>
                <td><strong>{{ $data->person->full_name }}</strong></td>
                <td>{{ $data->course->title }}</td>
                <td>{{ $data->course->major }}</td>
                <td>{{ "{$data->month} {$data->year}" }}</td>
                <td>
                    <div class="ui {{ $data->status ? 'green' : 'red' }} label">
                        {{ $data->status ? 'Approved' : 'Pending' }}
                    </div>
                </td>
                <td>
                    <button class="ui teal tertiary button verify-info" data-value="{{ $data->id }}">
                        <i class="eye icon"></i>{{ __('View Info') }}
                    </button>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
