<div class="ui fluid container">
    <div class="ui three stackable horizontal centered cards">
        <a href="{{ route('admin.reports') }}" class="card">
            <div class="ui large teal icon message">
                <i class="tachometer alternate teal icon"></i>
                <div class="content">
                    <div class="header">
                        {{ __('Dashboard & Reports') }}
                    </div>
                </div>
            </div>
        </a>
        <a href="{{ route('users.index') }}" class="card">
            <div class="ui large teal icon message">
                <i class="users teal icon"></i>
                <div class="content">
                    <div class="header">
                        {{ __('Registered Users') }}
                    </div>
                </div>
            </div>
        </a>
        <a href="{{ route('accounts.index') }}" class="card">
            <div class="ui large teal icon message">
                <i class="users cog teal icon"></i>
                <div class="content">
                    <div class="header">
                        {{ __('Account Management') }}
                    </div>
                </div>
            </div>
        </a>
        <a href="{{ route('linkedin.index') }}" class="card">
            <div class="ui large teal icon message">
                <i class="linkedin in teal icon"></i>
                <div class="content">
                    <div class="header">
                        {{ __('LinkedIn Data') }}
                    </div>
                </div>
            </div>
        </a>
        <a href="{{ route('departments.index') }}" class="card">
            <div class="ui large teal icon message">
                <i class="building teal icon"></i>
                <div class="content">
                    <div class="header">
                        {{ __('Departments') }}
                    </div>
                </div>
            </div>
        </a>
        <a href="{{ route('admin.fm') }}" class="card">
            <div class="ui large teal icon message">
                <i class="folder open teal icon"></i>
                <div class="content">
                    <div class="header">
                        {{ __('File Manager') }}
                    </div>
                </div>
            </div>
        </a>
    </div>
</div>
