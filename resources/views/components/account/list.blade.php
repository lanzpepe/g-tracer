<div class="column">
    <table class="ui compact selectable celled teal table">
        <thead>
            <tr class="center aligned">
                <th>{{ __('Access Level') }}</th>
                <th>{{ __('Username') }}</th>
                <th>{{ __('Name') }}</th>
                <th>{{ __('Department') }}</th>
                <th>{{ __('School') }}</th>
                <th>{{ __('Actions') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($admins as $adm)
                @foreach ($adm->departments as $dept)
                    <tr class="center aligned">
                        <td>{{ $adm->role }}</td>
                        <td>{{ $adm->username }}</td>
                        <td>{{ $adm->user->full_name }}</td>
                        <td>{{ $dept->name }}</td>
                        <td>{{ $adm->school->name }}</td>
                        <td>
                            <button class="ui compact icon green inverted button edit-account" data-value="{{ $adm->admin_id }}">
                                <i class="pen icon"></i>
                            </button>
                            <button class="ui compact icon red inverted button mark-account" data-value="{{ $adm->admin_id }}">
                                <i class="trash icon"></i>
                            </button>
                        </td>
                    </tr>
                @endforeach
            @endforeach
        </tbody>
    </table>
</div>
{{ $admins->links() }}
