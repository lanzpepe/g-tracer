<div class="ui small modal" id="linkedinModal" tabindex="-1" role="dialog" aria-labelledby="linkedinModalLabel" aria-hidden="true">
    <div class="header" id="linkedinModalLabel">
        <i class="user teal icon"></i>{{ __('LinkedIn Profile') }}
    </div>
    <div class="content" role="document">
        <div class="ui fluid container">
            <div class="ui equal width stackable grid basic segment">
                <div class="two wide column">
                    <img src="" alt="Profile Image" id="profileImage" class="ui centered small circular image">
                </div>
                <div class="left floated column">
                    <h2 class="ui teal header">
                        <span id="profileName"></span>
                    </h2>
                    <div>
                        <span class="ui teal text">{{ __('LinkedIn ID: ') }}</span>
                        <span id="profileID"></span>
                    </div>
                    <div>
                        <span class="ui teal text">{{ __('Profile URL: ') }}</span>
                        <span id="profileUrl"></span>
                    </div>
                    <div>
                        <span class="ui teal text">{{ __('Company: ') }}</span>
                        <span id="profileCompany"></span>
                    </div>
                    <div>
                        <span class="ui teal text">{{ __('Company Position: ') }}</span>
                        <span id="profilePosition"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="actions">
        <button type="button" class="ui red cancel inverted button">
            <i class="close icon"></i>{{ __('Close') }}
        </button>
    </div>
</div>
