<div class="ui modal" id="linkedInUploadModal" tabindex="-1" role="dialog" aria-labelledby="linkedInUploadModalLabel" aria-hidden="true">
    <div class="header" id="linkedInUploadModalLabel">
        <i class="question circle outline teal icon"></i> {{ __('Import LinkedIn Data') }}
    </div>
    <div class="content" role="document">
        <form action="{{ route('in.parse') }}" class="ui form" id="uploadInForm" method="POST" role="form" enctype="multipart/form-data">
            @csrf
            <div class="required field">
                <label for="file"><i class="linkedin in teal icon"></i>{{ __('LinkedIn Data (.csv)') }}</label>
                <input type="file" accept=".csv" name="file" id="file" required>
            </div>
            <div class="ui blue message">
                <p><strong>{{ __('NOTE:') }}</strong></p>
                <p>{{ __('Use Linked Helper app to scrape public data from LinkedIn, and the extracted data will be uploaded here.') }}</p>
                <p>{{ __('For download and installation process, click ') }}<a href="https://lh2.linkedhelper.com/downloads.html" target="_blank"><strong>{{ __('here.') }}</strong></a></p>
            </div>
        </form>
    </div>
    <div class="actions">
        <button type="button" class="ui red cancel inverted button">
            <i class="close icon"></i>{{ __('Cancel') }}
        </button>
        <button type="submit" class="ui green submit inverted button" form="uploadInForm">
            <i class="check icon"></i> {{ __('Submit') }}
        </button>
    </div>
</div>
