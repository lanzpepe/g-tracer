@if ($profiles->isNotEmpty())
<div class="column">
    <table class="ui celled compact selectable striped teal table" id="linkedin">
        <thead>
            <tr class="center aligned">
                <th>{{ __('ID') }}</th>
                <th>{{ __('First Name') }}</th>
                <th>{{ __('Last Name') }}</th>
                <th>{{ __('Title') }}</th>
                <th class="two wide">{{ __('Actions') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($profiles as $profile)
            <tr class="center aligned">
                <td>{{ $profile->id }}</td>
                <td>{{ $profile->first_name }}</td>
                <td>{{ $profile->last_name }}</td>
                <td>{{ $profile->title }}</td>
                <td>
                    <button class="ui teal tertiary button view-info" data-value="{{ $profile->id }}">
                        <i class="eye icon"></i>{{ __('View Info') }}
                    </button>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@else
<div class="ui placeholder basic segment">
    <div class="ui icon header">
        <i class="exclamation circle teal icon"></i>
        {{ __('Click the \'Import LinkedIn Data\' button to display it here.') }}
    </div>
</div>
@endif
