<div class="column">
    <div class="ui three stackable horizontal cards">
        @foreach ($courseJobs as $job)
        <div class="card">
            <div class="image">
                <img src="{{ asset('img/app/logo.png') }}" alt="Logo">
            </div>
            <div class="content">
                <div class="header">{{ $job->name }}</div>
                <div class="meta">{{ __("updated {$job->updated_at->diffForHumans()}") }}</div>
                <div class="description">
                    <p>{{ __('Related Course(s): ') }}</p>
                    <p>
                        @foreach ($job->courses as $course)
                        <span>{{ $course->code }}</span>
                        @endforeach
                    </p>
                </div>
            </div>
            <button class="ui bottom attached red inverted button mark-job" data-value="{{ $job->id }}">
                <i class="trash icon"></i> {{ __('Remove') }}
            </button>
        </div>
        @endforeach
    </div>
</div>
{{ $courseJobs->links() }}
