<div class="ui tiny modal" id="jobModal" tabindex="-1" role="dialog" aria-labelledby="jobModalLabel" aria-hidden="true">
    <div class="header" id="jobModalLabel">
        <i class="question circle outline teal icon"></i><span class="title">{{ __('Add Job') }}</span>
    </div>
    <div class="content" role="document">
        <form action="{{ route('jobs.store') }}" class="ui form" id="jobForm" method="POST" role="form">
            @csrf
            <div class="required field course">
                <label for="course"><i class="book reader teal icon"></i>{{ __('Related Course(s)') }}</label>
                <div class="ui fluid multiple selection dropdown" id="course">
                    <input type="hidden" name="course">
                    <i class="dropdown icon"></i>
                    <div class="default text">{{ __('- Select Course -') }}</div>
                    <div class="menu">
                        @foreach ($courses as $course)
                            <div class="item" data-value="{{ $course->code }}">{{ $course->code }}</div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="required field job">
                <label for="job"><i class="briefcase teal icon"></i>{{ __('Job Name') }}</label>
                <select name="job" id="job" class="ui fluid search dropdown">
                    <option value="" selected>{{ __('Job Name') }}</option>
                    @foreach ($jobs as $job)
                        <option value="{{ $job->name }}">{{ $job->name }}</option>
                    @endforeach
                </select>
            </div>
        </form>
    </div>
    <div class="actions">
        <button type="button" class="ui red cancel inverted button">
            <i class="close icon"></i>{{ __('Cancel') }}
        </button>
        <button type="submit" class="ui green submit inverted button" form="jobForm">
            <i class="check icon"></i><span class="label">{{ __('Submit') }}</span>
        </button>
    </div>
</div>
