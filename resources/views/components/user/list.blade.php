@if ($users->isNotEmpty())
<div class="column">
    <table class="ui celled compact selectable striped teal table">
        <thead>
            <tr class="center aligned">
                <th>{{ __('Last Name') }}</th>
                <th>{{ __('First Name') }}</th>
                <th>{{ __('M.I.') }}</th>
                <th>{{ __('Date Registered') }}</th>
                <th class="four wide">{{ __('User ID') }}</th>
                <th>{{ __('Actions') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($users as $user)
            <tr class="center aligned">
                <td>{{ $user->person->last_name }}</td>
                <td>{{ $user->person->first_name }}</td>
                <td>{{ $user->person->middle_name }}</td>
                <td>{{ $user->created_at->toFormattedDateString() }}</td>
                <td>{{ $user->user_id }}</td>
                <td>
                    <button class="ui teal tertiary button">
                        <i class="eye icon"></i> {{ __('View Info') }}
                    </button>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
{{ $users->links() }}
@else
<div class="ui placeholder basic segment">
    <div class="ui icon header">
        <i class="exclamation circle teal icon"></i>
        {{ __('No registered users yet.') }}
    </div>
</div>
@endif
