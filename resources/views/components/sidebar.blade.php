<!-- Sidebar -->
<div class="ui middle aligned vertical borderless inverted teal sidebar menu">
    <div class="header item">
        <div class="ui equal width middle aligned grid container">
            <div class="row">
                <div class="six wide column">
                    <img src="{{ asset('img/app/logo.png') }}" alt="App Logo" class="ui tiny image" role="img">
                </div>
                <div class="column">
                    <span class="ui large text"> {{ __('G-Tracer') }}</span>
                </div>
            </div>
        </div>
    </div>
    <div class="header item">
        <div class="ui equal width grid container">
            <div class="center aligned column">
                <span class="ui text">{{ __('Welcome, ' . Auth::user()->name) }}</span>
            </div>
        </div>
    </div>
    <a href="{{ $admin ? route('admin.profile') : route('dept.profile') }}" class="{{ request()->is('a/profile') || request()->is('d/profile') ? 'item active' : 'item' }}" role="link">
        <i class="user icon"></i> {{ __('User Profile') }}
    </a>
    <a href="{{ $admin ? route('admin.reports') : route('dept.reports') }}" class="{{ request()->is('a/reports') || request()->is('d/reports') ? 'item active' : 'item' }}" role="link">
        <i class="tachometer alternate icon"></i> {{ __('Dashboard & Reports') }}
    </a>
    @if ($admin)
    <a href="{{ route('users.index') }}" class="{{ request()->is('a/users') ? 'item active' : 'item' }}" role="link">
        <i class="users icon"></i> {{ __('Registered Users') }}
    </a>
    <a href="{{ route('accounts.index') }}" class="{{ request()->is('a/accounts') ? 'item active' : 'item' }}" role="link">
        <i class="users cog icon"></i> {{ __('Accounts Management') }}
    </a>
    <a href="{{ route('schools.index') }}" class="{{ request()->is('a/schools') ? 'item active' : 'item' }}" role="link">
        <i class="school icon"></i> {{ __('Schools') }}
    </a>
    <a href="{{ route('departments.index') }}" class="{{ request()->is('a/departments') ? 'item active' : 'item' }}" role="link">
        <i class="building icon"></i> {{ __('Departments') }}
    </a>
    <a href="{{ route('linkedin.index') }}" class="{{ request()->is('a/linkedin') ? 'item active' : 'item' }}" role="link">
        <i class="linkedin in icon"></i> {{ __('LinkedIn') }}
    </a>
    @else
    <a href="{{ route('graduates.index') }}" class="{{ request()->is('d/graduates') ? 'item active' : 'item' }}" role="link">
        <i class="users icon"></i> {{ __('Graduate List') }}
    </a>
    <a href="{{ route('courses.index') }}" class="{{ request()->is('d/courses') ? 'item active' : 'item' }}" role="link">
        <i class="book reader icon"></i> {{ __('Degree Programs') }}
    </a>
    <a href="{{ route('jobs.index') }}" class="{{ request()->is('d/jobs') ? 'item active' : 'item' }}" role="link">
        <i class="briefcase icon"></i> {{ __('Course Related Jobs') }}
    </a>
    <a href="{{ route('requests.index') }}" class="{{ request()->is('d/requests') ? 'item active' : 'item' }}" role="link">
        <i class="user check icon"></i> {{ __('Verification Requests') }}
    </a>
    <a href="{{ route('rewards.index') }}" class="{{ request()->is('d/rewards') ? 'item active' : 'item' }}" role="link">
        <i class="gifts icon"></i> {{ __('Points & Rewards') }}
    </a>
    @endif
    <a href="{{ $admin ? route('admin.fm') : route('dept.fm') }}" class="{{ request()->is('a/file-manager') || request()->is('d/file-manager') ? 'item active' : 'item' }}" role="link">
        <i class="folder open icon"></i>{{ __('File Manager') }}
    </a>
    <div class="item">
        <form action="{{ route('logout') }}" method="POST">
            @csrf
            <button type="submit" class="ui fluid inverted button">
                <i class="sign out alternative icon"></i>{{ __('Log out') }}
            </button>
        </form>
    </div>
</div>
<!-- End sidebar -->
