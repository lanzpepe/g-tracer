<div class="column">
    <div class="ui three doubling horizontal cards">
        @foreach ($rewards as $reward)
        <div class="card">
            <div class="image">
                <img src="{{ $reward->image }}">
            </div>
            <div class="center aligned content">
                <div class="header">{{ $reward->name }}</div>
                <div class="meta">{{ "{$reward->points} points" }}</div>
                <div class="description">
                    <p>{{ $reward->description }}</p>
                    <a href="{{ route('rewards.show', $reward->id) }}" class="ui teal tertiary button">
                        {{ __('Click to view details') }} <i class="angle right teal icon"></i>
                    </a>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
{{ $rewards->links() }}
