<div class="ui tiny modal" id="pointModal" tabindex="-1" role="dialog" aria-labelledby="pointModalLabel" aria-hidden="true">
    <div class="header" id="pointModalLabel">
        <i class="question circle outline teal icon"></i><span class="title">{{ __('Point Settings') }}</span>
    </div>
    <div class="content" role="document">
        <form action="{{ route('points.store') }}" class="ui form" id="pointForm" method="POST" role="form">
            @csrf
            <div class="inline fields">
                <div class="ten wide field">
                    <label for="submitted"><span class="ui text">{{ __('Submitted response point threshold:') }}</span></label>
                </div>
                <div class="six wide field">
                    <input type="number" name="submitted" id="submitted" value="{{ old('submitted') }}" placeholder="1" min="1">
                </div>
            </div>
            <div class="inline fields">
                <div class="ten wide field">
                    <label for="confirmed"><span class="ui text">{{ __('Confirmed response point threshold:') }}</span></label>
                </div>
                <div class="six wide field">
                    <input type="number" name="confirmed" id="confirmed" value="{{ old('confirmed') }}" placeholder="1" min="1">
                </div>
            </div>
        </form>
    </div>
    <div class="actions">
        <button type="button" class="ui red cancel inverted button">
            <i class="close icon"></i>{{ __('Cancel') }}
        </button>
        <button type="submit" class="ui green submit inverted button" form="pointForm" id="btnPoint" name="btnPoint">
            <i class="check icon"></i><span class="label">{{ __('Apply') }}</span>
        </button>
    </div>
</div>
