<div class="ui mini modal" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel" aria-hidden="true">
    <div class="header" id="confirmModalLabel">
        <i class="question circle outline teal icon"></i> {{ __('Release Reward Item') }}
    </div>
    <div class="content" role="document">
        <form action="#" class="ui form" id="updateForm" method="post" role="form">
            @csrf
            @method('PATCH')
            <input type="hidden" name="reference" id="reference" value="">
            <p>{{ __('Accept reward item request?') }}</p>
        </form>
    </div>
    <div class="actions">
        <button class="ui red cancel inverted button">
            <i class="close icon"></i> {{ __('Cancel') }}
        </button>
        <button type="submit" class="ui green submit inverted button accept-item" form="updateForm">
            <i class="check icon"></i> {{ __('Accept') }}
        </button>
    </div>
</div>
