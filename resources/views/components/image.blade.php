<div class="ui special centered cards">
    <div class="card">
        <div class="blurring dimmable image">
            <div class="ui dimmer">
                <div class="content">
                    <div class="center">
                        <button type="button" class="ui teal inverted button">
                            <label for="image">{{ __('Upload Image') }}</label>
                            <input type="file" name="image" id="image" accept="image/*">
                        </button>
                    </div>
                </div>
            </div>
            <img src="{{ asset('img/app/default_avatar_m.png') }}" alt="Preview Image" id="preview">
        </div>
        <div class="content">
            <div class="center aligned header">{{ __('Graduate Image') }}</div>
        </div>
    </div>
</div>
