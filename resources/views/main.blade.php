<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge,chrome=1">
    <meta name="application-name" content="{{ env('APP_NAME') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') | {{ __('G-Tracer') }}</title>

    <link rel="icon" href="{{ asset('img/app/logo.png') }}">

    <script src="{{ asset('js/scripts.js') }}" crossorigin="anonymous"></script>
    <script src="{{ asset('semantic/dist/semantic.min.js') }}" crossorigin="anonymous" defer></script>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css">
    <link rel="stylesheet" href="{{ asset('semantic/dist/semantic.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/styles.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    @yield('stylesheets')
</head>
<body>
@yield('content')
@yield('scripts')
</body>
</html>
