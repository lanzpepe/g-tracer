@extends('main')

@section('title', 'Home')

@section('header')
<i class="external alternate teal icon"></i> {{ __('Quick Links') }}
@endsection

@section('content')
<x-sidebar :admin="$admin" />
<div class="pusher">
    <x-nav :admin="$admin" :count="$count" />
    <main role="main">
        <div class="ui fluid container" id="container">
            @if (!(Request::is('a/file-manager') || Request::is('d/file-manager')))
            <div class="ui equal width middle aligned grid basic segment">
                <div class="column">
                    <h3 class="ui left floated header title">
                        @yield('header')
                    </h3>
                </div>
                <div class="column">
                    @yield('button')
                </div>
            </div>
            @endif
            @if ($errors->any())
            <div class="ui red notify toast">
                <div class="content">
                    <div class="ui header">
                        <i class="exclamation circle icon"></i>{{ __('Message') }}
                    </div>
                    <ul class="list">
                        @foreach ($errors->all() as $message)
                            <li>{{ $message }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
            @elseif ($message = Session::get('success'))
            <div class="ui green notify toast">
                <div class="content">
                    <div class="ui header">
                        <i class="check circle outline icon"></i>{{ __('Message') }}
                    </div>
                    <ul class="list">
                        <li>{{ $message }}</li>
                    </ul>
                </div>
            </div>
            @endif
            <div class="ui equal width middle aligned grid basic segment">
                <div class="ui fluid container">
                    @if (Request::is('a/home'))
                    <x-admin />
                    @elseif (Request::is('d/home'))
                    <x-dept />
                    @endif
                    @yield('main')
                </div>
            </div>
        </div>
    </main>
</div>
@yield('modal')
<div class="ui inverted vertical footer teal segment">
    <div class="ui container">
        <div class="ui middle aligned inverted equal width grid">
            <div class="column">
                <p>Copyright &copy; {{ date('Y') }} G-Tracer Development Team</p>
            </div>
            <div class="column">
                <h4 class="ui inverted header">{{ __('References') }}</h4>
                <div class="ui inverted link list">
                    <a href="#" class="item">{{ __('About G-Tracer') }}</a>
                    <a href="#" class="item">{{ __('Privacy Policy') }}</a>
                    <a href="#" class="item">{{ __('Contact Us') }}</a>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('js/main.gtracer.js') }}" crossorigin="anonymous"></script>
@endsection
