@extends('main')

@section('title', "Welcome to the Web Admin")

@section('content')
<div class="ui large top fixed hidden borderless menu">
    <div class="ui container">
        <div class="header item">
            <img src="{{ asset('img/app/logo.png') }}" alt="App Logo" class="logo">
        </div>
        <div class="right item">
            <button class="ui teal button btn-login" data-target="{{ route('login') }}">
            @if (Route::has('login'))
                <i class="home icon"></i>
                <small>{{ __('HOME') }}</small>
            @else
                <i class="sign in alternate icon"></i>
                <small>{{ __('LOG IN') }}</small>
            @endif
            </button>
        </div>
    </div>
</div>
<div class="ui inverted vertical center aligned masthead segment" style="background: linear-gradient(-70deg, #00B5AD 30%, rgba(0, 0, 0, 0.625) 30%), url('{{ asset('img/app/background.jpg') }}'); background-size: cover">
    <div class="ui container">
        <div class="ui equal width grid">
            <div class="center aligned column">
                <img src="{{ asset('img/app/logo.png') }}" alt="App Logo" class="ui centered tiny image">
                <div class="ui hidden divider"></div>
                <h1 class="ui inverted header">{{ strtoupper(env('APP_NAME')) }}
                    <h2>{{ __('A Mobile-Web Application for Graduate Tracing using Crowdsourcing and Social Media Listening') }}</h2>
                </h1>
                <div class="ui hidden divider"></div>
                <button class="ui huge inverted button btn-login" data-target="{{ route('login') }}">
                    @if (Route::has('login'))
                        @auth
                            <i class="home icon"></i>
                            <small>{{ __('HOME') }}</small>
                        @else
                            <i class="sign in alternate icon"></i>
                            <small>{{ __('LOG IN') }}</small>
                        @endauth
                    @endif
                </button>
            </div>
            <div class="column">
                <img src="{{ asset('img/app/gtracer.png') }}" alt="App Branding" class="ui large image">
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
$(function () {
    $('.btn-login').on('click', function () {
        window.location = $(this).data('target');
    });
});
</script>
@endsection
