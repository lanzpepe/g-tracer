@extends('layouts.home')

@section('title', 'File Manager')

@section('stylesheets')
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="{{ asset('vendor/file-manager/css/file-manager.css') }}">
@endsection

@section('main')
<div class="ui equal width centered grid" id="manager">
    <div class="column">
        <div id="fm"></div>
    </div>
</div>
<script src="{{ asset('vendor/file-manager/js/file-manager.js') }}" crossorigin="anonymous" defer></script>
@endsection
