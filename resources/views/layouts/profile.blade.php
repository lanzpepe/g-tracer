@extends('layouts.home')

@section('title', 'User Profile')

@section('header')
<i class="user outline teal icon"></i> @yield('title')
@endsection

@section('main')
<div class="ui center aligned container grid">
    <div class="row">
        <img class="ui circular image profile" src="{{ $user->image }}" alt="User Image">
    </div>
    <div class="row">
        <h2 class="ui center aligned header">
            <div class="content"> {{ $user->full_name }}
                <div class="sub header">{{ $user->admin->department->name }} &middot; {{ $user->admin->school->name }}</div>
            </div>
        </h2>
    </div>
    <div class="row">
        <div class="three wide column">
            <h4 class="ui center aligned grey header">
                <i class="venus mars teal icon"></i>{{ $user->person->gender }}
            </h4>
        </div>
        <div class="three wide column">
            <h4 class="ui center aligned grey header">
                <i class="calendar alternate teal icon"></i>{{ $user->person->birth_date }}
            </h4>
        </div>
        <div class="three wide column">
            <h4 class="ui center aligned grey header">
                <i class="user cog teal icon"></i>{{ $user->admin->role }}
            </h4>
        </div>
    </div>
</div>
@endsection
