@extends('layouts.home')

@section('title', "{$graduate->name}")

@section('header')
<i class="user graduate teal icon"></i>{{ __("Graduate Details - {$graduate->full_name}") }}
@endsection

@section('button')
@if ($graduate->confirmed->isEmpty())
<button class="ui right floated teal button confirm-graduate" role="button">
    <i class="check icon"></i> {{ __('Confirm Graduate') }}
</button>
@endif
@endsection

@section('main')
<div class="ui basic segment">
    <div class="ui breadcrumb">
        <a href="{{ route('graduates.index') }}" class="section">{{ __('Graduate List') }}</a>
        <i class="right angle icon divider"></i>
        <div class="active section">{{ __('Graduate Details') }}</div>
    </div>
</div>
<div class="ui basic segment">
    <div class="ui equal width grid">
        <div class="two wide column">
            <img src="{{ $graduate->image }}" class="ui centered tiny circular image">
        </div>
        <div class="left floated column">
            <h2 class="ui teal header">{{ $graduate->full_name }}
                <div class="ui {{ $graduate->status == "Open" ? 'green' : 'red' }} label">{{ $graduate->status }}</div>
            </h2>
            <div><span class="ui teal text">{{ __('Address:') }}</span> {{ __("{$graduate->address}") }}</div>
            <div><span class="ui teal text">{{ __('Degree Program:') }}</span> {{ __("{$graduate->degree_program}") }}</div>
            <div><span class="ui teal text">{{ __('Date Graduated:') }}</span> {{ __("{$graduate->date_graduated}") }}</div>
            <div><span class="ui teal text">{{ __('Contact No.:') }}</span> {{ __("{$graduate->contact}") }}</div>
            <div>
                <button class="ui green tertiary button edit-graduate">
                    <i class="pen icon"></i> {{ __('Edit') }}
                </button>
                <button class="ui red tertiary button mark-graduate">
                    <i class="trash icon"></i> {{ __('Remove') }}
                </button>
            </div>
        </div>
        <div class="column">
            <div class="ui teal header">{{ __('Employment details') }}</div>
            @if ($graduate->confirmed->isNotEmpty())
            <div><span class="ui teal text">{{ __('Employment Status:') }}</span> {{ __("{$graduate->employment_status}") }}</div>
            @if ($graduate->employment_status == 'Employed')
            <div><span class="ui teal text">{{ __('Company & Address:') }}</span> {{ __("{$graduate->company_name}, {$graduate->company_address}") }}</div>
            <div><span class="ui teal text">{{ __('Position/Designation:') }}</span> {{ __("{$graduate->job_position}") }}</div>
            <div><span class="ui teal text">{{ __('Date Employed:') }}</span> {{ __("{$graduate->date_employed}") }}</div>
            @endif
            <div><span class="ui teal text">{{ __('Remarks:') }}</span> {{ __("{$graduate->remarks}") }}</div>
            <div>
                <button class="ui green tertiary button edit-employment">
                    <i class="pen icon"></i> {{ __('Edit') }}
                </button>
            </div>
            @else
            <div class="ui small yellow message">
                {{ __("Crowdsourcing of employment data is on-going, awaiting for the graduate's confirmation. To confirm, just click the 'Confirm Graduate' button.") }}
            </div>
            @endif
        </div>
    </div>
</div>
@if ($responses->isNotEmpty())
<div class="row">
    <div class="column">
        <div class="ui top attached tabular menu">
            <a class="item active" data-tab="first">{{ __('Dashboard') }}</a>
            <a class="item" data-tab="second">{{ __('Data Table') }}</a>
        </div>
        <div class="ui bottom attached tab segment active" data-tab="first">
            <div class="ui equal width stackable grid container">
                <div class="column">
                    <div class="ui padded raised teal segment">
                        <div class="ui center aligned dividing header">
                            {{ __("Most Frequent Answers based on {$responses->count()} Responses") }}
                            <i class="question circle outline teal small icon link" data-content="Responses that have the highest frequency of answers per category."></i>
                        </div>
                        <div class="ui three small statistics">
                            @foreach ($results as $key => $value)
                            @if ($loop->first)
                            <div class="teal statistic">
                                <div class="value">{{ __("{$value}%") }}</div>
                                <div class="label">{{ Str::of($key)->explode('+')[0] }}</div>
                                <div class="label">{{ Str::of($key)->explode('+')[1] }}</div>
                            </div>
                            @else
                            <div class="teal statistic">
                                <div class="value">{{ __("{$value}%") }}</div>
                                <div class="label">{{ $key }}</div>
                            </div>
                            @endif
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="eight wide column">
                        <div class="ui raised padded teal segment">
                            <div class="ui center aligned dividing header">
                                {{ __('Companies answered by Respondents') }}
                            </div>
                            <div id="companies" style="height: 50vh"></div>
                        </div>
                    </div>
                    <div class="eight wide column">
                        <div class="ui raised padded teal segment">
                            <div class="ui center aligned dividing header">
                                {{ __("Respondents with relation to {$graduate->person->first_name}") }}
                            </div>
                            <div id="remarks" style="height: 50vh"></div>
                        </div>
                    </div>
                </div>
                <div class="centered row">
                    <div class="column">
                        <div class="ui raised padded teal segment">
                            <div class="ui center aligned dividing header">
                                {{ __('Job Positions answered by Respondents') }}
                            </div>
                            <div id="positions" style="height: 50vh"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="ui bottom attached tab segment" data-tab="second">
            <table class="ui unstackable selectable celled compact teal table" id="responses" data-page-length="10">
                <thead>
                    <tr class="center aligned">
                        <th>{{ __('Response ID') }}</th>
                        <th>{{ __('Company Name') }}</th>
                        <th>{{ __('Company Address') }}</th>
                        <th>{{ __('Job Position') }}</th>
                        <th>{{ __('Relation') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($responses as $response)
                    <tr class="center aligned">
                        <td>{{ $response->response_id }}</td>
                        <td>{{ $response->company->name }}</td>
                        <td>{{ $response->company->address }}</td>
                        <td>{{ $response->job_position }}</td>
                        <td>{{ $response->remarks }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@else
<div class="ui raised placeholder basic teal segment">
    <div class="ui icon header">
        <i class="exclamation circle teal icon"></i>
        {{ __('No responses yet.') }}
    </div>
</div>
@endif
@endsection

@section('modal')
<x-graduate.add :courses="$courses" />
@if ($responses->isNotEmpty())
<x-graduate.confirm :graduate="$graduate" :responses="$responses" :results="$results" />
@else
<x-graduate.confirm :graduate="$graduate" :responses="$responses" />
@endif
<x-graduate.delete />
@endsection

@section('scripts')
@if ($responses->isNotEmpty())
<script src="{{ asset('js/responses.chart.js') }}" crossorigin="anonymous"></script>
@endif
@endsection
