@extends('layouts.home')

@section('title', 'Course Related Jobs')

@section('header')
<i class="briefcase teal icon"></i> @yield('title')
@endsection

@section('button')
<button class="ui right floated teal button add-job">
    <i class="plus icon"></i> {{ __('Add Related Job') }}
</button>
@endsection

@section('main')
@if ($courseJobs->isNotEmpty())
<x-job.list :courseJobs="$courseJobs" />
@else
<div class="ui placeholder basic segment">
    <div class="ui icon header">
        <i class="exclamation circle teal icon"></i>
        {{ __('No course-related jobs displayed.') }}
    </div>
</div>
@endif
@endsection

@section('modal')
<x-job.add :courses="$courses" />
<x-job.delete />
@endsection
