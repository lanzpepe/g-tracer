@extends('layouts.home')

@section('title', 'Graduate List')

@section('header')
<i class="user graduate teal icon"></i> @yield('title')
@endsection

@section('button')
<button class="ui right floated teal button import-graduates">
    <i class="file import icon"></i> {{ __('Import List') }}
</button>
<button class="ui right floated teal button add-graduate">
    <i class="plus icon"></i> {{ __('Add Graduate') }}
</button>
@endsection

@section('main')
@if ($graduates->isNotEmpty())
<x-graduate.list :graduates="$graduates" />
@else
<div class="ui placeholder basic segment">
    <div class="ui icon header">
        <i class="exclamation circle teal icon"></i>
        {{ __('Click the \'Add Graduate\' or \'Import List\' button to display it here.') }}
    </div>
</div>
@endif
@endsection

@section('modal')
<x-graduate.import :courses="$courses" />
<x-graduate.add :courses="$courses" />
@endsection
