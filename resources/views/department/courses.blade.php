@extends('layouts.home')

@section('title', 'Degree Programs')

@section('header')
<i class="book reader teal icon"></i> @yield('title')
@endsection

@section('button')
<button class="ui right floated teal button add-course">
    <i class="plus icon"></i> {{ __('Add Degree Program') }}
</button>
@endsection

@section('main')
@if ($courses->isNotEmpty())
<x-course.list :courses="$courses" />
@else
<div class="ui placeholder basic segment">
    <div class="ui icon header">
        <i class="exclamation circle teal icon"></i>
        {{ __('Click the \'Add Degree Program\' button to display it here.') }}
    </div>
</div>
@endif
@endsection

@section('modal')
<x-course.add :courses="$courses" />
<x-course.delete />
@endsection
