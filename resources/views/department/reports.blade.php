@extends('layouts.home')

@section('title', 'Dashboard & Reports')

@section('header')
<i class="tachometer alternate teal icon"></i> @yield('title')
@endsection

@section('main')
<div class="ui equal width stackable grid">
    <div class="column">
        <div class="ui raised teal segment">
            <div class="ui center aligned dividing header">
                {{ __("Graduate Statistics by Year") }}
                <div class="sub header">
                    {{ __("College of $department") }}
                </div>
            </div>
            <div id="statistics" style="height: 50vh"></div>
        </div>
        <div class="ui raised teal segment">
            <div class="ui center aligned dividing header">
                {{ __("Employment Data by Year") }}
                <div class="sub header">
                    {{ __("College of $department") }}
                </div>
            </div>
            <div id="employment" style="height: 50vh"></div>
        </div>
        <div class="ui center aligned raised teal segment">
            <div class="ui dividing header">
                {{ __('Lag Time Prior to Employment Frequency Distribution') }}
                <div class="sub header">
                    {{ __("{$years->last()} to {$years->first()}") }}
                </div>
            </div>
            <table class="ui compact selectable striped table" role="table">
                <thead>
                    <tr class="center aligned">
                        <th>{{ __('Length of Job Search') }}</th>
                        <th>{{ __('Frequency') }}</th>
                        <th>{{ __('Percentage (%)') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($intervals as $key => $value)
                    <tr class="center aligned">
                        <td>{{ $key }}</td>
                        <td>{{ $value }}</td>
                        <td>{{ ($value === 0 ? 0 : round($value / $employed * 100))."%" }}</td>
                    </tr>
                    @endforeach
                    <tr class="center aligned">
                        <td><strong>{{ __('Total') }}</strong></td>
                        <td><strong>{{ $employed }}</strong></td>
                        <td><strong>{{ __('100%') }}</strong></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="five wide column">
        <div class="ui raised teal segment">
            <div class="ui center aligned dividing header">
                {{ __('Report Generator') }}
            </div>
            <form action="{{ route('dept.generate') }}" class="ui form search" id="generateForm" method="POST" role="form" target="_blank">
                @csrf
                <div class="field">
                    <label for="course"><i class="book reader teal icon"></i>{{ __('Degree Program') }}</label>
                    <select name="course" id="course" class="ui fluid dropdown" required>
                        <option value="">{{ __('- Select Degree Program -') }}</option>
                        @foreach ($courses as $course)
                        <option value="{{ $course->code }}">{{ $course->code }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="field">
                    <label for=""><i class="graduation cap teal icon"></i>{{ __('Term Graduated') }}</label>
                    <div class="equal width fields">
                        <div class="field">
                            <select name="year" id="year" class="ui fluid dropdown" required>
                                @foreach ($years as $year)
                                <option value="{{ $year }}">{{ $year }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="field">
                            <select name="batch" id="batch" class="ui fluid dropdown" required>
                                @foreach ($batches as $batch)
                                <option value="{{ $batch->month }}">{{ $batch->month }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </form>
            <div class="actions">
                <button type="submit" class="ui teal fluid button" form="generateForm" role="button">
                    {{ __('Generate') }}
                </button>
            </div>
        </div>
        <div class="ui center aligned raised teal segment">
            <div class="ui dividing header">
                {{ __('Graduates\' Response Rate (%)') }}
                <div class="sub header">
                    {{ __("{$years->last()} to {$years->first()}") }}
                </div>
            </div>
            <div class="ui one small statistics">
                <div class="teal statistic">
                    <div class="value">{{ ($graduates->count() === 0 ? 0 : round($confirmed / $graduates->count() * 100, 2))."%" }}</div>
                    <div class="label">{{ __("{$confirmed} of {$graduates->count()} graduates responded") }}</div>
                </div>
            </div>
        </div>
        <div class="ui center aligned raised teal segment">
            <div class="ui dividing header">
                {{ __('Graduates\' Employment Status (%)') }}
                <div class="sub header">
                    {{ __("{$years->last()} to {$years->first()}") }}
                </div>
            </div>
            <div class="ui two small statistics">
                @foreach ($statuses as $key => $value)
                <div class="teal statistic">
                    <div class="value">{{ ($value === 0 ? 0 : round($value / $confirmed * 100)) . '%' }}</div>
                    <div class="label">{{ "{$key}: {$value}" }}</div>
                </div>
                @endforeach
            </div>
        </div>
        <div class="ui center aligned raised teal segment">
            <div class="ui dividing header">
                {{ __('Job Placement Relevancy (%)') }}
                <div class="sub header">
                    {{ __("{$years->last()} to {$years->first()}") }}
                </div>
            </div>
            <div class="ui one small statistics">
                @foreach ($statuses as $key => $value)
                @if ($loop->first)
                <div class="teal statistic">
                    <div class="value">{{ ($relevance === 0 ? 0 : round($relevance / $value * 100)) . "%" }}</div>
                    <div class="label">{{ __('Related to the Completed Course') }}</div>
                </div>
                <div class="teal statistic">
                    <div class="value">{{ ($relevance === 0 ? 0 : round(($value - $relevance) / $value * 100)) . "%" }}</div>
                    <div class="label">{{ __('Not Related to the Completed Course') }}</div>
                </div>
                @endif
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('js/statistics.chart.js') }}" crossorigin="anonymous"></script>
@endsection
