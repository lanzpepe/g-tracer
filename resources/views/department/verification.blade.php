@extends('layouts.home')

@section('title', 'Verification Requests')

@section('header')
<i class="user check teal icon"></i> @yield('title')
@endsection

@section('main')
@if ($verifications->isNotEmpty())
<x-verification.list :verifications="$verifications" />
@else
<div class="ui placeholder basic segment">
    <div class="ui icon header">
        <i class="exclamation circle teal icon"></i>
        {{ __('No pending requests yet.') }}
    </div>
</div>
@endif
@endsection

@section('modal')
<x-verification.add :courses="$courses" />
@endsection
