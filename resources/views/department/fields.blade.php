@extends('layouts.home')

@section('title', 'Preview Graduate Data')

@section('header')
<i class="eye teal icon"></i> @yield('title')
@endsection

@section('button')
<button type="submit" class="ui right floated teal submit button" form="uploadForm" role="button">
    <i class="file upload icon"></i> {{ __('Upload Data') }}
</button>
@endsection

@section('main')
<div class="ui center aligned grid basic segment">
    <div class="column">
        <span class="ui large text">{{ __("List of {$month} {$data['_day']}, {$data['_year']} {$data['_course']} Graduates") }}</span>
    </div>
</div>
<div class="column">
    <table class="ui compact selectable striped celled teal table" id="fields">
        <thead>
            <tr class="center aligned">
                <th>{{ __('Last Name') }}</th>
                <th>{{ __('First Name') }}</th>
                <th>{{ __('M.I.') }}</th>
                <th>{{ __('Gender') }}</th>
                <th>{{ __('Address') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($csvData as $row)
            <tr class="center aligned">
                @foreach ($row as $key => $value)
                <td>{{ $value }}</td>
                @endforeach
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
<form action="{{ route('g.upload') }}" id="uploadForm" method="POST" role="form">
    @csrf
    <input type="hidden" name="data" value="{{ encrypt($csvFile->id) }}">
    <input type="hidden" name="school" value="{{ Auth::user()->school->name }}">
    <input type="hidden" name="dept" value="{{ Auth::user()->department->name }}">
    <input type="hidden" name="course" value="{{ $data['_course'] }}">
    <input type="hidden" name="major" value="{{ $data['_major'] }}">
    <input type="hidden" name="year" value="{{ $data['_year'] }}">
    <input type="hidden" name="month" value="{{ $month }}">
    <input type="hidden" name="day" value="{{ $data['_day'] }}">
</form>
@endsection
