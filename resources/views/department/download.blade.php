<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ __('Download PDF') }}</title>
    <link rel="icon" href="{{ asset('images/favicon.png') }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <style>
        @font-face {
            font-family: 'Product Sans';
            font-style: normal;
            font-weight: normal;
            src: url({{ asset('fonts/Product-Sans.ttf') }}) format('truetype');
        }
        body {
            font-family: 'Product Sans', Verdana, Geneva, Tahoma, sans-serif;
            font-size: 14px;
        }
    </style>
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="text-center">
                    <h3 class="text-uppercase font-weight-bold">{{ __("College of {$department} Graduate Tracer") }}</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="text-center">
                    <h4 class="font-weight-normal">{{ __("{$term->first()}") }}</h4>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="text-center py-4">
                    <h6 class="text-uppercase font-weight-bold">{{ __("{$course->title}") }}</h6>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <table class="table table-sm table-bordered">
                    <thead>
                        <tr class="text-center">
                            <th class="align-middle" style="width: 15%">{{ __('Name') }}</th>
                            <th class="align-middle" style="width: 20%">{{ __('Address') }}</th>
                            <th class="align-middle" style="width: 10%">{{ __('Contact No.') }}</th>
                            <th class="align-middle" style="width: 10%">{{ __('Company') }}</th>
                            <th class="align-middle" style="width: 15%">{{ __('Company Address') }}</th>
                            <th class="align-middle" style="width: 10%">{{ __('Company Contact No.') }}</th>
                            <th class="align-middle" style="width: 10%">{{ __('Position/ Designation') }}</th>
                            <th class="align-middle" style="width: 10%">{{ __('Remarks') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($employed as $item)
                        <tr class="text-center">
                            <td class="align-middle">{{ $item->full_name }}</td>
                            <td class="align-middle">{{ $item->address }}</td>
                            <td class="align-middle">{{ $item->contact }}</td>
                            <td class="align-middle">{{ $item->company_name }}</td>
                            <td class="align-middle">{{ $item->company_address }}</td>
                            <td class="align-middle">{{ $item->company_contact }}</td>
                            <td class="align-middle">{{ $item->job_position }}</td>
                            <td class="align-middle">{{ $item->remarks }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row my-5">
            <div class="col-5">
                <table class="table table-sm table-bordered">
                    <tbody>
                        <tr>
                            <td class="align-middle">{{ __('Total No. of Graduates') }}</td>
                            <td class="align-middle text-center">{{ $confirmed->count() }}</td>
                            <td class="align-middle text-center">{{ __('100%') }}</td>
                        </tr>
                        <tr>
                            <td class="align-middle">{{ __('Employed') }}</td>
                            <td class="align-middle text-center">{{ $employed->count() }}</td>
                            <td class="align-middle text-center">{{ round($employed->count() / $confirmed->count() * 100) . "%" }}</td>
                        </tr>
                        <tr>
                            <td class="align-middle">{{ __('Unemployed') }}</td>
                            <td class="align-middle text-center">{{ $confirmed->count() - $employed->count() }}</td>
                            <td class="align-middle text-center">{{ round(($confirmed->count() - $employed->count()) / $confirmed->count() * 100) . "%" }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>
