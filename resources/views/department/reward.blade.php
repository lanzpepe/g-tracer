@extends('layouts.home')

@section('title', 'Reward Item Details')

@section('header')
<i class="gift icon teal"></i> @yield('title')
@endsection

@section('main')
<div class="ui basic segment">
    <div class="ui breadcrumb">
        <a href="{{ route('rewards.index') }}" class="section">{{ __('Rewards List') }}</a>
        <i class="right angle icon divider"></i>
        <div class="active section">@yield('title')</div>
    </div>
</div>
<div class="ui fluid container">
    <div class="ui equal width stackable grid">
        <div class="four wide column">
            <div class="ui card">
                <div class="ui centered image">
                    <img src="{{ $reward->image }}" alt="Item Image">
                </div>
                <div class="content">
                    <div class="center aligned header">{{ $reward->name }}</div>
                    <div class="description">
                        <div><span class="ui teal text">{{ __('Required Points: ') }}</span>{{ $reward->points }}</div>
                        <div><span class="ui teal text">{{ __('Quantity: ') }}</span>{{ $reward->quantity }}</div>
                        <div><span class="ui teal text">{{ __('Description: ') }}</span>{{ $reward->description }}</div>
                    </div>
                </div>
                <div class="center aligned extra content">
                    <button class="ui green tertiary button edit-item">
                        <i class="pen icon"></i> {{ __('Edit') }}
                    </button>
                    <button class="ui red tertiary button mark-item">
                        <i class="trash icon"></i> {{ __('Remove') }}
                    </button>
                </div>
            </div>
        </div>
        <div class="column">
            @if ($reward->users->isNotEmpty())
            <table class="ui celled compact selectable striped teal table" id="requests">
                <thead>
                    <tr class="center aligned">
                        <th>{{ __('Reference #') }}</th>
                        <th>{{ __('Name') }}</th>
                        <th>{{ __('User ID') }}</th>
                        <th>{{ __('Date Requested') }}</th>
                        <th>{{ __('Status') }}</th>
                        <th>{{ __('Actions') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($reward->users as $user)
                    <tr class="center aligned">
                        <td>{{ $user->pivot->id }}</td>
                        <td>{{ $user->full_name }}</td>
                        <td>{{ $user->user_id }}</td>
                        <td>{{ $user->pivot->created_at->toFormattedDateString() }}</td>
                        <td>
                            <div class="ui {{ $user->pivot->status ? 'red' : 'green' }} label">
                                {{ $user->pivot->status ? 'Claimed' : 'Unclaimed' }}
                            </div>
                        </td>
                        <td>
                            <button class="ui {{ $user->pivot->status ? 'green' : 'red confirm-item' }} tertiary button" data-value="{{ $user->pivot->id }}">
                                {{ $user->pivot->status ? 'Accepted' : 'Accept' }}
                            </button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @else
            <div class="ui placeholder basic teal segment">
                <div class="ui icon header">
                    <i class="exclamation circle teal icon"></i>
                    {{ __('No requests yet.') }}
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection

@section('modal')
<x-reward.add />
<x-reward.accept />
<x-reward.delete />
@endsection
