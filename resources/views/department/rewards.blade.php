@extends('layouts.home')

@section('title', 'Points & Rewards')

@section('header')
<i class="gifts teal icon"></i> @yield('title')
@endsection

@section('button')
<button class="ui right floated teal button add-item">
    <i class="plus icon"></i> {{ __('Add Reward Item') }}
</button>
<button class="ui right floated teal button set-point">
    <i class="star outline icon"></i> {{ __('Point Settings') }}
</button>
@endsection

@section('main')
@if ($rewards->isNotEmpty())
<x-reward.list :rewards="$rewards" />
@else
<div class="ui placeholder basic segment">
    <div class="ui icon header">
        <i class="exclamation circle teal icon"></i>
        {{ __('Click the \'Add Reward Item\' button to display it here.') }}
    </div>
</div>
@endif
@endsection

@section('modal')
<x-reward.points />
<x-reward.add />
@endsection
