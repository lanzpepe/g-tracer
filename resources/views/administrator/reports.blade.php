@extends('layouts.home')

@section('title', 'Dashboard & Reports')

@section('header')
<i class="tachometer alternate teal icon"></i> @yield('title')
@endsection

@section('main')
<div class="ui equal width stackable grid">
    <div class="column">
        <div class="ui raised teal segment">
            <div class="ui center aligned dividing header">
                {{ __("Graduate Statistics by Year") }}
                <div class="sub header" id="first"></div>
            </div>
            <div id="statistics" style="height: 50vh"></div>
        </div>
        <div class="ui raised teal segment">
            <div class="ui center aligned dividing header">
                {{ __("Employment Data by Year") }}
                <div class="sub header two" id="second"></div>
            </div>
            <div id="employment" style="height: 50vh"></div>
        </div>
        <div class="ui center aligned raised teal segment">
            <div class="ui dividing header">
                {{ __('Lag Time Prior to Employment Frequency Distribution') }}
                <div class="sub header">
                    <div class="sub header" id="third"></div>
                </div>
            </div>
            <table class="ui compact selectable striped table" role="table">
                <thead>
                    <tr class="center aligned">
                        <th>{{ __('Length of Job Search') }}</th>
                        <th>{{ __('Frequency') }}</th>
                        <th>{{ __('Percentage (%)') }}</th>
                    </tr>
                </thead>
                <tbody id="report_one"></tbody>
            </table>
        </div>
    </div>
    <div class="five wide column">
        <div class="ui raised teal segment">
            <div class="ui center aligned dividing header">
                {{ __('Select Department') }}
            </div>
            <div class="ui form search">
                <div class="field">
                    <label for="srch_school"><i class="school teal icon"></i>{{ __('School') }}</label>
                    <select name="srch_school" id="srch_school" class="ui disabled fluid dropdown" required>
                        <option value="{{ $school->id }}">{{ $school->name }}</option>
                    </select>
                </div>
                <div class="field">
                    <label for="srch_dept"><i class="building teal icon"></i>{{ __('Department') }}</label>
                    <select name="srch_dept" id="srch_dept" class="ui fluid dropdown" required>
                    @foreach ($departments as $dept)
                        <option value="{{ $dept->id }}">{{ $dept->name }}</option>
                    @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="ui raised teal segment">
            <div class="ui center aligned dividing header">
                {{ __('Report Generator') }}
            </div>
            <form action="{{ route('admin.generate') }}" class="ui form search" id="generateForm" method="POST" role="form" target="_blank">
                @csrf
                <div class="field">
                    <label for="gen_dept"><i class="building teal icon"></i>{{ __('Department') }}</label>
                    <select name="department" id="gen_dept" class="ui fluid dropdown" required>
                    @foreach ($departments as $dept)
                        <option value="{{ $dept->id }}">{{ $dept->name }}</option>
                    @endforeach
                    </select>
                </div>
                <div class="field">
                    <label for="course"><i class="book reader teal icon"></i>{{ __('Degree Program') }}</label>
                    <select name="course" id="course" class="ui fluid dropdown" required>
                        <option value="">{{ __('- Select Degree Program -') }}</option>
                    </select>
                </div>
                <div class="field">
                    <label for=""><i class="graduation cap teal icon"></i>{{ __('Term Graduated') }}</label>
                    <div class="equal width fields">
                        <div class="field">
                            <select name="year" id="year" class="ui fluid dropdown" required>
                                @foreach ($years as $year)
                                <option value="{{ $year }}">{{ $year }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="field">
                            <select name="batch" id="batch" class="ui fluid dropdown" required>
                                @foreach ($batches as $batch)
                                <option value="{{ $batch->month }}">{{ $batch->month }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </form>
            <div class="actions">
                <button type="submit" class="ui teal fluid button" form="generateForm" role="button">
                    {{ __('Generate') }}
                </button>
            </div>
        </div>
        <div id="report_two"></div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('js/statistics.chart.js') }}" crossorigin="anonymous"></script>
@endsection
