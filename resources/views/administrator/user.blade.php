@extends('layouts.home')

@section('title', 'Manage Users')

@section('header')
<i class="users teal icon"></i> @yield('title')
@endsection

@section('main')
<div class="ui equal width centered grid">
    <x-user.list :users="$users" />
</div>
@endsection

