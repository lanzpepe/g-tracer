@foreach ($intervals as $key => $value)
<tr class="center aligned">
    <td>{{ $key }}</td>
    <td>{{ $value }}</td>
    <td>{{ ($value === 0 ? 0 : round($value / $employed * 100))."%" }}</td>
</tr>
@endforeach
<tr class="center aligned">
    <td><strong>{{ __('Total') }}</strong></td>
    <td><strong>{{ $employed }}</strong></td>
    <td><strong>{{ __('100%') }}</strong></td>
</tr>
