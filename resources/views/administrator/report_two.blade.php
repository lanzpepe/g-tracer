<div class="ui center aligned raised teal segment">
    <div class="ui dividing header">
        {{ __('Graduates\' Response Rate (%)') }}
        <div class="sub header">
            {{ __("{$years->last()} to {$years->first()}") }}
        </div>
    </div>
    <div class="ui one small statistics">
        <div class="teal statistic">
            <div class="value">{{ ($graduates->count() === 0 ? 0 : round($confirmed / $graduates->count() * 100, 2))."%" }}</div>
            <div class="label">{{ __("{$confirmed} of {$graduates->count()} graduates responded") }}</div>
        </div>
    </div>
</div>
<div class="ui center aligned raised teal segment">
    <div class="ui dividing header">
        {{ __('Graduates\' Employment Status (%)') }}
        <div class="sub header">
            {{ __("{$years->last()} to {$years->first()}") }}
        </div>
    </div>
    <div class="ui two small statistics">
        @foreach ($statuses as $key => $value)
        <div class="teal statistic">
            <div class="value">{{ ($value === 0 ? 0 : round($value / $confirmed * 100)) . '%' }}</div>
            <div class="label">{{ "{$key}: {$value}" }}</div>
        </div>
        @endforeach
    </div>
</div>
<div class="ui center aligned raised teal segment">
    <div class="ui dividing header">
        {{ __('Job Placement Relevancy (%)') }}
        <div class="sub header">
            {{ __("{$years->last()} to {$years->first()}") }}
        </div>
    </div>
    <div class="ui one small statistics">
        @foreach ($statuses as $key => $value)
        @if ($loop->first)
        <div class="teal statistic">
            <div class="value">{{ ($relevance === 0 ? 0 : round($relevance / $value * 100)) . "%" }}</div>
            <div class="label">{{ __('Related to the Completed Course') }}</div>
        </div>
        <div class="teal statistic">
            <div class="value">{{ ($relevance === 0 ? 0 : round(($value - $relevance) / $value * 100)) . "%" }}</div>
            <div class="label">{{ __('Not Related to the Completed Course') }}</div>
        </div>
        @endif
        @endforeach
    </div>
</div>
