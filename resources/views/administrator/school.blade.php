@extends('layouts.home')

@section('title', 'Manage Schools')

@section('header')
<i class="school teal icon"></i> @yield('title')
@endsection

@section('button')
<button class="ui right floated teal button add-school">
    <i class="plus icon"></i> {{ __('Add School') }}
</button>
@endsection

@section('main')
<div class="ui equal width centered grid">
    <x-school.list :schools="$schools" />
</div>
@endsection

@section('modal')
<x-school.add />
<x-school.delete />
@endsection
