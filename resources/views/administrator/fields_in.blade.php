@extends('layouts.home')

@section('title', 'Preview LinkedIn Data')

@section('header')
<i class="eye teal icon"></i> @yield('title')
@endsection

@section('button')
<button type="submit" class="ui right floated teal submit button" form="uploadForm" role="button">
    <i class="file upload icon"></i> {{ __('Upload Data') }}
</button>
@endsection

@section('main')
<div class="column">
    <table class="ui unstackable selectable single line teal table" id="fields" style="display:block; overflow-x:auto">
        <thead>
            <tr>
                @foreach ($headers as $key => $value)
                <th>{{ $value }}</th>
                @endforeach
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $row)
            <tr>
                @foreach ($row as $key => $value)
                <td>{{ $value }}</td>
                @endforeach
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
<form action="{{ route('in.upload') }}" id="uploadForm" method="POST" role="form">
    @csrf
    <input type="hidden" name="data" value="{{ encrypt($csv->id) }}">
</form>
@endsection
