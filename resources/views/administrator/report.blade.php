@extends('layouts.home')

@section('title', 'Report Viewer')

@section('header')
<i class="chart bar teal icon"></i> @yield('title')
@endsection

@section('button')
@if ($graduates->isNotEmpty())
<button type="button" class="ui right floated red button" id="admin-download" role="button">
    <i class="file pdf outline icon"></i> {{ __("Save as PDF") }}
</button>
@endif
@endsection

@section('main')
<div class="ui center aligned grid basic segment">
    <div class="column row">
        <h2 class="ui header">{{ strtoupper($course->title) }}
            <div class="sub header">{{ __("{$term->first()}") }}</div>
        </h2>
    </div>
</div>
@if ($graduates->isNotEmpty())
<div class="column">
    <form action="{{ route('admin.download') }}" method="post" class="ui form" role="form">
        @csrf
        <table class="ui celled compact selectable striped teal table">
            <thead>
                <tr class="center aligned">
                    <th class="three wide">{{ __('Name') }}</th>
                    <th class="two wide">{{ __('Address') }}</th>
                    <th class="two wide">{{ __('Contact No.') }}</th>
                    <th class="two wide">{{ __('Company') }}</th>
                    <th class="two wide">{{ __('Company Address') }}</th>
                    <th class="two">{{ __('Company Contact No.') }}</th>
                    <th>{{ __('Position/Designation') }}</th>
                    <th class="two wide">{{ __('Remarks') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($graduates as $graduate)
                <tr class="center aligned">
                    <td>{{ $graduate->full_name }}</td>
                    <td>{{ $graduate->address }}</td>
                    <td>{{ $graduate->contact }}</td>
                    <td>{{ $graduate->company_name }}</td>
                    <td>{{ $graduate->company_address }}</td>
                    <td>{{ $graduate->company_contact }}</td>
                    <td>{{ $graduate->job_position }}</td>
                    <td>{{ $graduate->remarks }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <input type="hidden" name="params" value="{{ __("{$request->department},{$request->course},{$request->batch},{$request->year}") }}">
    </form>
</div>
@else
<div class="ui placeholder basic teal segment">
    <div class="ui icon header">
        <i class="exclamation circle teal icon"></i>
        {{ __('No graduates displayed.') }}
    </div>
</div>
@endif
@endsection
