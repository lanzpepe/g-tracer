@extends('layouts.home')

@section('title', 'LinkedIn Data')

@section('header')
<i class="linkedin in teal icon"></i> @yield('title')
@endsection

@section('button')
<button class="ui right floated teal button import-linkedin">
    <i class="file import import icon"></i> {{ __('Import LinkedIn Data') }}
</button>
@endsection

@section('main')
<div class="ui equal width centered grid">
    <x-linkedin.list :profiles="$profiles" />
</div>
@endsection

@section('modal')
<x-linkedin.import />
<x-linkedin.profile />
@endsection
