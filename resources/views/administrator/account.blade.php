@extends('layouts.home')

@section('title', 'Manage Accounts')

@section('header')
<i class="users cog teal icon"></i> @yield('title')
@endsection

@section('button')
<button class="ui right floated teal button add-account">
    <i class="plus icon"></i> {{ __('Add Account') }}
</button>
@endsection

@section('main')
<div class="ui equal width centered grid">
    <x-account.list :admins="$admins" />
</div>
@endsection

@section('modal')
<x-account.add :schools="$schools" />
<x-account.delete />
@endsection
