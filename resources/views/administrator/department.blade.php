@extends('layouts.home')

@section('title', 'Manage Departments')

@section('header')
<i class="building teal icon"></i> @yield('title')
@endsection

@section('button')
<button type="button" class="ui right floated teal button add-dept">
    <i class="plus icon"></i> {{ __('Add Department') }}
</button>
@endsection

@section('main')
<div class="ui equal width centered grid">
    <x-department.list :depts="$depts" />
</div>
@endsection

@section('modal')
<x-department.add :schools="$schools" :depts="$depts" />
<x-department.delete />
@endsection
