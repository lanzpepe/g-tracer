@extends('main')

@section('title', 'Login')

@section('content')
<div class="ui inverted vertical masthead segment" style="background: linear-gradient(-70deg, #00A39C 30%, #00B5AD 30%);">
    <div class="ui container">
        <div class="ui large secondary inverted menu">
            <a href="{{ route('index') }}" class="header item">
                <img src="{{ asset('img/app/logo.png') }}" alt="App Logo" class="logo">
                <span class="ui inverted text">{{ env('APP_NAME') }}</span>
            </a>
        </div>
    </div>
    <div class="ui middle aligned center aligned grid">
        <div class="column login">
            <div class="ui attached message">
                <h2 class="ui teal image header">
                    <img src="{{ asset('img/app/logo.png') }}" alt="App Logo" class="image">
                    <div class="content">{{ __('Administrator Login') }}</div>
                </h2>
            </div>
            <form action="{{ route('login') }}" class="ui left aligned attached form segment" method="POST">
                @csrf
                <div class="field">
                    <div class="ui left icon input">
                        <i class="user icon"></i>
                        <input type="text" id="username" name="username" value="{{ old('username') }}" placeholder="Username" required autocomplete="username">
                    </div>
                </div>
                <div class="field">
                    <div class="ui left icon input">
                        <i class="lock icon"></i>
                        <input type="password" id="password" name="password" value="{{ old('password') }}" placeholder="Password" required autocomplete="password">
                    </div>
                </div>
                <div class="inline field">
                    <div class="ui checkbox">
                        <input type="checkbox" name="remember" id="remember" tabindex="0" class="hidden">
                        <label for="remember" class="">{{ __('Remember Me') }}</label>
                    </div>
                </div>
                <button type="submit" class="ui fluid large teal submit button">{{ __('Login') }}</button>
                @error('username')
                    <div class="ui negative message">
                        <ul class="list">
                            <li>{{ $message }}</li>
                        </ul>
                    </div>
                @enderror
            </form>
            <div class="ui bottom attached warning message">
                <i class="help icon"></i>
                {{ __('For account registration, kindly contact your school IT administrator to acquire a new account.') }}
            </div>
        </div>
    </div>
</div>
@endsection
