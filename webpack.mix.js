const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/main.gtracer.js', 'public/js/main.gtracer.js')
   .js('resources/js/responses.chart.js', 'public/js/responses.chart.js')
   .js('resources/js/statistics.chart.js', 'public/js/statistics.chart.js')
   .sass('resources/sass/app.scss', 'public/css/app.css')
   .styles('resources/css/main.css', 'public/css/main.css')

mix.styles([
    'node_modules/normalize.css/normalize.css',
    'node_modules/datatables.net-se/css/dataTables.semanticui.min.css'
], 'public/css/styles.css')

mix.scripts([
    'node_modules/jquery/dist/jquery.min.js',
    'node_modules/datatables.net/js/jquery.dataTables.min.js',
    'node_modules/datatables.net-se/js/dataTables.semanticui.min.js',
    'node_modules/moment/min/moment.min.js',
    'node_modules/blueimp-load-image/js/load-image.all.min.js'
], 'public/js/scripts.js')
