<?php

namespace App\Repositories;

use Alexusmai\LaravelFileManager\Services\ACLService\ACLRepository;
use App\Models\Admin;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class UsersACLRepository implements ACLRepository
{
    /**
     * Get user ID
     *
     * @return mixed
     */
    public function getUserID()
    {
        return Auth::user()->admin_id;
    }

    /**
     * Get ACL rules list for user
     *
     * @return array
     */
    public function getRules(): array
    {
        $admin = Admin::find(Auth::user()->admin_id);

        if (Str::of($admin->role)->exactly(config('constants.roles.admin'))) {
            return [
                ['disk' => 'public', 'path' => '*', 'access' => 2],
                ['disk' => 'users', 'path' => '*', 'access' => 2]
            ];
        }

        return [
            ['disk' => 'public', 'path' => '*', 'access' => 1],
            ['disk' => 'users', 'path' => '/', 'access' => 1],
            ['disk' => 'users', 'path' => "{$admin->username}", 'access' => 1],
            ['disk' => 'users', 'path' => "{$admin->username}/*", 'access' => 2]
        ];
    }
}
