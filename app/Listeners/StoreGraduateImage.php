<?php

namespace App\Listeners;

use App\Models\Graduate;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Storage;

class StoreGraduateImage implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $graduates = Graduate::whereHas('academic', fn ($query) =>
            $query->where('department', $event->admin->department->name)->where('school', $event->admin->school->name)
        )->get();

        $graduates->each(function ($item) use ($event) {
            $academic = $item->academic; $name = "{$item->person->last_name}, {$item->person->first_name}";
            $directory = "files/{$event->admin->admin_id}/images/graduates/{$academic->course->code}/{$academic->year}/{$academic->month}";

            if ($item->image_uri == null && Storage::disk('public')->exists("{$directory}/{$name}.jpg")) {
                $item->update([
                    'image_uri' => Storage::url("{$directory}/{$name}.jpg")
                ]);
            }
        });
    }
}
