<?php

namespace App\Listeners;

use App\Models\ProviderAccount;
use App\Models\TwitterFriendId;
use App\Traits\StaticTrait;
use Illuminate\Contracts\Queue\ShouldQueue;

class StoreTwitterFriendIds implements ShouldQueue
{
    use StaticTrait;

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $userId = $event->user->user_id;
        $token = env('TWITTER_ACCESS_TOKEN');
        $twitter = ProviderAccount::where('provider_name', 'twitter.com')->where('user_id', $userId)->first();

        if ($twitter) {
            $ids = $this->getTwitterFriendIds($twitter->provider_id, $token);

            foreach ($ids as $key => $value) {
                TwitterFriendId::updateOrCreate([
                    'user_id' => $userId,
                    'friend_id' => $value
                ]);
            }
        }
    }
}
