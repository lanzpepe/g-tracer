<?php

declare(strict_types = 1);

namespace App\Charts;

use App\Models\Course;
use App\Models\Department;
use App\Models\Graduate;
use Chartisan\PHP\Chartisan;
use ConsoleTVs\Charts\BaseChart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GraduateStatisticsChart extends BaseChart
{
    /**
     * Determines the chart name to be used on the
     * route. If null, the name will be a snake_case
     * version of the class name.
     */
    public ?string $name = 'statistics';

    /**
     * Determines the prefix that will be used by the chart
     * endpoint.
     */
    public ?string $prefix = 'graduates';

    /**
     * Determines the middlewares that will be applied
     * to the chart endpoint.
     */
    public ?array $middlewares = ['auth'];

    /**
     * Handles the HTTP request for the given chart.
     * It must always return an instance of Chartisan
     * and never a string or an array.
     */
    public function handler(Request $request): Chartisan
    {
        $department = $request->exists('dept') ? Department::find($request->query('dept')) : Auth::user()->department;
        
        $courses = Course::whereHas('departments', fn ($query) =>
            $query->where('id', $department->id)
        )->whereHas('schools', fn ($query) =>
            $query->where('id', Auth::user()->school->id)
        )->orderBy('code')->get();

        $years = Graduate::whereHas('academic.course', fn ($query) =>
            $query->where('department', $department->name)->where('school', Auth::user()->school->name)
        )->get()->groupBy(['academic.year', 'academic.course.code'])->map(fn ($item) =>
            $item->map(fn ($item) => $item->count())
        )->sortKeys();

        $chartisan = Chartisan::build();
        $chartisan->labels($years->keys()->all());

        foreach ($courses as $course) {
            $values = collect();
            $years->map(fn ($item) => $values->push($item->map(fn ($item, $key) =>
                $course->code == $key ? $item : 0)->sum())
            );
            $chartisan->dataset($course->code, $values->all());
        }

        return $chartisan;
    }
}
