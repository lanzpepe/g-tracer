<?php

declare(strict_types = 1);

namespace App\Charts;

use App\Models\Department;
use App\Models\EmploymentStatus;
use App\Models\Graduate;
use Chartisan\PHP\Chartisan;
use ConsoleTVs\Charts\BaseChart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GraduateEmploymentChart extends BaseChart
{
    /**
     * Determines the chart name to be used on the
     * route. If null, the name will be a snake_case
     * version of the class name.
     */
    public ?string $name = 'employment';

    /**
     * Determines the prefix that will be used by the chart
     * endpoint.
     */
    public ?string $prefix = 'graduates';

    /**
     * Determines the middlewares that will be applied
     * to the chart endpoint.
     */
    public ?array $middlewares = ['auth'];

    /**
     * Handles the HTTP request for the given chart.
     * It must always return an instance of Chartisan
     * and never a string or an array.
     */
    public function handler(Request $request): Chartisan
    {
        $department = $request->exists('dept') ? Department::find($request->query('dept')) : Auth::user()->department;
        
        $status = EmploymentStatus::all();
        $graduates = Graduate::whereHas('academic', fn ($query) =>
            $query->where('department', $department->name)->where('school', Auth::user()->school->name)
        )->get()->groupBy('academic.year')->sortKeys();

        $employed = $graduates->map(fn ($item) =>
            $item->map(fn ($item) => $item->confirmed->where('status', $status->first()->description)->count())->sum()
        );

        $unemployed = $graduates->map(fn ($item) =>
            $item->map(fn ($item) => $item->confirmed->where('status', $status->last()->description)->count())->sum()
        );

        return Chartisan::build()
            ->labels($graduates->keys()->all())
            ->dataset('Employed', $employed->values()->all())
            ->dataset('Unemployed', $unemployed->values()->all());
    }
}
