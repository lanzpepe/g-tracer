<?php

declare(strict_types = 1);

namespace App\Charts;

use App\Models\Graduate;
use Chartisan\PHP\Chartisan;
use ConsoleTVs\Charts\BaseChart;
use Illuminate\Http\Request;

class CompanyResponsesChart extends BaseChart
{
    /**
     * Determines the chart name to be used on the
     * route. If null, the name will be a snake_case
     * version of the class name.
     */
    public ?string $name = 'company';

    /**
     * Determines the prefix that will be used by the chart
     * endpoint.
     */
    public ?string $prefix = 'responses';

    /**
     * Determines the middlewares that will be applied
     * to the chart endpoint.
     */
    public ?array $middlewares = ['auth', 'dept'];

    /**
     * Handles the HTTP request for the given chart.
     * It must always return an instance of Chartisan
     * and never a string or an array.
     */
    public function handler(Request $request): Chartisan
    {
        $responses = Graduate::find($request->query('id'))->responses;
        $names = $responses->groupBy('company.name')->map(fn ($item) => $item->count())
                ->sort()->reverse();

        return Chartisan::build()
            ->labels($names->keys()->all())
            ->dataset('Respondents', $names->values()->all());
    }
}
