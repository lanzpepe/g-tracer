<?php

namespace App\Providers;

use App\Events\GraduateAdded;
use App\Events\ImageAdded;
use App\Events\TwitterFriendsSaved;
use App\Listeners\LogNotification;
use App\Listeners\StoreGraduateImage;
use App\Listeners\StoreGraduateLatLng;
use App\Listeners\StoreTwitterFriendIds;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Notifications\Events\NotificationSent;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        GraduateAdded::class => [
            StoreGraduateLatLng::class,
        ],
        ImageAdded::class => [
            StoreGraduateImage::class,
        ],
        TwitterFriendsSaved::class => [
            StoreTwitterFriendIds::class,
        ],
        NotificationSent::class => [
            LogNotification::class
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
