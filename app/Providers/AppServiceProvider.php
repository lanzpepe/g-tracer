<?php

namespace App\Providers;

use App\Charts\CompanyResponsesChart;
use App\Charts\GraduateEmploymentChart;
use App\Charts\GraduateStatisticsChart;
use App\Charts\JobPositionResponsesChart;
use App\Charts\RemarkResponsesChart;
use App\Models\Admin;
use App\Models\Batch;
use App\Models\Company;
use App\Models\Department;
use App\Models\EmploymentStatus;
use App\Models\Gender;
use App\Models\Occupation;
use App\Models\Role;
use App\Models\School;
use App\Models\SchoolYear;
use ConsoleTVs\Charts\Registrar as Charts;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Charts $charts)
    {
        view()->composer('layouts.home', function ($view) {
            $view->with('count', Auth::user()->unreadNotifications->count());
            $view->with('admin', Auth::user()->role === config('constants.roles.admin'));
        });

        view()->composer('components.*', function ($view) {
            $view->with('genders', Gender::all());
            $view->with('roles', Role::all()->sortBy('name'));
            $view->with('months', range(1, 12));
            $view->with('days', range(1, 31));
            $view->with('years', SchoolYear::pluck('year')->reverse()->all());
            $view->with('companies', Company::all()->sortBy('name'));
            $view->with('jobs', Occupation::all()->sortBy('name'));
            $view->with('statuses', EmploymentStatus::all());
            $view->with('batches', Batch::all());
        });

        view()->composer(['administrator.*', 'department.*'], function ($view) {
            $view->with('admins', Admin::orderBy('username')->paginate(10));
            $view->with('batches', Batch::all());
            $view->with('depts', Department::orderBy('name')->paginate(9));
            $view->with('schools', School::orderBy('name')->paginate(9));
            $view->with('years', collect(SchoolYear::pluck('year')->reverse()->all()));
        });

        Paginator::defaultView('vendor.pagination.semantic-ui');
        Paginator::defaultSimpleView('vendor.pagination.semantic-ui');

        $charts->register([
            CompanyResponsesChart::class, JobPositionResponsesChart::class,
            RemarkResponsesChart::class, GraduateEmploymentChart::class,
            GraduateStatisticsChart::class
        ]);
    }
}
