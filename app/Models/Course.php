<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $guarded = [];
    public $incrementing = false;
    public $keyType = 'string';

    public function academic()
    {
        return $this->hasMany(Academic::class, 'course_id');
    }

    public function schools()
    {
        return $this->belongsToMany(School::class, 'course_school', 'course_id', 'school_id')->withTimestamps();
    }

    public function departments()
    {
        return $this->belongsToMany(Department::class, 'department_course', 'course_id', 'dept_id')->withTimestamps();
    }

    public function occupations()
    {
        return $this->belongsToMany(Occupation::class, 'course_job', 'course_id', 'job_id')->withTimestamps();
    }

    public function verifications()
    {
        return $this->hasMany(Verification::class, 'course_id');
    }
}
