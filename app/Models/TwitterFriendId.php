<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TwitterFriendId extends Model
{
    protected $guarded = [];
    public $incrementing = false;
    public $keyType = 'string';

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
