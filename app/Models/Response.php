<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Response extends Model
{
    protected $guarded = [];
    protected $primaryKey = 'response_id';
    public $incrementing = false;
    public $keyType = 'string';

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

    public function graduates()
    {
        return $this->belongsToMany(Graduate::class, 'user_response', 'response_id', 'graduate_id')
            ->as('respondent')->withPivot('user_id')->withTimestamps()->latest();
    }
}
