<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employment extends Model
{
    protected $guarded = [];
    public $incrementing = false;
    public $keyType = 'string';

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_employment', 'employment_id', 'user_id')->withTimestamps();
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

    public function confirmations()
    {
        return $this->hasMany(Confirmation::class);
    }
}
