<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Confirmed extends Model
{
    protected $guarded = [];
    protected $table = 'confirmed_list';
    public $incrementing = false;
    public $keyType = 'string';

    public function graduate()
    {
        return $this->belongsTo(Graduate::class);
    }

    public function employment()
    {
        return $this->belongsTo(Employment::class);
    }
}
