<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LinkedInProfile extends Model
{
    protected $guarded = [];
    protected $table = 'linkedin_profiles';
    public $incrementing = false;
    public $keyType = 'string';
}
