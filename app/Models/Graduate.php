<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Graduate extends Model
{
    protected $guarded = [];
    protected $primaryKey = 'graduate_id';
    public $incrementing = false;
    public $keyType = 'string';

    public function contacts()
    {
        return $this->hasMany(Contact::class, 'graduate_id');
    }

    public function academic()
    {
        return $this->hasOne(Academic::class, 'graduate_id');
    }

    public function employments()
    {
        return $this->hasMany(Employment::class, 'graduate_id');
    }

    public function confirmed()
    {
        return $this->hasMany(Confirmed::class, 'graduate_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function person()
    {
        return $this->belongsTo(Person::class, 'person_id');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_graduate', 'graduate_id', 'user_id')
            ->as('respondent')->withPivot('response_id')->withTimestamps()->latest();
    }

    public function responses()
    {
        return $this->belongsToMany(Response::class, 'user_graduate', 'graduate_id', 'response_id')
            ->as('respondent')->withPivot('user_id')->withTimestamps()->latest();
    }

    public function getFullNameAttribute()
    {
        return "{$this->person->first_name} {$this->person->middle_name} {$this->person->last_name}";
    }

    public function getNameAttribute()
    {
        $middleName = Str::substr($this->person->middle_name, 0, 1);

        return "{$this->person->first_name} {$middleName} {$this->person->last_name}";
    }

    public function getCourseAttribute()
    {
        return $this->academic->course->title;
    }

    public function getMajorAttribute()
    {
        return $this->academic->course->major;
    }

    public function getDegreeProgramAttribute()
    {
        return $this->academic->course->major == 'None' ? $this->academic->course->title : "{$this->academic->course->title} - {$this->academic->course->major}";
    }

    public function getDateGraduatedAttribute()
    {
        return "{$this->academic->month} {$this->academic->day}, {$this->academic->year}";
    }

    public function getAddressAttribute()
    {
        return $this->contacts->first()->address;
    }

    public function getContactAttribute()
    {
        return $this->contacts->first()->contact_number ?? 'None';
    }

    public function getEmploymentStatusAttribute()
    {
        return $this->confirmed->first()->status;
    }

    public function getCompanyNameAttribute()
    {
        return $this->confirmed->first()->employment->company->name;
    }

    public function getCompanyAddressAttribute()
    {
        return $this->confirmed->first()->employment->company->address;
    }

    public function getCompanyContactAttribute()
    {
        return $this->confirmed->first()->employment->company->contact_number;
    }

    public function getJobPositionAttribute()
    {
        return $this->confirmed->first()->employment->job_position;
    }

    public function getDateEmployedAttribute()
    {
        return $this->confirmed->first()->employment->date_employed;
    }

    public function getRemarksAttribute()
    {
        return $this->confirmed->first()->remarks;
    }

    public function getStatusAttribute()
    {
        return $this->confirmed->first() == null ? 'Open' : 'Closed';
    }

    public function getImageAttribute()
    {
        $defaultImage = $this->person->gender == 'Male' ? 'default_avatar_m.png' : 'default_avatar_f.png';
        $imagePath = Str::of(rawurldecode($this->image_uri))->replace('+', ' ')->__toString();

        if ($this->image_uri != null) {
            return $imagePath;
        }

        return asset("img/app/{$defaultImage}");
    }
}
