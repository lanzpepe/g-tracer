<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VerificationRequest extends Model
{
    protected $guarded = [];
    public $incrementing = false;
    public $keyType = 'string';

    public function person()
    {
        return $this->belongsTo(Person::class, 'person_id');
    }

    public function course()
    {
        return $this->belongsTo(Course::class, 'course_id');
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class, 'admin_id');
    }
}
