<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $guarded = [];
    protected $primaryKey = 'user_id';
    public $incrementing = false;
    public $keyType = 'string';

    public function admin()
    {
        return $this->hasOne(Admin::class, 'user_id');
    }

    public function contacts()
    {
        return $this->hasMany(Contact::class, 'user_id')->latest();
    }

    public function graduate()
    {
        return $this->hasOne(Graduate::class, 'user_id');
    }

    public function providerAccounts()
    {
        return $this->hasMany(ProviderAccount::class, 'user_id');
    }

    public function twitterFriendIds()
    {
        return $this->hasMany(TwitterFriendId::class, 'user_id');
    }

    public function preference()
    {
        return $this->hasOne(Preference::class, 'user_id');
    }

    public function achievement()
    {
        return $this->hasOne(Achievement::class, 'user_id');
    }

    public function person()
    {
        return $this->belongsTo(Person::class, 'person_id');
    }

    public function employments()
    {
        return $this->belongsToMany(Employment::class, 'user_employment', 'user_id', 'employment_id')->withTimestamps();
    }

    public function graduates()
    {
        return $this->belongsToMany(Graduate::class, 'user_graduate', 'user_id', 'graduate_id')
            ->as('respondent')->withPivot('response_id')->orderByDesc('user_graduate.updated_at')->withTimestamps();
    }

    public function responses()
    {
        return $this->belongsToMany(Response::class, 'user_graduate', 'user_id', 'response_id')
            ->as('respondent')->withPivot('graduate_id')->orderByDesc('user_graduate.updated_at')->withTimestamps();
    }

    public function rewards()
    {
        return $this->belongsToMany(Reward::class, 'user_reward', 'user_id', 'reward_id')
            ->withPivot('id', 'status')->orderByDesc('user_reward.updated_at')->withTimestamps();
    }

    public function getFullNameAttribute()
    {
        return "{$this->person->first_name} {$this->person->middle_name} {$this->person->last_name}";
    }

    public function getDepartmentAttribute()
    {
        return $this->graduate->academic->department;
    }

    public function getSchoolAttribute()
    {
        return $this->graduate->academic->school;
    }

    public function getImageAttribute()
    {
        $defaultImage = $this->person->gender == 'Male' ? 'default_avatar_m.png' : 'default_avatar_f.png';
        $imagePath = rawurldecode($this->image_uri);

        if ($this->image_uri != null) {
            return $imagePath;
        }

        return asset("img/app/{$defaultImage}");
    }

    public function routeNotificationForFcm($notification)
    {
        return $this->device_token;
    }
}
