<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $guarded = [];
    protected $primaryKey = 'company_id';
    public $incrementing = false;
    public $keyType = 'string';

    public function employments()
    {
        return $this->hasMany(Employment::class, 'company_id');
    }

    public function responses()
    {
        return $this->hasMany(Response::class, 'company_id', 'response_id');
    }
}
