<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $guarded = [];
    protected $primaryKey = 'person_id';
    public $incrementing = false;
    public $keyType = 'string';

    public function user()
    {
        return $this->hasOne(User::class, 'person_id');
    }

    public function graduate()
    {
        return $this->hasOne(Graduate::class, 'person_id');
    }

    public function verifications()
    {
        return $this->hasMany(VerificationRequest::class, 'person_id');
    }

    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->middle_name} {$this->last_name}";
    }
}
