<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProviderAccount extends Model
{
    protected $guarded = [];
    protected $primaryKey = 'provider_id';
    public $incrementing = false;
    public $keyType = 'string';

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
