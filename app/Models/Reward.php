<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Reward extends Model
{
    protected $guarded = [];
    public $incrementing = false;
    public $keyType = 'string';

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_reward', 'reward_id', 'user_id')->withPivot('id', 'status')->withTimestamps();
    }

    public function admins()
    {
        return $this->belongsToMany(Admin::class, 'admin_reward', 'reward_id', 'admin_id')->withPivot('quantity')->withTimestamps();
    }

    public function getQuantityAttribute()
    {
        return $this->admins->first()->pivot->quantity;
    }

    public function getImageAttribute()
    {
        if ($this->image_uri != null) {
            return Str::of(rawurldecode($this->image_uri))->replace('+', ' ')->__toString();
        }

        return asset('img/app/logo.png');
    }
}
