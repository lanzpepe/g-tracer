<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\URL;

class UserRespond extends Notification implements ShouldQueue
{
    use Queueable;

    private $graduate, $url;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($graduate)
    {
        $this->graduate = $graduate;
        $this->url = URL::to("/d/graduates/{$graduate->graduate_id}");
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    public function toDatabase($notifiable)
    {
        return [
            'graduate_id' => $this->graduate->graduate_id,
            'title' => 'Response Activity',
            'body' => "Someone has responded {$this->graduate->name}",
            'click_action' => $this->url
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
