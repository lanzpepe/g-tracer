<?php

namespace App\Http\Controllers\Api;

use App\Events\GraduateAdded;
use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Graduate;
use App\Models\Person;
use App\Models\ProviderAccount;
use App\Models\Response;
use App\Models\User;
use App\Notifications\UserRespond;
use App\Traits\IssueTokenTrait;
use App\Traits\StaticTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Laravel\Passport\Client;

class SocialAuthController extends Controller
{
    use IssueTokenTrait, StaticTrait;

    private $client;

    public function __construct()
    {
        $this->client = Client::find(1);
    }

    public function login(Request $request)
    {
        $socialAccount = ProviderAccount::where('provider_name', $request->provider_name)
        ->where('provider_id', $request->provider_id)->first();

        if ($socialAccount) {
            return $this->issueToken($request, 'social');
        }
        else {
            return response()->json(['message' => "Account does not exists."], 404);
        }
    }

    public function register(Request $request)
    {
        $graduate = $this->search($request);

        if ($graduate) {
            $person = Person::firstOrCreate([
                'last_name' => $request->lastName,
                'first_name' => $request->firstName,
                'middle_name' => $request->middleName,
                'gender' => $request->gender,
                'birth_date' => $request->birthDate
            ], [
                'person_id' => Str::random()
            ]);

            $user = User::create([
                'user_id' => Str::uuid(),
                'image_uri' => $request->imageUri,
                'person_id' => $person->person_id
            ]);

            $user->graduate()->save($graduate);

            $graduate->contacts()->create([
                'contact_id' => Str::random(),
                'address' => $request->address,
                'contact_number' => $request->contactNumber,
                'email' => $request->email
            ]);

            $user->preference()->create([
                'id' => Str::random(),
                'school' => $request->school,
                'department' => $request->department,
                'degree' => $request->degree,
                'major' => $request->major,
                'school_year' => $request->schoolYear,
                'batch' => $request->batch
            ]);

            $user->achievement()->create([
                'id' => Str::random(),
                'points' => 0,
                'exp_points' => 0,
                'rank_id' => 1
            ]);

            $this->addSocialAccount($request, $user);
            $this->addEmploymentResponse($request, $user, $graduate);

            event(new GraduateAdded($graduate));

            return $this->issueToken($request, 'social');
        }

        return response()->json([
            'message' => 'There is something wrong with the server. Please try again.'
        ], 500);
    }

    private function addEmploymentResponse(Request $request, User $user, Graduate $graduate)
    {
        $company = Company::create([
            'company_id' => Str::random(),
            'name' => $request->companyName,
            'address' => $request->companyAddress
        ]);

        $employment = $company->employments()->create([
            'id' => Str::random(),
            'job_position' => $request->jobPosition,
            'date_employed' => $request->dateEmployed
        ]);

        $user->employments()->attach($employment->id);

        $response = Response::create([
            'response_id' => Str::random(),
            'job_position' => $request->jobPosition,
            'remarks' => $request->remarks,
            'company_id' => $company->company_id
        ]);

        $user->graduates()->syncWithoutDetaching([
            $graduate->graduate_id => ['response_id' => $response->response_id]
        ]);

        $admin = $this->findAdmin($graduate);

        $admin->notify(new UserRespond($graduate));

    }

    private function addSocialAccount(Request $request, User $user)
    {
        $this->validate($request, [
    		'provider_name' => ['required', Rule::unique('provider_accounts')->where(function ($query) use ($user) {
    			return $query->where('user_id', $user->user_id);
    		})],
    		'provider_id' => 'required'
        ]);

        $user->providerAccounts()->create([
            'provider_id' => $request->provider_id,
            'provider_name' => $request->provider_name
        ]);
    }

    public function logout()
    {
        foreach (auth('api')->user()->tokens as $token) {
            $token->revoke();
        }

        return response()->json(['message' => "Logged out successfully!"]);
    }
}
