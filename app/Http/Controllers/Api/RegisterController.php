<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Course;
use App\Models\LinkedInProfile;
use App\Models\Person;
use App\Models\User;
use App\Notifications\UserVerify;
use App\Traits\StaticTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    use StaticTrait;

    public function linkedin(Request $request)
    {
        $profile = LinkedInProfile::where('last_name', $request->lastName)
                    ->where('first_name', $request->firstName)
                    ->firstOrFail();

        return response()->json(compact('profile'));
    }

    public function verify(Request $request)
    {
        $graduate = $this->search($request);

        if ($graduate) {
            return response()->json([
                'title' => 'Verification Message',
                'message' => 'Your information have matched with the records from the department. Please confirm to proceed.'
            ]);
        }

        return response()->json([
            'title' => 'Verification Message',
            'message' => 'Your information did not match with the records from the department. Would you like to submit an application for verification?'
        ], 404);
    }

    public function verification(Request $request)
    {
        $person = Person::firstOrCreate([
            'last_name' => $request->lastName,
            'first_name' => $request->firstName,
            'middle_name' => $request->middleName,
            'gender' => $request->gender,
            'birth_date' => $request->birthDate
        ], [
            'person_id' => Str::random()
        ]);

        $course = Course::where('title', $request->degree)->where('major', $request->major)->first();

        $admins = Admin::whereHas('departments', fn ($query) =>
            $query->where('departments.name', $request->department)
        )->whereHas('schools', fn ($query) =>
            $query->where('schools.name', $request->school)
        )->whereHas('roles', fn ($query) =>
            $query->where('roles.name', config('constants.roles.dept'))
        )->get();

        foreach ($admins as $admin) {
            $person->verifications()->create([
                'id' => Str::random(),
                'address' => $request->address,
                'contact_number' => $request->contactNumber,
                'department' => $request->department,
                'school' => $request->school,
                'month' => $request->batch,
                'year' => $request->schoolYear,
                'course_id' => $course->id,
                'admin_id' => $admin->admin_id
            ]);
            $admin->notify(new UserVerify());
        }

        return response()->json([
            'title' => 'Confirmation Message',
            'message' => 'Your information has been sent for verification. Please try to verify your data again later.'
        ]);
    }

    public function token(Request $request)
    {
        $user = User::findOrFail($request->userId);

        $user->update(['device_token' => $request->token]);

        return response()->json([
            'message' => 'Token saved successfully.'
        ]);
    }
}
