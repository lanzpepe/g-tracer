<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Graduate;
use App\Models\Point;
use App\Models\Response;
use App\Models\User;
use App\Notifications\UserRespond;
use App\Traits\StaticTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class GraduateController extends Controller
{
    use StaticTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('api');
    }

    /**
     * Display the list of stored graduates.
     *
     * @return \Illuminate\Http\Response
     */
    public function graduates()
    {
        $graduates = Graduate::with([
            'person', 'contacts', 'academic.course', 'user.person',
            'users', 'responses.company', 'confirmed'
        ])->withCount('responses')->get();

        return response()->json(compact('graduates'));
    }

    /**
     * Display the specified graduate.
     *
     * @param string $id
     * @return \Illuminate\Http\Response
     */
    public function graduate($id)
    {
        $graduate = Graduate::with([
            'person', 'contacts', 'academic.course', 'user.person',
            'users', 'responses.company', 'confirmed'
        ])->withCount('responses')->find($id);

        return response()->json(compact('graduate'));
    }

    /**
     * Store the newly saved graduate in the database.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $user = User::find(Auth::user()->user_id);
        $graduate = Graduate::find($request->graduateId);

        if ($graduate) {
            $user->graduates()->attach($graduate->graduate_id);
        }

        return response()->json([
            'message' => "You have tagged {$graduate->full_name}."
        ]);
    }

    /**
     * Store the user's response of graduate in the database.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function response(Request $request)
    {
        $user = User::find(Auth::user()->user_id);
        $graduate = Graduate::find($request->graduateId);
        $company = Company::firstOrCreate([
            'name' => $request->companyName,
            'address' => $request->companyAddress
        ], [
            'company_id' => Str::random()
        ]);
        $response = Response::create([
            'response_id' => Str::random(),
            'job_position' => $request->jobPosition,
            'remarks' => $request->remarks,
            'company_id' => $company->company_id
        ]);

        $user->graduates()->syncWithoutDetaching([
            $graduate->graduate_id => ['response_id' => $response->response_id]
        ]);

        $admin = $this->findAdmin($graduate);
        $points = Point::where('admin_id', $admin->admin_id)->first();

        $admin->notify(new UserRespond($graduate));

        $user->achievement()->update([
            'points' => $user->achievement->points + $points->submitted,
            'exp_points' => $user->achievement->exp_points + ($points->submitted * 10)
        ]);

        return response()->json([
            'message' => "Your response has been submitted. A reward of {$points->submitted} point(s) is credited to your account."
        ]);
    }
}
