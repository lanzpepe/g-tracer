<?php

namespace App\Http\Controllers\Api;

use App\Events\TwitterFriendsSaved;
use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Graduate;
use App\Models\User;
use App\Traits\StaticTrait;
use App\Helpers\GraduateSimilarity;
use App\Models\ProviderAccount;
use App\Models\Reward;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class UserController extends Controller
{
    use StaticTrait;

    private $token;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('api');
        $this->token = env('TWITTER_ACCESS_TOKEN');
    }

    /**
     * Display the list of existing users.
     *
     * @return \Illuminate\Http\Response
     */
    public function users()
    {
        $users = User::with([
            'person', 'providerAccounts', 'graduate.contacts' => fn ($query) => $query->orderByDesc('contacts.updated_at'),
            'graduate.academic.course', 'employments.company', 'achievement.rank', 'preference', 'responses.company',
            'rewards', 'graduates.person', 'graduates.academic.course', 'graduate.confirmed.employment.company'
        ])->get();

        return response()->json(compact('users'));
    }

    /**
     * Display the specified user.
     *
     * @return \Illuminate\Http\Response
     */
    public function user()
    {
        $user = User::with([
            'person' ,'providerAccounts', 'graduate.contacts' => fn ($query) => $query->orderByDesc('contacts.updated_at'),
            'graduate.academic.course', 'employments.company', 'achievement.rank', 'preference', 'responses.company',
            'rewards', 'graduates' => fn ($query) => $query->where('graduates.user_id', '<>', Auth::user()->user_id)
            ->orWhereNull('graduates.user_id')->with(['person', 'academic.course']), 'graduate.confirmed.employment.company'
        ])->find(Auth::user()->user_id);

        return response()->json(compact('user'));
    }

    /**
     * Display the recommended graduates based on similar attributes.
     *
     * @return \Illuminate\Http\Response
     */
    public function graduates() {
        $user = User::find(Auth::user()->user_id);
        // $graduates = Graduate::all();
        $graduates = Graduate::whereHas('academic', fn ($query) => $query->whereBetween('year', ['2015', '2020']))->get();
        $graduateSimilarity = new GraduateSimilarity($graduates);
        $latest = $user->preference->updated_at->gt($user->graduates->first()->getOriginal()['pivot_updated_at']);
        $preferences = collect();

        $preferences->put('school', $latest ? $user->preference->school : $user->graduates->first()->academic->school);
        $preferences->put('department', $latest ? $user->preference->department : $user->graduates->first()->academic->department);
        $preferences->put('degree', $latest ? $user->preference->degree : $user->graduates->first()->course);
        $preferences->put('major', $latest ? $user->preference->major : $user->graduates->first()->major);
        $preferences->put('school_year', $latest ? $user->preference->school_year : $user->graduates->first()->academic->year);
        $preferences->put('batch', $latest ? $user->preference->batch : $user->graduates->first()->academic->month);

        $similarityScores = $graduateSimilarity->calculateSimilarityMatrix($user, $preferences);
        $graduates = $graduateSimilarity->getGraduatesSortedBySimilarity($similarityScores, $user);

        event(new TwitterFriendsSaved($user));

        return response()->json(compact('graduates'));
    }

    /**
     * Store the Twitter user's information to the database.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function socialAccount(Request $request)
    {
        $user = User::find(Auth::user()->user_id);

        ProviderAccount::updateOrCreate([
            'user_id' => $user->user_id,
            'provider_name' => $request->providerName
        ], [
            'provider_id' => $request->providerId,
            'profile_id' => $request->username
        ]);

        return response()->json([
            'message' => 'Twitter account added successfully.'
        ]);
    }

    /**
     * Update the existing data of current user.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function employment(Request $request)
    {
        $user = User::find(Auth::user()->user_id);

        $company = Company::firstOrCreate([
            'name' => $request->companyName,
            'address' => $request->companyAddress
        ], [
            'company_id' => Str::random()
        ]);

        $user->employments()->create([
            'company_id' => $company->company_id,
            'job_position' => $request->jobPosition,
            'date_employed' => $request->dateEmployed
        ]);

        return response()->json(['message' => "Your profile has been updated."]);
    }

    /**
     * Set preference according to the user's interests.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function preference(Request $request)
    {
        $user = User::find(Auth::user()->user_id);

        $user->preference()->update([
            'school' => $request->school,
            'department' => $request->department,
            'degree' => $request->degree,
            'major' => $request->major,
            'school_year' => $request->schoolYear,
            'batch' => $request->batch
        ]);

        return response()->json([
            'message' => 'User preference saved successfully.'
        ]);
    }

    /**
     * Record transaction history of reward items from the exchanged points.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function reward(Request $request)
    {
        $user = User::find(Auth::user()->user_id);
        $reward = Reward::find($request->rewardId);

        $user->achievement()->update([
            'points' => ($user->achievement->points - $request->rewardPoints)
        ]);
        $user->rewards()->attach($request->rewardId, ['id' => Str::random()]);

        $reward->admins->each(function ($item) use ($user) {
            if ($item->department->name === $user->department && $item->school->name === $user->school) {
                return $item->pivot->update(['quantity' => $item->pivot->quantity - 1]);
            }
        });

        return response()->json([
            'message' => "Confirmation sent. Just present your reference # to the department office to claim your reward.\n\nReference #: {$user->rewards->first()->pivot->id}"
        ]);
    }
}
