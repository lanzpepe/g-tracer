<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Batch;
use App\Models\Company;
use App\Models\Gender;
use App\Models\Response;
use App\Models\Reward;
use App\Models\School;
use App\Models\SchoolYear;

class ListController extends Controller
{
    public function genders()
    {
        $genders = Gender::orderByDesc('name')->get();

        return response()->json(compact('genders'));
    }

    public function schools()
    {
        $schools = School::with('departments.courses')->get();

        return response()->json(compact('schools'));
    }

    public function courses()
    {
        $schools = School::with('courses.departments')->get();

        return response()->json(compact('schools'));
    }

    public function schoolYears()
    {
        $school_years = SchoolYear::orderByDesc('year')->get();

        return response()->json(compact('school_years'));
    }

    public function batches()
    {
        $batches = Batch::all();

        return response()->json(compact('batches'));
    }

    public function jobs()
    {
        $employments = Response::all()->unique('job_position')->values()->all();

        return response()->json(compact('employments'));
    }

    public function companies()
    {
        $companies = Company::all()->unique('name')->values()->all();

        return response()->json(compact('companies'));
    }

    public function rewards()
    {
        $rewards = Reward::with(['admins.departments'])->whereHas('admins')->get();

        return response()->json(compact('rewards'));
    }
}
