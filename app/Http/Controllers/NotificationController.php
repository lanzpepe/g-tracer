<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Graduate;
use App\Notifications\UserRespond;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notifications = Auth::user()->notifications;

        return view('components.notification', compact('notifications'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->ajax()) {
            $admin = Admin::where('admin_id', $request->admin);
            $graduate = Graduate::where('graduate_id', $request->graduate);

            $admin->notify(new UserRespond($graduate));
        }

        return response('Notification stored successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $notifications = Auth::user()->notifications;

        if (request()->ajax()) {
            foreach ($notifications as $notification) {
                if ($notification->id === $id) {
                    $notification->markAsRead();
                }
            }
        }

        return response('Notification displayed successfully!');
    }
}
