<?php

namespace App\Http\Controllers;

use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function storeToken(Request $request)
    {
        if ($request->ajax()) {
            $result = Auth::user()->user->update([
                'device_token' => $request->token
            ]);

            if ($result) {
                echo('Token successfully added.');
                return;
            }
        }

        echo('Failed to add token.');
    }

    public function showPages()
    {
        $pages = Auth::user()->pages;

        return response()->json(compact('pages'));
    }

    public function storePage(Request $request)
    {
        if ($request->ajax()) {
            $page = Page::updateOrCreate([
                'url'=> $request->url
            ],[
                'title' => $request->title,
                'description' => $request->description
            ]);

            $page->admins()->syncWithoutDetaching(Auth::user()->admin_id);
        }
    }
}
