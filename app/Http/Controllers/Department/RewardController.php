<?php

namespace App\Http\Controllers\Department;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreRewardRequest;
use App\Models\Reward;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class RewardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'dept']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rewards = Reward::whereHas('admins', function ($query) {
            return $query->where('admins.admin_id', Auth::user()->admin_id);
        })->paginate(9);

        return view('department.rewards', compact('rewards'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRewardRequest $request)
    {
        $imagePath = null;
        $id = Auth::user()->admin_id;
        $data = $request->validated();
        $reward = Reward::find($request->reward);

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $directory = "files/{$id}/images/rewards";

            if (Storage::makeDirectory("public/{$directory}")) {
                $imagePath = $file->storeAs($directory, $file->getClientOriginalName(), 'public');
                $image = Image::make(public_path("storage/{$imagePath}"))->orientate();
                $image->resize(800, 800); $image->save();
                $imagePath = Storage::url($imagePath);
            }
        }

        $reward = Reward::updateOrCreate([
            'id' => $reward->id ?? Str::random()
        ], [
            'name' => $data['name'],
            'description' => $data['description'],
            'points' => $data['points'],
            'image_uri' => $imagePath ?? $reward->image_uri ?? null
        ]);

        if ($request->btnReward == 'added') {
            $reward->admins()->attach($id, ['quantity' => $data['quantity']]);
        }
        else {
            $reward->admins()->syncWithoutDetaching([$id => ['quantity' => $data['quantity']]]);
        }

        return back()->with('success', "Item {$request->btnReward} successfully.");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $reward = Reward::with('admins')->find($id);

        return view('department.reward', compact('reward'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reward = Reward::with('admins')->find($id);

        return response()->json(compact('reward'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $reward = Reward::find($id);

        $reward->users->each(function ($item) use ($request) {
            if ($item->pivot->id === $request->reference) {
                $item->pivot->update(['status' => 1]);
            }
        });

        return back()->with('success', 'Reward item accepted successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reward = Reward::find($id);

        if ($reward) {
            $reward->delete();
        }

        return redirect()->route('rewards.index')->with('success', "Item removed successfully.");
    }
}
