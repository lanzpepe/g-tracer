<?php

namespace App\Http\Controllers\Department;

use App\Events\GraduateAdded;
use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Graduate;
use App\Models\Person;
use App\Models\VerificationRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class VerificationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'dept']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = Course::whereHas('departments', fn ($query) =>
            $query->where('id', Auth::user()->department->id)
        )->orderBy('title')->get();

        $verifications = VerificationRequest::whereHas('admin', fn ($query) =>
            $query->where('admin_id', Auth::user()->admin_id)
        )->get();

        return view('department.verification', compact('courses' ,'verifications'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $course = Course::where('code', $request->course)->where('major', $request->major)->first();
        $month = date('F', mktime(0, 0, 0, $request->month, 10));
        $verification = VerificationRequest::find($request->verification);

        $academic = [
            'course_id' => $course->id,
            'department' => Auth::user()->department->name,
            'school' => Auth::user()->school->name,
            'year' => $request->year,
            'month' => $month,
            'day' => $request->day
        ];

        $contact = [
            'address' => $request->address,
            'contact_number' => $request->contact
        ];

        $person = Person::firstOrCreate([
            'last_name' => $request->lastname,
            'first_name' => $request->firstname,
            'middle_name' => $request->midname,
            'gender' => $request->gender
        ], [
            'person_id' => Str::random()
        ]);

        $graduate = Graduate::updateOrCreate([
            'graduate_id' => $person->graduate->graduate_id ?? Str::random()
        ], [
            'image_uri' => $imagePath ?? $person->graduate->image_uri ?? null,
            'person_id' => $person->person_id
        ]);

        $academic['academic_id'] = Str::random();
        $contact['contact_id'] = Str::random();
        $graduate->academic()->create($academic);
        $graduate->contacts()->create($contact);

        $verification->update(['status' => 1]);

        event(new GraduateAdded($graduate));

        return back()->with('success', 'Data verification successful.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $verification = VerificationRequest::with(['person', 'course'])->find($id);

        return response()->json(compact('verification'));
    }
}
