<?php

namespace App\Http\Controllers\Department;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCourseRequest;
use App\Models\Course;
use App\Models\Department;
use App\Models\School;
use App\Traits\StaticTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class CourseController extends Controller
{
    use StaticTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $courses = Course::whereHas('departments', fn ($query) => $query->where('id', $user->department->id))
            ->whereHas('schools', fn ($query) => $query->where('id', $user->school->id))
            ->orderBy('title')->paginate(10);

        return view('department.courses', compact('courses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCourseRequest $request)
    {
        $data = $request->validated();
        $school = School::where('name', $data['school'])->first();
        $dept = Department::where('name', $data['dept'])->first();
        $major = $request->filled('major') ? $data['major'] : 'None';
        $result = Course::whereHas('departments', fn ($query) => $query->where('id', $dept->id))
            ->whereHas('schools', fn ($query) => $query->where('id', $school->id))
            ->where('code', $data['code'])->first();

        if ($request->btnCourse == 'added') {
            if (!$result) {
                $course = Course::firstOrCreate([
                    'title' => $this->capitalize($data['title']),
                    'major' => $this->capitalize($major),
                    'code' => strtoupper($data['code'])
                ], [
                    'id' => Str::random()
                ]);

                $course->departments()->attach($dept->id);
                $course->schools()->attach($school->id);

                return back()->with('success', "Degree program {$request->btnCourse} successfully.");
            }
            else {
                return back()->withInput()->withErrors(['Degree program already exists.']);
            }
        }
        else {
            $course = Course::updateOrCreate([
                'id' => $result->id
            ], [
                'title' => $this->capitalize($data['title']),
                'major' => $this->capitalize($major),
                'code' => strtoupper($data['code'])
            ]);

            $course->departments()->sync($dept->id);
            $course->schools()->sync($school->id);

            return back()->with('success', "Course {$request->btnCourse} successfully.");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $course = Course::with(['departments', 'schools'])->find($id);

        return response()->json(compact('course'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $course = Course::with(['departments' => fn ($query) => $query->where('id', $user->department->id),
            'schools' => fn ($query) => $query->where('id', $user->school->id)])->find($id);

        return response()->json(compact('course'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user(); $course = Course::find($id);

        if ($course) {
            $course->departments()->detach($user->department->id);
            $course->schools()->detach($user->school->id);
        }

        return back()->with('success', "Course removed successfully.");
    }
}
