<?php

namespace App\Http\Controllers\Department;

use App\Http\Controllers\Controller;
use App\Http\Requests\StorePointRequest;
use App\Models\Admin;
use App\Models\Point;
use Illuminate\Support\Facades\Auth;

class PointController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $point = Point::where('admin_id', Auth::user()->admin_id)->first();

        return response()->json(compact('point'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePointRequest $request)
    {
        $data = $request->validated();
        $admin = Admin::find(Auth::user()->admin_id);

        $admin->point()->update([
            'submitted' => $data['submitted'],
            'confirmed' => $data['confirmed']
        ]);

        return back()->with('success', 'Point threshold set successfully.');
    }
}
