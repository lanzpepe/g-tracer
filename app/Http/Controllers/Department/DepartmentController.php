<?php

namespace App\Http\Controllers\Department;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class DepartmentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'dept']);
    }

    public function home()
    {
        return view('layouts.home');
    }

    public function fileManager()
    {
        return view('layouts.file_manager');
    }

    public function profile()
    {
        $user = User::find(Auth::user()->user_id);

        return view('layouts.profile', compact('user'));
    }
}
