<?php

namespace App\Http\Controllers\Department;

use App\Http\Controllers\Controller;
use App\Models\Batch;
use App\Models\Course;
use App\Models\EmploymentStatus;
use App\Models\Graduate;
use App\Models\Occupation;
use Barryvdh\DomPDF\Facade as PDF;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'dept']);
    }

    public function reports(Request $request)
    {
        $school = Auth::user()->school->name;
        $department = Auth::user()->department->name;
        $courses = Course::whereHas('departments', fn ($query) =>
            $query->where('id', Auth::user()->department->id)
        )->whereHas('schools', fn ($query) =>
            $query->where('id', Auth::user()->school->id)
        )->orderBy('code')->get();
        $graduatess = Graduate::whereHas('academic', fn ($query) =>
            $query->where('department', $department)->where('school', $school)
        );
        $graduates = $graduatess->get();
        $programs = $graduates->groupBy('academic.degree_code')->map(fn ($item) =>
            $item->count()
        )->sortKeys()->all();

        $confirmed = $graduatess->has('confirmed')->count();
        $employed = $graduatess->whereHas('confirmed.employment')->count();
        $statuses = $this->getEmploymentStatusCount($school, $department);
        $relevance = $this->getJobRelatedToCourseCompletedCount($school, $department);
        $intervals = $this->getLagTimePriorToEmploymentCount($school, $department);

        $data = compact('confirmed', 'courses', 'department', 'employed', 'graduates', 'intervals', 'programs', 'relevance', 'statuses');

        return view('department.reports', $data);
    }

    public function generate(Request $request)
    {
        $request->validate([
            'course' => 'required',
            'year' => 'required|exists:school_years',
            'batch' => 'required|exists:batches,month'
        ]);

        $school = Auth::user()->school->name;
        $department = Auth::user()->department->name;

        $graduates = Graduate::whereHas('academic.course', fn ($query) =>
            $query->where('school', $school)->where('department', $department)
            ->where('code', $request->course)->where('year', $request->year)->where('month', $request->batch)
        )->whereHas('confirmed.employment')->get();

        $term = collect();
        $batches = Batch::all();
        $course = Course::where('code', $request->course)->first();

        foreach ($batches as $batch) {
            if ($batch->month === $request->batch) {
                switch ($batch->month) {
                    case 'March':
                        $term->push("{$batch->term}, SY ".($request->year - 1)."-{$request->year}");
                    break;
                    case 'May':
                        $term->push("Summer {$request->year}");
                    break;
                    case 'October':
                        $term->push("{$batch->term}, SY {$request->year}-".($request->year + 1));
                    break;
                }
            }
        }

        $data = compact('course', 'department', 'graduates', 'request', 'term');

        return view('department.report', $data);
    }

    public function download(Request $request)
    {
        $term = collect();
        $data = explode(',', $request->params);
        $school = Auth::user()->school->name;
        $department = Auth::user()->department->name;
        $batches = Batch::all();
        $year = $data[2];

        $graduates = Graduate::whereHas('academic.course', fn ($query) =>
            $query->where('school', $school)->where('department', $department)->where('code', $data[0])
            ->where('year', $year)->where('month', $data[1])
        );

        $confirmed = $graduates->whereHas('confirmed')->get();
        $employed = $graduates->whereHas('confirmed.employment')->get();
        $course = Course::where('code', $data[0])->first();

        foreach ($batches as $batch) {
            if ($batch->month === $data[1]) {
                switch ($batch->month) {
                    case 'March':
                        $term->put($batch->month, "{$batch->term}, SY ".($year - 1)."-{$year}");
                    break;
                    case 'May':
                        $term->put($batch->month, "Summer {$year}");
                    break;
                    case 'October':
                        $term->put($batch->month, "{$batch->term}, SY {$year}-".($year + 1));
                    break;
                }
            }
        }

        $data = compact('confirmed', 'course', 'department', 'employed', 'graduates', 'term');

        $pdf = PDF::loadView('department.download', $data);
        $pdf->setPaper('A4', 'landscape');

        return $pdf->download("{$term->first()} Graduate Report ($course->code).pdf");
    }

    private function getEmploymentStatusCount($school, $department)
    {
        $result = collect();

        foreach (EmploymentStatus::all() as $status) {
            $result->put($status->description, Graduate::whereHas('academic', fn ($query) =>
                $query->where('school', $school)->where('department', $department)
            )->whereHas('confirmed', fn ($query) =>
                $query->where('status', $status->description)
            )->count());
        }

        return $result->all();
    }

    private function getJobRelatedToCourseCompletedCount($school, $department)
    {
        $count = collect();

        $jobs = Occupation::all()->groupBy('name')->map(fn ($item) =>
            $item->map(fn ($item) => $item->courses->pluck('code'))->flatten()
        );

        foreach ($jobs as $key => $value) {
            $count->push(Graduate::whereHas('academic.course', fn ($query) =>
                $query->where('school', $school)->where('department', $department)->whereIn('code', $value)
            )->whereHas('confirmed.employment', fn ($query) => $query->where('job_position', $key))->count());
        }

        return $count->sum();
    }

    private function getLagTimePriorToEmploymentCount($school, $department)
    {
        $count = collect();

        $graduates = Graduate::whereHas('academic', fn ($query) =>
            $query->where('school', $school)->where('department', $department)
        )->whereHas('confirmed.employment')->get();

        $count->put("less than a month", $graduates->map(fn ($graduate) =>
            $this->getDaysDifference($graduate) < 31
        )->filter()->count());

        $count->put("1 to 6 months", $graduates->map(fn ($graduate) =>
            $this->getDaysDifference($graduate) >= 31 && $this->getDaysDifference($graduate) < 210
        )->filter()->count());

        $count->put("7 to 11 months", $graduates->map(fn ($graduate) =>
            $this->getDaysDifference($graduate) >= 210 && $this->getDaysDifference($graduate) < 365
        )->filter()->count());

        $count->put("1 year to less than 2 years", $graduates->map(fn ($graduate) =>
            $this->getDaysDifference($graduate) >= 365 && $this->getDaysDifference($graduate) < 730
        )->filter()->count());

        $count->put("2 years to less than 3 years", $graduates->map(fn ($graduate) =>
            $this->getDaysDifference($graduate) >= 730 && $this->getDaysDifference($graduate) < 1095
        )->filter()->count());

        $count->put("3 years to less than 4 years", $graduates->map(fn ($graduate) =>
            $this->getDaysDifference($graduate) >= 1095 && $this->getDaysDifference($graduate) < 1460
        )->filter()->count());

        $count->put("4 years and above", $graduates->map(fn ($graduate) =>
            $this->getDaysDifference($graduate) >= 1460
        )->filter()->count());

        return $count->all();
    }

    private function getDaysDifference($graduate)
    {
        $graduated = new DateTime($graduate->date_graduated);
        $employed = new DateTime($graduate->date_employed);

        return $graduated->diff($employed)->days;
    }
}
