<?php

namespace App\Http\Controllers\Department;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreConfirmRequest;
use App\Http\Requests\StoreGraduateRequest;
use App\Http\Requests\StoreImportRequest;
use App\Models\Graduate;
use App\Traits\GraduateTrait;
use App\Traits\StaticTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GraduateController extends Controller
{
    use GraduateTrait, StaticTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'dept']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->showGraduates(Auth::user());

        return view('department.graduates', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreGraduateRequest $request)
    {
        $this->storeGraduate($request, Auth::user());

        return back()->with('success', "Graduate {$request->btnGraduate} successfully.");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->showGraduate($id);

        return view('department.graduate', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $graduate = Graduate::with([
            'person', 'academic.course', 'contacts', 'confirmed.employment.company'
        ])->find($id);

        return response()->json(compact('graduate'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $graduate = Graduate::find($id);

        if ($graduate) {
            $graduate->person->delete();
        }

        return redirect()->route('graduates.index')->with('success', 'Graduate removed successfully.');
    }

    public function confirm(StoreConfirmRequest $request)
    {
        $this->confirmGraduate($request);

        return back()->with('success', "Graduate {$request->btnConfirm} successfully.");
    }

    public function preview(StoreImportRequest $request)
    {
        $data = $this->previewGraduates($request);

        if ($data != null) {
            return view('department.fields', $data);
        }

        return redirect()->route('graduates.index');
    }

    public function upload(Request $request)
    {
        $success = $this->uploadGraduates($request);

        if ($success) {
            return redirect()->route('graduates.index')->with('success', 'Data uploaded successfully.');
        }

        return redirect()->route('graduates.index')->withErrors(['File upload has encountered an error.']);
    }
}
