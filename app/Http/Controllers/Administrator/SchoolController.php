<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreSchoolRequest;
use App\Models\School;
use App\Traits\StaticTrait;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class SchoolController extends Controller
{
    use StaticTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'admin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $schools = School::orderBy('name')->paginate(15);
        $page = request()->page;

        return view('administrator.school', compact('schools', 'page'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSchoolRequest $request)
    {
        $imagePath = null; $data = $request->validated();
        $school = School::find($request->school);

        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            $directory = "files/shares/logos/school";

            if (Storage::makeDirectory("public/{$directory}")) {
                $imagePath = $file->storeAs($directory, $file->getClientOriginalName(), 'public');
                $image = Image::make(public_path("storage/{$imagePath}"));
                $image->fit(130, 130); $image->save();
                $imagePath = Storage::url($imagePath);
            }
        }

        School::updateOrCreate([
            'id' => $request->school ?? Str::random()
        ], [
            'name' => $this->capitalize($data['name']),
            'logo' => $imagePath ?? $school->logo ?? null
        ]);

        return back()->with('success', "School {$request->btnSchool} successfully.");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $school = School::find($id);

        return response()->json(compact('school'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $school = School::find($id);

        return response()->json(compact('school'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $school = School::find($id);

        if ($school)
            $school->delete();

        return back()->with('success', "School removed successfully.");
    }
}
