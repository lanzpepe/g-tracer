<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreDepartmentRequest;
use App\Models\Department;
use App\Models\School;
use App\Traits\StaticTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class DepartmentController extends Controller
{
    use StaticTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'admin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $schools = School::orderBy('name')->get();
        $depts = Department::orderBy('name')->paginate(15);
        $page = request()->page;

        return view('administrator.department', compact('depts', 'schools', 'page'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDepartmentRequest $request)
    {
        $imagePath = null; $data = $request->validated();
        $school = School::where('name', $data['school'])->first();
        $dept = Department::whereHas('schools', fn ($query) =>
            $query->where('name', $this->capitalize($school->name))
        )->where('name', $this->capitalize($data['dept']))->first();

        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            $directory = "files/shares/logos/department/{$school->name}";

            if (Storage::makeDirectory("public/{$directory}")) {
                $imagePath = $file->storeAs($directory, $file->getClientOriginalName(), 'public');
                $image = Image::make(public_path("storage/{$imagePath}"));
                $image->fit(130, 130); $image->save();
                $imagePath = Storage::url($imagePath);
            }
        }

        if ($dept && $request->btnDept == 'added') {
            return back()->withErrors("Department name has already been taken.");
        }

        $department = Department::updateOrCreate([
            'id' => $request->department ?? Str::random()
        ], [
            'name' => $this->capitalize($data['dept']),
            'logo' => $imagePath ?? Department::find($request->department)->logo ?? null
        ]);

        $department->schools()->syncWithoutDetaching($school->id);

        return back()->with('success', "Department {$request->btnDept} successfully.");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dept = Department::with('schools')->find($id);

        return response()->json(compact('dept'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dept = Department::with('schools')->find($id);

        return response()->json(compact('dept'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Str::of($id)->explode('+');
        $department = Department::find($data[0]);

        if ($department) {
            $department->schools()->detach($data[1]);
        }

        return back()->with('success', "Department removed succesfully.");
    }

    public function fetch(Request $request)
    {
        $school = School::where('name', $request->get('value'))->first();
        $data = $school->departments()->get();
        $option = "<option value='' selected>- Select Department -</option>";

        foreach ($data as $row) {
            $option .= "<option value='{$row->name}'>{$row->name}</option>";
        }

        return $option;
    }
}
