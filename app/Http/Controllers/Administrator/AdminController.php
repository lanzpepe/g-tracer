<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'admin']);
    }

    public function home()
    {
        return view('layouts.home');
    }

    public function fileManager()
    {
        return view('layouts.file_manager');
    }

    public function profile()
    {
        $user = User::find(Auth::user()->user_id);

        return view('layouts.profile', compact('user'));
    }
}
