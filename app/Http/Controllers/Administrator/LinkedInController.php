<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\Models\CsvFile;
use App\Models\LinkedInProfile;
use App\Traits\StaticTrait;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use League\Csv\Reader;
use League\Csv\Statement;

class LinkedInController extends Controller
{
    use StaticTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'admin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        CsvFile::truncate();
        $profiles = LinkedInProfile::all();

        return view('administrator.linkedin', compact('profiles'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $linkedin = LinkedInProfile::find($id);

        return response()->json(compact('linkedin'));
    }

    public function parse(Request $request)
    {
        if ($request->hasFile('file')) {
            $path = $request->file('file')->getRealPath();
            $file = Reader::createFromPath($path, 'r')->setHeaderOffset(0);
            $records = (new Statement)->process($file);

            $csv = CsvFile::create([
                'file_name' => $request->file('file')->getClientOriginalName(),
                'headers' => json_encode($file->getHeader()),
                'data' => json_encode($records)
            ]);

            $headers = json_decode($csv['headers'], true);
            $data = json_decode($csv['data'], true);

            return view('administrator.fields_in', compact('csv', 'headers', 'data'));
        }

        return back()->withErrors(['error' => "CSV file is required."]);
    }

    public function upload(Request $request)
    {
        try {
            $csv = CsvFile::find(Crypt::decrypt($request->data));
            $csvData = json_decode($csv->data, true);

            foreach ($csvData as $data) {
                LinkedInProfile::create([
                    'id' => $data['id'],
                    'profile_url' => $data['profile_url'],
                    'first_name' => $data['original_first_name'],
                    'last_name' => $data['original_last_name'],
                    'avatar' => empty($data['avatar']) ? null : $data['avatar'],
                    'title' => empty($data['headline']) ? null : $data['headline'],
                    'company' => empty($data['current_company']) ? null : $data['current_company'],
                    'position' => empty($data['current_company_position']) ? null : $data['current_company_position']
                ]);
            }

            $csv->delete();

            return redirect()->route('linkedin.index')->with('success', 'Data uploaded successfully.');

        } catch (DecryptException $e) {
            echo($e->getMessage());
        }
    }
}
