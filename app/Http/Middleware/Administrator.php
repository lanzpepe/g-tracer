<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Admin;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class Administrator
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            $admin = Admin::find(Auth::user()->admin_id);

            if (Str::of($admin->role)->exactly(config('constants.roles.admin'))) {
                $admin->user->update(['is_active' => true]);
                return $next($request);
            }
        }

        return redirect()->route('dept.home');
    }
}
