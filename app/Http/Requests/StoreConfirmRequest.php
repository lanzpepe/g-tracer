<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreConfirmRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'status' => ['bail', 'required', 'exists:employment_status,description'],
            'coname' => ['exclude_if:status,Unemployed', 'bail', 'required', 'regex:/^[A-z\s-]+$/', 'max:128'],
            'coaddress' => ['exclude_if:status,Unemployed', 'bail', 'required', 'max:255'],
            'cocontact' => ['exclude_if:status,Unemployed', 'bail', 'required', 'digits:11'],
            'position' => ['exclude_if:status,Unemployed', 'bail', 'required', 'string', 'max:128'],
            'datehired' => ['exclude_if:status,Unemployed', 'bail', 'required', 'date', 'max:32'],
            'remarks' => ['bail', 'required', 'string']
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'status.required' => 'Employment status is required.',
            'status.exists' => 'Employment status does not exists.',
            'coname.required' => 'Company name is required.',
            'coname.regex' => 'Company name format is invalid.',
            'coname.max' => 'Company name should not exceed 128 characters long.',
            'coaddress.required' => 'Company address is required.',
            'coaddress.max' => 'Company address should not exceed 255 characters long.',
            'cocontact.required' => 'Company contact number is required.',
            'cocontact.numeric' => 'Company contact number should be numeric',
            'position.required' => 'Position/designation is required.',
            'position.string' => 'Position/designation should be string.',
            'position.max' => 'Position/designation should not exceed 128 characters long.',
            'datehired.required' => 'Date employed is required.',
            'datehired.date' => 'Date employed should be in date format.',
            'datehired.max' => 'Date employed should not exceed 32 characters long.'
        ];
    }
}
