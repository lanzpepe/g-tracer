<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreGraduateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => ['required', 'regex:/^[A-z\s-]+$/', 'max:64',
                Rule::unique('people', 'last_name')->where(fn ($query) =>
                    $query->where('first_name', $this->firstname)
                )->ignore($this->person, 'person_id')
            ],
            'lastname' => ['required', 'regex:/^([A-z\s-]+).$/', 'max:64'],
            'midname' => ['nullable', 'regex:/^[A-z\s-]+$/', 'max:64'],
            'gender' => ['required', 'regex:/^[A-z\s-]+$/', 'exists:genders,name', 'max:8'],
            'address' => ['required', 'max:255'],
            'contact' => ['nullable', 'digits:11'],
            'course' => ['required', 'regex:/^[A-z\s-]+$/', 'max:128'],
            'major' => ['nullable', 'regex:/^[A-z\s-]+$/', 'max:64'],
            'year' => ['required', 'numeric', 'exists:school_years,year'],
            'month' => ['required', 'between:1,12'],
            'day' => ['required', 'between:1,31'],
            'image' => ['nullable', 'file', 'image']
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'firstname.unique' => 'Name has already been taken.',
            'firstname.required' => 'First name is required',
            'lastname.required' => 'Last name is required',
            'gender.required' => 'Gender is required.',
            'gender.exists' => 'Gender does not exist.',
            'address.required' => 'Address is required.',
            'course.required' => 'Course is required.',
            'major.required' => 'Major is required.',
            'year.required' => 'Year is required.',
            'year.exists' => 'Year does not exists.',
            'month.required' => 'Month is required.',
            'day.required' => 'Day is required.'
        ];
    }
}
