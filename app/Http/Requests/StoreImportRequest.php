<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreImportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            '_school' => ['required', 'regex:/^[A-z\s-]+$/', 'exists:schools,name', 'max:64'],
            '_dept' => ['required', 'regex:/^[A-z\s-]+$/', 'exists:departments,name', 'max:64'],
            '_course' => ['required', 'regex:/^[A-z\s-]+$/', 'exists:courses,code', 'max:64'],
            '_major' => ['required', 'regex:/^[A-z\s-]+$/', 'exists:courses,major', 'max:32'],
            '_year' => ['required', 'numeric', 'exists:school_years,year'],
            '_month' => ['required', 'between:1,12'],
            '_day' => ['required', 'between:1,31'],
            '_file' => ['required', 'file', 'mimes:csv,txt']
        ];
    }
}
