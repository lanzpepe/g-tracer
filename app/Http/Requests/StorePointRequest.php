<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePointRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'submitted' => ['nullable', 'numeric', 'min:1'],
            'confirmed' => ['nullable', 'numeric', 'min:1']
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'submitted.numeric' => 'Invalid submitted response point threshold value.',
            'submitted.min' => 'Submitted response point threshold value must be at least 1.',
            'confirmed.numeric' => 'Invalid confirmed response point threshold value.',
            'confirmed.min' => 'Confirmed response point threshold value must be at least 1.'
        ];
    }
}
