<?php

namespace App\Helpers;

use App\Events\GraduateAdded;
use App\Events\TwitterFriendsSaved;
use App\Models\Company;
use App\Models\Person;
use App\Models\Response;
use App\Models\User;
use App\Notifications\UserRespond;
use App\Traits\StaticTrait;
use Illuminate\Support\Str;

class UserInstance
{
    use StaticTrait;

    public static function create($graduate, $data)
    {
        $person = Person::firstOrCreate([
            'last_name' => $data['last_name'],
            'first_name' => $data['first_name'],
            'middle_name' => $data['middle_name'],
            'gender' => $data['gender'],
            'birth_date' => $data['birth_date']
        ], [
            'person_id' => Str::random()
        ]);

        $user = User::create([
            'user_id' => Str::uuid(),
            'image_uri' => $data['image_uri'],
            'person_id' => $person->person_id
        ]);

        $user->graduate()->save($graduate);

        $graduate->contacts()->create([
            'contact_id' => Str::random(),
            'address' => $data['address'],
            'contact_number' => $data['contact_number'],
            'email' => $data['email']
        ]);

        $user->providerAccounts()->create([
            'provider_id' => $data['provider_id'],
            'provider_name' => $data['provider_name'],
            'profile_id' => $data['profile_id']
        ]);

        $user->preference()->create([
            'id' => Str::random(),
            'school' => $graduate->academic->school,
            'department' => $graduate->academic->department,
            'degree' => $graduate->academic->course->title,
            'major' => $graduate->academic->course->major,
            'school_year' => $graduate->academic->year,
            'batch' => $graduate->academic->month
        ]);

        $user->achievement()->create([
            'id' => Str::random(),
            'points' => 0,
            'exp_points' => 0,
            'rank_id' => 1
        ]);

        $company = Company::create([
            'company_id' => Str::random(),
            'name' => $data['co_name'],
            'address' => $data['co_address'],
            'contact_number' => $data['co_contact']
        ]);

        $company->employments()->create([
            'id' => Str::random(),
            'job_position' => $data['job_position'],
            'date_employed' => $data['date_employed']
        ]);

        $response = Response::create([
            'response_id' => Str::random(),
            'job_position' => $data['job_position'],
            'remarks' => $data['remarks'],
            'company_id' => $company->company_id
        ]);

        $user->graduates()->syncWithoutDetaching([
            $graduate->graduate_id => ['response_id' => $response->response_id]
        ]);

        $admin = self::findAdmin($graduate);
        $admin->notify(new UserRespond($graduate));

        event(new GraduateAdded($graduate));
        event(new TwitterFriendsSaved($user));

        return $user;
    }
}

