<?php

namespace App\Helpers;

use App\Models\Graduate;
use App\Models\ProviderAccount;
use App\Models\TwitterFriendId;
use App\Traits\StaticTrait;
use Exception;
use Phpml\Math\Distance\Euclidean;

class GraduateSimilarity
{
    use StaticTrait;

    private $graduates;
    private $educationWeight = 0.5, $addressWeight = 0.35, $friendsWeight = 0.15;

    public function __construct($graduates)
    {
        $this->graduates = $graduates;
    }

    public function calculateSimilarityMatrix($user, $preferences)
    {
        $education = collect(); $distance = collect(); $friends = collect();

        foreach ($this->graduates as $graduate) {
            if ($graduate->graduate_id == $user->graduate->graduate_id) { continue; }
            $education->put($graduate->full_name, $this->calculateEducationSimilarity($graduate, $preferences));
            $distance->put($graduate->full_name, $this->calculateNearestDistance($graduate, $user));
            $friends->put($graduate->full_name, $this->suggestTwitterFriends($graduate, $user));
        }

        $similarityScores = $education->map(fn ($item) =>
            $this->minMaxNorm($item, $education->min(), $education->max()) * $this->educationWeight
        )->mergeRecursive($distance->map(fn ($item) =>
            $this->minMaxNorm($item, $distance->min(), $distance->max()) * $this->addressWeight
        ))->mergeRecursive($friends->map(fn ($item) =>
            $this->minMaxNorm($item, $friends->min(), $friends->max()) * $this->friendsWeight
        ));

        $scores = $similarityScores->map(fn ($item, $key) =>
            collect($similarityScores->get($key))->sum()
        );

        return $scores;
    }

    public function getGraduatesSortedBySimilarity($scores, $user)
    {
        $sortedGraduates = collect();
        $scores = $scores->sort()->reverse()->take(100)->all(); // retrieve first 100 results only

        $graduatess = Graduate::with(['person', 'contacts', 'academic.course'])->whereDoesntHave('users', fn ($query) =>
            $query->where('users.user_id', $user->user_id)
        )->whereDoesntHave('confirmed')->get();

        foreach ($scores as $key => $value) {
            $graduates = collect($graduatess)->filter(fn ($value) =>
                $value->full_name == $key
            );
            if (!count($graduates)) { continue; }
            $graduate = $graduates->get($graduates->keys()->first());
            $graduate->score = $value;
            $sortedGraduates->push($graduate);
        }

        return $sortedGraduates->all();
    }

    private function calculateEducationSimilarity($graduate, $preferences)
    {
        $vector1 = collect([3, 2.5, 2, 1.5, 1]); // base attribute scores
        $vector2 = collect();
        $euclidean = new Euclidean();

        $vector2->push($preferences->get('school') == $graduate->academic->school ? 3 : 0);
        $vector2->push($preferences->get('department') == $graduate->academic->department ? 2.5 : 0);
        $vector2->push($preferences->get('degree') == $graduate->course && $preferences->get('major') == $graduate->major ? 2 : 0);
        $vector2->push($preferences->get('school_year') == $graduate->academic->year ? 1.5 : 0);
        $vector2->push($preferences->get('batch') == $graduate->academic->month ? 1 : 0);

        // return the Euclidean distance of two vectors
        return $euclidean->distance($vector1->all(), $vector2->all());
    }

    private function calculateNearestDistance($graduate, $user)
    {
        $contact1 = $graduate->contacts->first();
        $contact2 = $user->graduate->contacts->first();

        return $this->getDistance($contact1->latitude, $contact2->latitude, $contact1->longitude, $contact2->longitude);
    }

    private function suggestTwitterFriends($graduate1, $graduate2)
    {
        $suggested = 0; $provider1 = null; $provider2 = null;

        if ($graduate1->user != null)
            $provider1 = ProviderAccount::where('provider_name', 'twitter.com')
                ->where('user_id', $graduate1->user->user_id)->first();

        if ($graduate2->user != null)
            $provider2 = ProviderAccount::where('provider_name', 'twitter.com')
                ->where('user_id', $graduate2->user->user_id)->first();

        if ($graduate1->user != null && $provider2 != null) {
            $friends = TwitterFriendId::where('user_id', $graduate1->user->user_id)->get();
            foreach ($friends as $friend) {
                if ($provider2 != null) {
                    if ($friend->friend_id == $provider2->provider_id) {
                        $suggested = 1;
                    }
                }
            }
        }

        if ($graduate2->user != null && $provider1 != null) {
            $friends = TwitterFriendId::where('user_id', $graduate2->user->user_id)->get();
            foreach ($friends as $friend) {
                if ($provider1 != null) {
                    if ($friend->friend_id == $provider1->provider_id) {
                        $suggested = 1;
                    }
                }
            }
        }

        return $suggested;
    }

    private function minMaxNorm($value, $min, $max)
    {
        try {
            $numerator = $max - $value;
            $denominator = $max - $min;
            $norm = $numerator / $denominator;
        } catch (Exception $e) {
            return 0;
        }

        return $norm;
    }
}
