<?php

namespace App\Traits;

use App\Models\Admin;
use App\Models\Department;
use App\Models\Person;
use App\Models\Role;
use App\Models\School;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

trait AccountTrait
{
    use StaticTrait;

    public function storeAccount(FormRequest $request)
    {
        $data = $request->validated();
        $roleId = Role::where('name', $data['role'])->first()->id;
        $deptId = Department::where('name', $data['dept'])->first()->id;
        $schoolId = School::where('name', $data['school'])->first()->id;

        $person = Person::firstOrCreate([
            'last_name' => $this->capitalize($data['lastname']),
            'first_name' => $this->capitalize($data['firstname']),
            'middle_name' => $this->capitalize($data['midname']),
            'gender' => $this->capitalize($data['gender']),
            'birth_date' => $data['dob']
        ], [
            'person_id' => Str::random()
        ]);

        $user = User::updateOrCreate([
            'user_id' => $person->user->user_id ?? Str::uuid()
        ], [
            'image_uri' => null,
            'person_id' => $person->person_id
        ]);

        $admin = Admin::updateOrCreate([
            'admin_id' => $request->_admin ?? Str::uuid()
        ], [
            'username' => $data['username'],
            'password' => Hash::make($data['password']),
            'user_id' => $user->user_id
        ]);

        $admin->departments()->sync($deptId);
        $admin->schools()->sync($schoolId);
        $admin->roles()->sync($roleId);

        if ($request->btnAccount == 'added') {
            $admin->point()->create();
        }

        return $user;
    }
}
