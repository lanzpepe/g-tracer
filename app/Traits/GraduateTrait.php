<?php

namespace App\Traits;

use App\Events\GraduateAdded;
use App\Events\ImageAdded;
use App\Models\Admin;
use App\Models\Company;
use App\Models\Course;
use App\Models\CsvFile;
use App\Models\Graduate;
use App\Models\Person;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use League\Csv\Reader;
use League\Csv\Statement;

trait GraduateTrait
{
    public function showGraduates(Admin $admin)
    {
        $courses = Course::whereHas('departments', fn ($query) =>
            $query->where('id', $admin->department->id)
        )->orderBy('title')->get();
        $graduates = Graduate::with('confirmed')->whereHas('academic', fn ($query) =>
            $query->where('department', $admin->department->name)->where('school', $admin->school->name)
        )->whereHas('person', fn ($query) => $query->orderBy('last_name'))->get();

        event(new ImageAdded($admin));

        return compact('courses', 'graduates');
    }

    public function storeGraduate(FormRequest $request, Admin $admin)
    {
        $imagePath = null; $data = $request->validated();
        $graduate = Graduate::find($request->graduate);
        $course = Course::where('code', $data['course'])->where('major', $data['major'])->first();
        $month = date('F', mktime(0, 0, 0, $data['month'], 10));

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $directory = "files/{$admin->admin_id}/images/graduates/{$course->code}/{$data['year']}/{$month}";

            if (Storage::makeDirectory("public/{$directory}")) {
                $imagePath = $file->storeAs($directory, $file->getClientOriginalName(), 'public');
                $image = Image::make(public_path("storage/{$imagePath}"))->orientate();
                $image->fit(640, 640); $image->save();
                $imagePath = Storage::url($imagePath);
            }
        }

        $academic = [
            'course_id' => $course->id,
            'department' => Auth::user()->department->name,
            'school' => Auth::user()->school->name,
            'year' => $data['year'],
            'month' => $month,
            'day' => $data['day']
        ];

        $contact = [
            'address' => $data['address'],
            'contact_number' => $data['contact']
        ];

        $person = Person::firstOrCreate([
            'last_name' => $data['lastname'],
            'first_name' => $data['firstname'],
            'middle_name' => $data['midname'],
            'gender' => $data['gender']
        ], [
            'person_id' => Str::random()
        ]);

        $graduate = Graduate::updateOrCreate([
            'graduate_id' => $person->graduate->graduate_id ?? Str::random()
        ], [
            'image_uri' => $imagePath ?? $person->graduate->image_uri ?? null,
            'person_id' => $person->person_id
        ]);

        if ($request->btnGraduate == 'added') {
            $academic['academic_id'] = Str::random();
            $contact['contact_id'] = Str::random();
            $graduate->academic()->create($academic);
            $graduate->contacts()->create($contact);
        } else {
            $graduate->academic()->update($academic);
            $graduate->contacts()->update($contact);
        }

        event(new GraduateAdded($graduate));

        return $graduate;
    }

    public function showGraduate($id)
    {
        $courses = Course::whereHas('departments', fn ($query) =>
            $query->where('id', Auth::user()->department->id)
        )->orderBy('title')->get();
        $graduate = Graduate::find($id);
        $responses = $graduate->responses;

        if ($responses->isNotEmpty()) {
            $results = $this->getMostCommonResponsePercentage($responses);

            return compact('courses', 'graduate', 'responses', 'results');
        }

        return compact('courses', 'graduate', 'responses');
    }

    public function confirmGraduate(FormRequest $request)
    {
        $graduate = Graduate::find($request->query('id'));
        $data = $request->validated();

        $confirmation = [
            'status' => $data['status'],
            'remarks' => $data['remarks']
        ];

        if ($data['status'] === 'Employed') {
            $employment = [
                'job_position' => $data['position'],
                'date_employed' => $data['datehired']
            ];

            $company = Company::firstOrCreate([
                'name' => $data['coname'],
                'address' => $data['coaddress'],
                'contact_number' => $data['cocontact']
            ], [
                'company_id' => Str::random()
            ]);

            if ($request->btnConfirm === 'confirmed') {
                $employment['id'] = Str::random();
                $confirmation['id'] = Str::random();
                $employment = $company->employments()->create($employment);
                $confirmation['employment_id'] = $employment->id;
                $graduate->confirmed()->create($confirmation);
            }
            else {
                $company->employments()->update($employment);
                $graduate->confirmed()->update($confirmation);
            }
        }
        else {
            if ($request->btnConfirm === 'confirmed') {
                $confirmation['id'] = Str::random();
                $graduate->confirmed()->create($confirmation);
            }
            else {
                $graduate->confirmed()->update($confirmation);
            }
        }

        return $graduate;
    }

    public function previewGraduates(FormRequest $request)
    {
        $data = $request->validated();
        $courses = Course::whereHas('schools', fn ($query) =>
            $query->where('name', Auth::user()->school->name)
        )->whereHas('departments', fn ($query) =>
            $query->where('name', Auth::user()->department->name)
        )->orderBy('title')->get();
        $month = date('F', mktime(0, 0, 0, $data['_month'], 10));

        if ($request->hasFile('_file')) {
            $path = $request->file('_file')->getRealPath();
            $file = Reader::createFromPath($path, 'r')->setHeaderOffset(0);
            $csvData = (new Statement)->process($file);

            $csvFile = CsvFile::create([
                'file_name' => $request->file('_file')->getClientOriginalName(),
                'headers' => json_encode($file->getHeader()),
                'data' => json_encode($csvData)
            ]);

            return compact('courses', 'csvData', 'csvFile', 'data', 'month');
        }

        return null;
    }

    public function uploadGraduates(Request $request)
    {
        try {
            $fileId = Crypt::decrypt($request->data);
            $csvFile = CsvFile::find($fileId);
            $csvData = json_decode($csvFile->data, true);
            $course = Course::where('code', $request->course)->where('major', $request->major)->first();

            foreach ($csvData as $data) {
                $person = Person::firstOrCreate([
                    'last_name' => $data['Last Name'],
                    'first_name' => $data['First Name'],
                    'middle_name' => $data['M.I.'],
                    'gender' => $data['Gender']
                ], [
                    'person_id' => Str::random()
                ]);
                $graduate = Graduate::create([
                    'graduate_id' => Str::random(),
                    'image_uri' => null,
                    'person_id' => $person->person_id
                ]);
                $graduate->contacts()->create([
                    'contact_id' => Str::random(),
                    'address' => $this->capitalize($data['Address'])
                ]);
                $graduate->academic()->create([
                    'academic_id' => Str::random(),
                    'department' => $request->dept,
                    'school' => $request->school,
                    'year' => $request->year,
                    'month' => $request->month,
                    'day' => $request->day,
                    'course_id' => $course->id
                ]);

                event(new GraduateAdded($graduate));
            }

            $csvFile->delete();

            return true;

        } catch (DecryptException $e) {
            return false;
        }
    }

    private function getMostCommonResponsePercentage($responses)
    {
        $results = collect();

        $companies = $responses->groupBy('company.name')->map(fn ($item) => $item->count());
        $company = $companies->search($companies->max(), true);
        $addresses = $responses->groupBy(['company.name', 'company.address'])
                    ->get($company)->map(fn ($item) => $item->count());
        $address = $addresses->search($addresses->max(), true);
        $positions = $responses->groupBy(['company.name', 'job_position'])
                    ->get($company)->map(fn ($item) => $item->count());
        $position = $positions->search($positions->max(), true);
        $remarks = $responses->groupBy(['company.name', 'job_position', 'remarks'])
                    ->get($company)->get($position)->map(fn ($item) => $item->count());
        $remark = $remarks->search($remarks->max(), true);

        $results->put("{$company},+{$address}", round($companies->max() / $responses->count() * 100));
        $results->put($position, round($positions->max() / $companies->max() * 100));
        $results->put($remark, round($remarks->max() / $positions->max() * 100));

        return $results;
    }
}
