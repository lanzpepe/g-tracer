<?php

namespace App\Traits;

use App\Models\Admin;
use App\Models\Graduate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

trait StaticTrait
{
    /**
     * Return the data of graduate being searched.
     *
     * @param \Illuminate\Http\Request $request
     * @return \App\Models\Graduate
     */
    public function search(Request $request)
    {
        $graduate = Graduate::whereHas('academic.course', fn ($query) =>
            $query->where('title', $request->degree)->where('school', $request->school)
                ->where('year', $request->schoolYear)->where('month', $request->batch)
        )->whereHas('person', fn ($query) =>
            $query->where('last_name', $request->lastName)->where('first_name', $request->firstName)
        );

        return $graduate->first();
    }

    /**
     * Find admin user with same department and school of the respondent.
     *
     * @param \App\Models\Graduate $graduate
     * @return mixed
     */
    public static function findAdmin(Graduate $graduate)
    {
        $admin = Admin::whereHas('departments', fn ($query) =>
            $query->where('departments.name', $graduate->academic->department)
        )->whereHas('schools', fn ($query) =>
            $query->where('schools.name', $graduate->academic->school)
        )->whereHas('roles', fn ($query) =>
            $query->where('roles.name', config('constants.roles.dept'))
        )->first();

        return $admin;
    }

    /**
     * Capitalize each word in a string.
     *
     * @param string $tag
     * @return mixed
     */
    public function capitalize($tag)
    {
        $words = ['And', 'In', 'Of', 'The'];
        $word = Str::ucfirst($tag);
        $regex = '/\b(' . implode('|', $words) . ')\b/i';

        return preg_replace_callback($regex, fn ($matches) => Str::lower($matches[1]), $word);
    }

    /**
     * Returns a cursored collection of user IDs for every user the specified user is following
     * (otherwise known as their "friends").
     *
     * @param string $id
     * @return mixed
     */
    public function getTwitterFriendIds($id, $token)
    {
        $response = Http::withHeaders([
            'Authorization' => "Bearer {$token}"
        ])->get('https://api.twitter.com/1.1/friends/ids.json', [
            'user_id' => $id
        ]);

        return $response->json()['ids'];
    }

    /**
     * Converts encoded address into geographic coordinates (latitude and longitude).
     *
     * @param string $address
     * @return mixed
     */
    public function getLatLng($address)
    {
        $response = Http::get('https://maps.googleapis.com/maps/api/geocode/json', [
            'address' => $address,
            'region' => 'PH',
            'key' => env('FCM_SERVER_KEY')
        ]);

        return $response->json()['results'][0]['geometry']['location'];
    }

    /**
     * Returns the computed distance between two points in kilometers.
     *
     * @param float $lat1
     * @param float $lat2
     * @param float $long1
     * @param float $long2
     * @return float
     */
    public function getDistance($lat1, $lat2, $long1, $long2)
    {
        $sin = sin(deg2rad($lat1)) * sin(deg2rad($lat2));
        $cos = cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($long1) - deg2rad($long2));

        return acos($sin + $cos) * 6371;
    }
}
