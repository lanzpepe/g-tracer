<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVerificationRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('verification_requests', function (Blueprint $table) {
            $table->string('id', 32)->primary();
            $table->text('address');
            $table->string('contact_number', 16)->nullable();
            $table->string('department', 64);
            $table->string('school', 128);
            $table->string('month', 16);
            $table->unsignedInteger('year');
            $table->boolean('status')->default(0);
            $table->string('person_id', 32);
            $table->string('course_id', 32);
            $table->uuid('admin_id');
            $table->foreign('person_id')->references('person_id')->on('people')->onDelete('cascade');
            $table->foreign('course_id')->references('id')->on('courses')->onDelete('cascade');
            $table->foreign('admin_id')->references('admin_id')->on('admins')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('verification_requests');
    }
}
