<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAcademicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('academics', function (Blueprint $table) {
            $table->string('academic_id', 32)->primary();
            $table->string('department', 64);
            $table->string('school', 128);
            $table->string('month', 16);
            $table->unsignedInteger('day');
            $table->unsignedInteger('year');
            $table->string('course_id', 32);
            $table->string('graduate_id', 32);
            $table->foreign('course_id')->references('id')->on('courses')->onDelete('cascade');
            $table->foreign('graduate_id')->references('graduate_id')->on('graduates')->onDelete('cascade');
            $table->index(['department', 'school', 'month', 'day', 'year']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('academics');
    }
}
