<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->string('contact_id', 32)->primary();
            $table->text('address');
            $table->double('latitude')->default(0);
            $table->double('longitude')->default(0);
            $table->string('contact_number', 16)->nullable();
            $table->string('email', 64)->nullable();
            $table->string('graduate_id', 32);
            $table->index(['latitude', 'longitude']);
            $table->foreign('graduate_id')->references('graduate_id')->on('graduates')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
