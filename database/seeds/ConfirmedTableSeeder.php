<?php

use App\Models\Company;
use App\Models\Graduate;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class ConfirmedTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = collect(['Employed', 'Unemployed']);

        $companies = collect([
            ['Accenture', 'Cebu IT Park, Cebu City, Cebu, Philippines'],
            ['Alliance', 'Sumilon Rd., Cebu City, Cebu, Philippines'],
            ['Advanced World Systems', 'Cebu IT Park, Cebu City, Cebu, Philippines'],
            ['Fujitsu', 'Cebu Business Park, Cebu City, Cebu, Philippines'],
            ['Lexmark', 'Cebu Business Park, Cebu City, Cebu, Philippines'],
            ['TrustArc', 'Cebu Business Park, Cebu City, Cebu, Philippines'],
            ['Sykes', 'F. Cabahug St., Cebu City, Cebu, Philippines'],
            ['Dreamscape', 'Cebu Business Park, Cebu City, Cebu, Philippines'],
            ['AGuyIKnow', 'Minglanilla, Cebu, Philippines']
        ]);

        $positions = collect([
            'Software Engineer', 'Software Developer', 'Quality Assurance Tester',
            'Web Developer', 'Mobile Developer', 'Game Developer', 'Java Developer',
            'Business Analyst', 'Data Analyst', 'System Analyst', 'Project Manager',
            'Clerk', 'Human Resource Manager', 'Operations Supervisor'
        ]);

        $employedStatuses = collect(['Full-time Employee', 'Part-time Employee']);

        $unemployedStatuses = collect(['Health-related reasons', 'Advance or further study', 'No job opportunity']);

        $graduates = Graduate::all()->take(300)->all();

        srand(time());

        foreach ($graduates as $graduate) {
            $status = rand(0, $statuses->count() - 1);
            $company = rand(0, $companies->count() - 1);
            $position = rand(0, $positions->count() - 1);
            $days = rand(1, 1460);
            $date = new DateTime($graduate->date_graduated);

            if ($statuses[$status] === 'Employed') {
                $dateEmployed = $date->add(new DateInterval("P{$days}D"));
                $employedStatus = rand(0, $employedStatuses->count() - 1);

                $employment = [
                    'id' => Str::random(),
                    'job_position' => $positions[$position],
                    'date_employed' => $dateEmployed->format('F j, Y')
                ];

                $confirmation = [
                    'id' => Str::random(),
                    'status' => $statuses[$status],
                    'remarks' => $employedStatuses[$employedStatus]
                ];

                $company = Company::firstOrCreate([
                    'name' => $companies[$company][0],
                    'address' => $companies[$company][1]
                ], [
                    'company_id' => Str::random()
                ]);

                $employment = $company->employments()->create($employment);
                $confirmation['employment_id'] = $employment->id;
                $graduate->confirmed()->create($confirmation);

            } else {
                $unemployedStatus = rand(0, $unemployedStatuses->count() - 1);

                $confirmation = [
                    'id' => Str::random(),
                    'status' => $statuses[$status],
                    'remarks' => $unemployedStatuses[$unemployedStatus]
                ];
                
                $graduate->confirmed()->create($confirmation);
            }
        }
    }
}
