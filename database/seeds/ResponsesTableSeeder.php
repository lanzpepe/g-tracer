<?php

use App\Models\Company;
use App\Models\Graduate;
use App\Models\Response;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class ResponsesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $companies = collect([
            ['Accenture', 'Cebu IT Park, Cebu City, Cebu, Philippines'],
            ['Alliance', 'Sumilon Rd., Cebu City, Cebu, Philippines'],
            ['Advanced World Systems', 'Cebu IT Park, Cebu City, Cebu, Philippines'],
            ['Fujitsu', 'Cebu Business Park, Cebu City, Cebu, Philippines'],
            ['Lexmark', 'Cebu Business Park, Cebu City, Cebu, Philippines'],
            ['TrustArc', 'Cebu Business Park, Cebu City, Cebu, Philippines'],
            ['Sykes', 'F. Cabahug St., Cebu City, Cebu, Philippines'],
            ['Dreamscape', 'Cebu Business Park, Cebu City, Cebu, Philippines'],
            ['AGuyIKnow', 'Minglanilla, Cebu, Philippines']
        ]);
        $positions = collect([
            'Software Engineer', 'Software Developer', 'Quality Assurance Tester',
            'Web Developer', 'Mobile Developer', 'Game Developer', 'Java Developer',
            'Business Analyst', 'Data Analyst', 'System Analyst', 'Project Manager',
            'Software Consultant', 'Information Security Specialist'
        ]);
        $remarks = collect([
            'Friend', 'Schoolmate', 'Batchmate', 'Colleague', 'Blood Relation'
        ]);

        $graduates = Graduate::whereHas('academic', fn ($query) => $query->whereIn('year', ['2016', '2020']))->get();

        srand(time());

        foreach ($graduates as $graduate) {
            for ($i = 0; $i < 50; $i++) {
                $company = rand(0, $companies->count() - 1);
                $position = rand(0, $positions->count() - 1);
                $remark = rand(0, $remarks->count() - 1);

                $company = Company::firstOrCreate([
                    'name' => $companies[$company][0],
                    'address' => $companies[$company][1]
                ], [
                    'company_id' => Str::random()
                ]);

                $response = Response::create([
                    'response_id' => Str::random(),
                    'job_position' => $positions[$position],
                    'remarks' => $remarks[$remark],
                    'company_id' => $company->company_id
                ]);

                $graduate->users()->attach(Str::uuid()->toString(), ['response_id' => $response->response_id]);
            }
        }
    }
}
