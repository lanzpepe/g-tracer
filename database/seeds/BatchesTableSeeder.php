<?php

use App\Models\Batch;
use Illuminate\Database\Seeder;

class BatchesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Batch::insert([
            [
                'month' => 'March',
                'term' => 'Second Semester',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'month' => 'May',
                'term' => 'Summer',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'month' => 'October',
                'term' => 'First Semester',
                'created_at' => now(),
                'updated_at' => now()
            ]
        ]);
    }
}
