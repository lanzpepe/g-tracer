<?php

use App\Models\Department;
use App\Models\EmploymentStatus;
use App\Models\Gender;
use App\Models\Role;
use App\Models\School;
use App\Models\SchoolYear;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Gender::insert([
            [ 'name' => 'Male', 'created_at' => now(), 'updated_at' => now() ],
            [ 'name' => 'Female', 'created_at' => now(), 'updated_at' => now() ]
        ]);

        Role::insert([
            [ 'name' => config('constants.roles.admin'), 'created_at' => now(), 'updated_at' => now() ],
            [ 'name' => config('constants.roles.dept'), 'created_at' => now(), 'updated_at' => now() ]
        ]);

        School::create([
            'id' => Str::random(),
            'name' => 'University of San Jose - Recoletos'
        ]);

        EmploymentStatus::insert([
            [ 'description' => 'Employed', 'created_at' => now(), 'updated_at' => now() ],
            [ 'description' => 'Unemployed', 'created_at' => now(), 'updated_at' => now() ]
        ]);

        $school = School::where('name', 'University of San Jose - Recoletos')->first();
        $dept = Department::create(['id' => Str::random(), 'name' => 'ICCT']);
        $school->departments()->attach($dept->id);

        for ($year = 2011; $year <= date('Y'); $year++) {
            SchoolYear::create(['year' => $year]);
        }

        $this->call([
            BatchesTableSeeder::class,
            AdminsTableSeeder::class,
            RanksTableSeeder::class
        ]);
    }
}
