<?php

use App\Helpers\UserInstance;
use App\Models\Graduate;
use App\Traits\StaticTrait;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    use StaticTrait;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $graduate = Graduate::whereHas('person', fn ($query) =>
            $query->where('last_name', 'Gimenez')->where('first_name', 'Kent')
        )->first();

        if ($graduate) {
            UserInstance::create($graduate, [
                'last_name' => $graduate->person->last_name, 'first_name' => $graduate->person->first_name,
                'middle_name' => $graduate->person->middle_name, 'gender' => $graduate->person->gender,
                'birth_date' => "December 26, 1999", 'image_uri' => null,
                'address' => "Poblacion Ward I, Minglanilla, Cebu", 'contact_number' => "09262606010",
                'email' => "kintoy@usjr.edu.ph", 'provider_id' => '245323291',
                'provider_name' => 'twitter.com', 'profile_id' => 'adonthinkso',
                'co_name' => "Advanced World Systems", 'co_address' => "Cebu IT Park, Cebu City, Cebu, Philippines",
                'co_contact' => "09262606010", 'job_position' => "Java Developer",
                'date_employed' => "March 30, 2020", 'remarks' => "Respondent"
            ]);
        }

        /* $graduate = Graduate::where('last_name', 'Ferolino')->where('first_name', 'Kent')->first();

        if ($graduate) {
            UserInstance::create($graduate, [
                'last_name' => $graduate->last_name, 'first_name' => $graduate->first_name,
                'middle_name' => $graduate->middle_name, 'gender' => $graduate->gender,
                'birth_date' => "January 1, 1996", 'image_uri' => null,
                'address' => "LT 5-C Mighty Villa Subd., Bulacao, Cebu City, Cebu", 'contact_number' => "09262606010",
                'email' => "kferolino@usjr.edu.ph", 'provider_id' => '345323291',
                'provider_name' => 'twitter.com', 'profile_id' => 'jjcccccccc28',
                'co_name' => "Accenture", 'co_address' => "Cebu IT Park, Lahug, Cebu City, Cebu",
                'co_contact' => "09262606010", 'job_position' => "Software Engineer",
                'date_employed' => "March 30, 2016", 'remarks' => "Respondent"
            ]);
        } */
    }
}
