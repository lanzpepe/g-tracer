<?php

use App\Models\Graduate;
use App\Traits\StaticTrait;
use Illuminate\Database\Seeder;

class ContactsTableSeeder extends Seeder
{
    use StaticTrait;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $graduates = Graduate::all();

        $graduates->each(function ($item) {
            $item->contacts->each(function ($item) {
                if ($item->latitude == 0) {
                    $item->update([
                        'latitude' => $this->getLatLng($item->address)['lat'],
                        'longitude' => $this->getLatLng($item->address)['lng']
                    ]);
                }
            });
        });
    }
}
