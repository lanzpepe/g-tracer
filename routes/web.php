<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false]);

Route::view('/', 'layouts.welcome')->name('index');
Route::get('pages', 'HomeController@showPages');
Route::post('page', 'HomeController@storePage')->name('page.store');
Route::post('token', 'HomeController@storeToken');
Route::resource('notifications', 'NotificationController');

Route::middleware('auth', 'admin')->group(function () {
    Route::prefix('a')->group(function () {
        Route::get('home', 'Administrator\AdminController@home')->name('admin.home');
        Route::get('file-manager', 'Administrator\AdminController@fileManager')->name('admin.fm');
        Route::get('profile', 'Administrator\AdminController@profile')->name('admin.profile');
        Route::get('reports', 'Administrator\ReportController@reports')->name('admin.reports');
        Route::post('reports/courses', 'Administrator\ReportController@courses')->name('admin.courses');
        Route::post('reports/department', 'Administrator\ReportController@department')->name('admin.dept');
        Route::post('reports/download', 'Administrator\ReportController@download')->name('admin.download');
        Route::post('reports/generate', 'Administrator\ReportController@generate')->name('admin.generate');
        Route::post('reports/one', 'Administrator\ReportController@one')->name('admin.one');
        Route::post('reports/two', 'Administrator\ReportController@two')->name('admin.two');
        Route::post('departments/fetch', 'Administrator\DepartmentController@fetch')->name('dept.fetch');
        Route::post('linkedin/parse', 'Administrator\LinkedInController@parse')->name('in.parse');
        Route::post('linkedin/upload', 'Administrator\LinkedInController@upload')->name('in.upload');
        Route::resources([
            'users' => 'Administrator\UserController',
            'accounts' => 'Administrator\AccountController',
            'linkedin' => 'Administrator\LinkedInController',
            'schools' => 'Administrator\SchoolController',
            'departments' => 'Administrator\DepartmentController',
        ]);
    });
});

Route::middleware('auth', 'dept')->group(function () {
    Route::prefix('d')->group(function () {
        Route::get('home', 'Department\DepartmentController@home')->name('dept.home');
        Route::get('file-manager', 'Department\DepartmentController@fileManager')->name('dept.fm');
        Route::get('profile', 'Department\DepartmentController@profile')->name('dept.profile');
        Route::get('reports', 'Department\ReportController@reports')->name('dept.reports');
        Route::post('reports/download', 'Department\ReportController@download')->name('dept.download');
        Route::post('reports/generate', 'Department\ReportController@generate')->name('dept.generate');
        Route::post('graduates/confirm', 'Department\GraduateController@confirm')->name('g.confirm');
        Route::post('graduates/preview', 'Department\GraduateController@preview')->name('g.preview');
        Route::post('graduates/upload', 'Department\GraduateController@upload')->name('g.upload');
        Route::resources([
            'graduates' => 'Department\GraduateController',
            'courses' => 'Department\CourseController',
            'jobs' => 'Department\OccupationController',
            'points' => 'Department\PointController',
            'rewards' => 'Department\RewardController',
            'requests' => 'Department\VerificationController'
        ]);
    });
});
